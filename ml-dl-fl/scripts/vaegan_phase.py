from datetime import datetime
from loguru import logger
from pathlib import Path
import torch
from torch.utils.tensorboard import SummaryWriter

from ml_dl_fl.config import Config
from ml_dl_fl.data.load_datasets import load_phase_dataset
from ml_dl_fl.vaegan import VAEGAN
from ml_dl_fl import logging


def main():
    default_path = Path(__file__).parent.parent / 'config/vaegan_phase.yaml'
    cfg = Config.load_config(default_path)
    logger.debug(cfg)

    # device
    CUDA_DEVICE_NUM = 0
    device = torch.device(f'cuda:{CUDA_DEVICE_NUM}' if torch.cuda.is_available() else 'cpu')
    print('Device:', device)

    # dataset
    train, val, test = load_phase_dataset(cfg.data_dir)

    # tensorboard summary writer
    log_dir = cfg.out_dir / f"logs/vae{datetime.now().strftime('%Y%m%d-%H%M%S')}"
    writer = SummaryWriter(
        str(log_dir)
    )

    # model
    model = VAEGAN(
        train_dataset=val,
        val_dataset=val,
        test_dataset=test,
        latent_dims=cfg.latent_dims,
        lr=cfg.lr,
        batch_size=cfg.batch_size,
        epochs=cfg.epochs,
        gamma=cfg.recon_weight,
        gp_weight=cfg.gp_weight,
        writer=writer,
    ).to(device)

    # train model
    model.train()

    # evaluate
    fig = logging.plot_vae_reconstruction(model, test)
    fig.savefig(str(log_dir / 'reconstruction.png'))

    fig = logging.plot_vae_generate_new(model)
    fig.savefig(str(log_dir / 'generated.png'))


if __name__ == '__main__':
    main()