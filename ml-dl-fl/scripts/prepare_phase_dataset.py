from argparse import ArgumentParser
import math
import numpy as np
from os import getcwd
from os.path import dirname, realpath
from pathlib import Path
from tqdm import tqdm

from ml_dl_fl.data.csi_record import CsiRecord


def load_phase_dataset(
    data_folder,
    file_name=None,
    transmitter=0,
    receiver=0):
    """
    Load and concatenate phase data for one pair of transmitter and receiver.
    
    Expects data that was recorded in CsiRecord format and saves it into single numpy file.
    Phase data is transformed to [0..4phi] and then normalized to [0.0..1.0].

    Args:
        data_folder (str): Folder containing single data files.
        file_name (str, optional): File name, if only one file should be loaded. Defaults to None.
        transmitter (int, optional): Id of transmitter, mostlikely between 0 and 2. Defaults to 0.
        receiver (int, optional): Id of receiver, most likely between 0 and 2. Defaults to 0.

    Returns:
        np.array: Numpy array containing concatenated samples from recorded data
            for one pair of transmitter and receiver. 
    """

    if file_name:
        train_paths = Path(data_folder).glob(file_name)
    else:
        train_paths = Path(data_folder).glob('*.npy')
    train_paths = list(train_paths)

    normalized_phase = []

    for train_path in tqdm(train_paths):

        record = CsiRecord()
        record.set_path(train_path)
        record.load()

        if len(record.phase) <= 0:
            continue
        for item in record.phase:
            if len(item) and len(item[receiver] > 0):
                item = item[receiver][transmitter] # select one antenna pair
                item += 2*math.pi # normalize to values between 0 and 4pi
                item /= 4*math.pi # normalize to values between 0 and 1
                item = np.array(item)
                normalized_phase.append(item)

    normalized_phase = np.array(normalized_phase)
    normalized_phase = np.expand_dims(normalized_phase, axis=-1)
    return normalized_phase


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-d', '--data-folder', type=str, required=True)
    parser.add_argument('-f', '--file-name', type=str, default=None)
    parser.add_argument('-t', '--transmitter', type=int, default=0)
    parser.add_argument('-r', '--receiver', type=int, default=0)
    parser.add_argument('-o', '--out-path', type=str,
        default=f'{dirname(realpath(__file__))}/../data/phase_dataset.npy')
    args = parser.parse_args()

    input_data = load_phase_dataset(
        data_folder=args.data_folder,
        file_name=args.file_name,
        transmitter=args.transmitter,
        receiver=args.receiver,
    )
    print(f"Loaded {len(input_data)} samples")
    np.save(args.out_path, input_data)