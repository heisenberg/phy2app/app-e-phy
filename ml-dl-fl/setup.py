from setuptools import setup, find_packages

setup(
    name='ml-dl-fl',
    version='0.1.0',
    packages=find_packages(include=['ml_dl_fl']),
    install_requires=[
        'matplotlib',
        'numpy',
        'seaborn',
        'tensorflow',
        'tqdm',
        'datargs',
        'loguru',
    ]
)

