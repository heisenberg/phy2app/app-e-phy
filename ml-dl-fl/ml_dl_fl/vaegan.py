import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from tqdm import tqdm

class Encoder(nn.Module):
	def __init__(self, latent_dims):
		super().__init__()
		self.conv1 = nn.Conv1d(1, 32, kernel_size=5, padding='same')
		self.conv2 = nn.Conv1d(32, 64, kernel_size=5, padding='same')

		self.maxpool = nn.MaxPool1d(2)

		self.linear1 = nn.Linear(896, 14)
		self.linear2 = nn.Linear(14, latent_dims)
		self.linear3 = nn.Linear(14, latent_dims)

	def forward(self, x):
		x = F.relu(self.conv1(x))
		x = self.maxpool(x)
		x = F.relu(self.conv2(x))
		x = self.maxpool(x)
		x = torch.flatten(x, start_dim=1)
		x = F.relu(self.linear1(x))
		mu = self.linear2(x)
		log_var = self.linear3(x)

		return mu, log_var

class Decoder(nn.Module):
	def __init__(self, shared_dim, exclusive_dim):
		super().__init__()

		self.linear1 = nn.Linear(shared_dim+exclusive_dim, 14)

		self.deconv1 = nn.ConvTranspose1d(1, 64, kernel_size=5, padding=2)
		self.upsample1 = nn.Upsample(size=28)

		self.deconv2 = nn.ConvTranspose1d(64, 32, kernel_size=5, padding=2)
		self.upsample2 = nn.Upsample(size=56)

		self.deconv3 = nn.ConvTranspose1d(32, 1, kernel_size=3, padding=1)
		
	def forward(self, x, y):
		x = torch.cat([x, y], dim=1)
		x = F.relu(self.linear1(x))
		x = x[:, None, :]
		x = F.relu(self.deconv1(x))
		x = self.upsample1(x)
		x = F.relu(self.deconv2(x))
		x = self.upsample2(x)
		x = torch.sigmoid(self.deconv3(x))
		return x

class Discriminator(nn.Module):
	def __init__(self):
		super().__init__()
		self.conv1 = nn.Conv1d(1, 32, kernel_size=5, padding='same')
		self.conv2 = nn.Conv1d(32, 64, kernel_size=5, padding='same')

		self.maxpool = nn.MaxPool1d(2)

		self.linear1 = nn.Linear(896, 14)
		self.linear2 = nn.Linear(14, 1)

	def forward(self, x):
		x = F.relu(self.conv1(x))
		x = self.maxpool(x)
		x = F.relu(self.conv2(x))
		x = self.maxpool(x)
		x = torch.flatten(x, start_dim=1)
		x1 = x
		x = F.relu(self.linear1(x))
		x = self.linear2(x)
		#x = torch.sigmoid(self.linear2(x))

		return x

class VAEGAN(nn.Module):
	def __init__(self, train_dataset, val_dataset, test_dataset, latent_dims, lr, gamma, writer, batch_size, epochs, gp_weight=0.1):
		super().__init__()
		self.train_dataset = train_dataset
		self.val_dataset = val_dataset
		self.test_dataset = test_dataset
		self.lr = lr
		self.gamma = gamma
		self.encoder = Encoder(latent_dims)
		self.decoder = Decoder(latent_dims)
		self.discriminator = Discriminator()
		self.N = torch.distributions.Normal(0, 1)
		self.loss_idx = 0
		self.writer = writer
		self.batch_size = batch_size
		self.epochs = epochs
		self.gp_weight = gp_weight
		torch.autograd.set_detect_anomaly(True)

	def train_dataloader(self):
		return torch.utils.data.DataLoader(self.train_dataset, batch_size=self.batch_size)
	
	def val_dataloader(self):
		return torch.utils.data.DataLoader(self.val_dataset, batch_size=self.batch_size)
	
	def test_dataloader(self):
		return torch.utils.data.DataLoader(self.test_dataset, batch_size=self.batch_size)

	def reparametrize(self, mu, log_var):
		epsilon = self.N.sample(log_var.shape).to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
		z = mu + torch.exp(log_var*0.5) * epsilon
		return z

	def forward(self, x):
		# encoding
		mu, log_var = self.encoder(x)

		# reparametrize
		z = self.reparametrize(mu, log_var)

		# decoding
		reconstruction = self.decoder(z)

		return reconstruction, mu, log_var

	def configure_optimizers(self):
		opt_enc = torch.optim.Adam(self.encoder.parameters(), lr=self.lr)
		opt_dec = torch.optim.Adam(self.decoder.parameters(), lr=self.lr)
		opt_dis = torch.optim.Adam(self.discriminator.parameters(), lr=self.lr)
		return opt_enc, opt_dec, opt_dis

	def train(self):
		opt_enc, opt_dec, opt_dis = self.configure_optimizers()
		data = self.train_dataloader()

		for epoch in range(self.epochs):
			pbar = tqdm(data)
			print("EPOCH", epoch+1)
			for iter, x in enumerate(pbar):
				x = x.float().to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))

				# forward pass
				x_hat, mu, log_var = self.forward(x)

				# l-layer representation
				x_l = self.discriminator(x)[1] # real data
				x_l_tilde = self.discriminator(x_hat)[1] # reconstructed

				# MSE of intermediate discriminator representation
				loss = nn.MSELoss(reduction="sum")
				reconstruction_loss = loss(x, x_hat)

				# L_prior (here: KL-D)
				kl_loss = torch.mean(- 0.5 * torch.sum(1 + log_var - torch.square(mu) - torch.exp(log_var), dim=1))

				# GAN loss
				z_p = torch.distributions.Normal(0, 1).sample(torch.Size([x.size(dim=0), 3])).to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
				x_p = self.decoder(z_p)
				#gan_loss = torch.log(self.discriminator(x)[0]) + torch.log(1 - self.discriminator(x_hat)[0]) + torch.log(1 - self.discriminator(x_p)[0])
				#gan_loss = torch.mean(gan_loss)
				gan_loss = self.discriminator(x)[0].mean() - self.discriminator(x_p)[0].mean()

				# combined encoder/decoder loss
				enc_loss = reconstruction_loss + kl_loss
				#dec_loss = reconstruction_loss + kl_loss + gan_loss
				gan_loss_gp = -gan_loss + self.gradient_penalty(x, x_p)

				

				opt_dis.zero_grad()
				gan_loss_gp.backward(retain_graph=True)
				opt_dis.step()


				###

				opt_enc.zero_grad()
				enc_loss.backward(retain_graph=True)
				opt_enc.step()


				###
				gan_loss = self.discriminator(x)[0].mean() - self.discriminator(x_p)[0].mean()
				dec_loss = reconstruction_loss + kl_loss + gan_loss

				opt_dec.zero_grad()
				dec_loss.backward(retain_graph=True)
				opt_dec.step()
				

				self.writer.add_scalars("Losses", {"enc": enc_loss, "dec": dec_loss, "gan": gan_loss, "gan_gp": gan_loss_gp, "rec": reconstruction_loss, "kl": kl_loss}, self.loss_idx)
				self.loss_idx += 1

				pbar.set_postfix_str("Encoder loss: " + str(enc_loss.item()) + "\tDecocder loss: " + str(dec_loss.item()) + "\tDiscriminator loss: " + str(gan_loss.item()) + "\trec: " + str(reconstruction_loss.item()) +"\tkl: " + str(kl_loss.item()) +"\tgan: " + str(gan_loss.item()))

	def gradient_penalty(self, x, x_p):
		alpha = torch.rand(torch.Size([x.shape[0], 1, 56])).to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
		x_hat = alpha*x + (1-alpha)*x_p
		x_hat = torch.autograd.Variable(x_hat, requires_grad=True)
		prob_x_hat = self.discriminator(x_hat)[0]
		gradients = torch.autograd.grad(outputs=prob_x_hat, inputs=x_hat,
										grad_outputs=torch.ones(prob_x_hat.size()).cuda() if torch.cuda.is_available() else torch.ones(prob_x_hat.size()),
										create_graph=True, retain_graph=True)[0]
		gradients = gradients.view(x.shape[0], -1)
		gradients_norm = torch.norm(gradients)

		return self.gp_weight * ((gradients_norm-1) ** 2).mean()

class VAE(nn.Module):
	def __init__(self, train_dataset_x, val_dataset_x, test_dataset_x, train_dataset_y, val_dataset_y, test_dataset_y, shared_dim, exclusive_dim, lr, writer, batch_size, epochs, l1_rec_coeff=1, l1_sh_coeff=1, kl_coeff=1):
		super().__init__()
		self.train_dataset_x = train_dataset_x
		self.val_dataset_x = val_dataset_x
		self.test_dataset_x = test_dataset_x
		self.train_dataset_y = train_dataset_y
		self.val_dataset_y = val_dataset_y
		self.test_dataset_y = test_dataset_y
		self.lr = lr
		self.encoder_sh = Encoder(shared_dim)
		self.encoder_ex = Encoder(exclusive_dim)
		self.decoder = Decoder(shared_dim, exclusive_dim)
		self.discriminator = Discriminator()
		self.N = torch.distributions.Normal(0, 1)
		self.writer = writer
		self.batch_size = batch_size
		self.loss_idx = 0
		self.epochs = epochs
		self.l1_rec_coeff = l1_rec_coeff
		self.l1_sh_coeff = l1_sh_coeff
		self.kl_coeff = kl_coeff
		self.train_dataloader_x = torch.utils.data.DataLoader(self.train_dataset_x, batch_size=self.batch_size)
		self.train_dataloader_y = torch.utils.data.DataLoader(self.train_dataset_y, batch_size=self.batch_size)

	def val_dataloader(self):
		return torch.utils.data.DataLoader(self.val_dataset, batch_size=self.batch_size)
	
	def test_dataloader(self):
		return torch.utils.data.DataLoader(self.test_dataset, batch_size=self.batch_size)

	def reparametrize(self, mu, log_var):
		epsilon = self.N.sample(log_var.shape).to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
		z = mu + torch.exp(log_var*0.5) * epsilon
		return z

	def forward(self, x, y):
		# encoding shared
		mu_sh_x, log_var_sh_x = self.encoder_sh(x)
		mu_sh_y, log_var_sh_y = self.encoder_sh(y)

		# encoding exclusive
		mu_ex_x, log_var_ex_x = self.encoder_ex(x)
		mu_ex_y, log_var_ex_y = self.encoder_ex(y)

		# reparametrize
		z_sh_x = self.reparametrize(mu_sh_x, log_var_sh_x)
		z_sh_y = self.reparametrize(mu_sh_y, log_var_sh_y)
		z_ex_x = self.reparametrize(mu_ex_x, log_var_ex_x)
		z_ex_y = self.reparametrize(mu_ex_y, log_var_ex_y)

		# decoding
		reconstruction_x = self.decoder(z_sh_y, z_ex_x)
		reconstruction_y = self.decoder(z_sh_x, z_ex_y)

		return reconstruction_x, reconstruction_y, z_sh_x, z_sh_y, mu_ex_x, log_var_ex_x, mu_ex_y, log_var_ex_y

	def configure_optimizers(self):
		opt_enc_sh = torch.optim.Adam(self.encoder_sh.parameters(), lr=self.lr)
		opt_enc_ex = torch.optim.Adam(self.encoder_ex.parameters(), lr=self.lr)
		opt_dec = torch.optim.Adam(self.decoder.parameters(), lr=self.lr)
		opt_dis = torch.optim.Adam(self.discriminator.parameters(), lr=self.lr)
		return opt_enc_sh, opt_enc_ex, opt_dec, opt_dis

	def train_model(self):
		opt_enc_sh, opt_enc_ex, opt_dec, opt_dis = self.configure_optimizers()

		for epoch in tqdm(range(self.epochs)):
			#pbar = (tqdm(data_x), tqdm(data_y))
			print("EPOCH", epoch+1)
			for (idx, x), (idy, y) in zip(enumerate(self.train_dataloader_x), enumerate(self.train_dataloader_y)):
				opt_enc_sh.zero_grad()
				opt_enc_ex.zero_grad()
				opt_dec.zero_grad()

				# forward pass
				x = x.float().to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
				y = y.float().to(torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu'))
				x_hat, y_hat, x_sh_enc, y_sh_enc, mu_ex_x, log_var_ex_x, mu_ex_y, log_var_ex_y = self.forward(x, y)

				## l1 loss between 1) input and switched reconstruction and 2) shared representations
				l1_loss = nn.L1Loss(reduction="sum")
				reconstruction_loss_x = l1_loss(x_hat, x)
				reconstruction_loss_y = l1_loss(y_hat, y)
				l1_shared = l1_loss(x_sh_enc, y_sh_enc) # TODO

				# L_prior (here: KL-D)
				kl_x = torch.mean(- 0.5 * torch.sum(1 + log_var_ex_x - torch.square(mu_ex_x) - torch.exp(log_var_ex_x), dim=1))
				kl_y = torch.mean(- 0.5 * torch.sum(1 + log_var_ex_y - torch.square(mu_ex_y) - torch.exp(log_var_ex_y), dim=1))

				# GAN
				gan_x = torch.mean(torch.square(self.discriminator(x)) + torch.square(1 - self.discriminator(x_hat)))
				gan_y = torch.mean(torch.square(self.discriminator(y)) + torch.square(1 - self.discriminator(y_hat)))

				# combined loss
				total_loss = (
					self.l1_rec_coeff * (reconstruction_loss_x + reconstruction_loss_y) +
					self.l1_sh_coeff * l1_shared +
					self.kl_coeff * (kl_x + kl_y) +
					gan_x + 
					gan_y
				)

				self.writer.add_scalars("Loss", {"total": total_loss}, self.loss_idx)
				self.loss_idx += 1
				
				total_loss.backward()

				opt_enc_sh.step()
				opt_enc_ex.step()
				opt_dec.step()
				opt_dis.step()

				#pbar.set_postfix_str("\tLOSS: " + str(loss.item()) + "\t(rec: " + str(reconstruction_loss.item()) +"\tkl: " + str(kl_loss.item()) + ")")
