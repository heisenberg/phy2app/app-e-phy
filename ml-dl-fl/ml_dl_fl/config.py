from dataclasses import dataclass, asdict
from datargs import parse
from os import makedirs
from pathlib import Path
from typing import Optional
import yaml


@dataclass
class Config:
    # network
    latent_dims: Optional[int]
    # losses
    kl_weight: Optional[float]
    recon_weight: Optional[float]
    disc_weight: Optional[float]
    gp_weight: Optional[float]
    # training
    batch_size: Optional[int]
    lr: Optional[float]
    epochs: Optional[int]
    # use only from command line
    config_path: Optional[Path]=None
    out_dir: Optional[Path]=None
    data_dir: Optional[Path]=None

    def __post_init__(self):
        if self.config_path:
            self.config_path = self.config_path.resolve()
        if self.out_dir:
            self.out_dir = self.out_dir.resolve()
        if self.data_dir:
            self.data_dir = self.data_dir.resolve()

    def read(config_path: Path):
        with open(config_path, 'r') as f:
            return Config(**yaml.safe_load(f))


    def update(self, other):
        d = self.dump()
        for k, v in other.dump().items():
            if v is not None:
                d[k] = v
        return Config(**d)

    def dump(self):
        return asdict(self)

    def save(self, config_path: Path=None):
        config = self.dump()
        del config['config_path']
        makedirs(config_path.parent, exist_ok=True)
        with open(config_path, 'w') as f:
            yaml.dump(config, f)

    @staticmethod
    def load_config(default_path: Path):
        """
        Read configuration from file and arguments.

        First, read config from file (given or default).
        Update with config parsed as arguments.
        """
        config_parsed = parse(Config)

        if not config_parsed.config_path is None:
            default_path = config_parsed.config_path
        config_default = Config.read(default_path)

        config_merged = config_default.update(config_parsed)

        return config_merged