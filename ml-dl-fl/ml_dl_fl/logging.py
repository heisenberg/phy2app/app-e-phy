import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
import torch


def plot_vae_reconstruction(model, test_data, n=10):
    fig, axs = plt.subplots(2, n, figsize=(40, 5))

    for i in range(n):
        s = np.random.randint(0, 169472, 1)[0]
        data = torch.tensor(test_data[s][0])[None, None, :].float()
        device = torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu')
        data = data.to(device)
        mu, log_var = model.encoder(data)
        z = model.reparametrize(mu, log_var)
        reconstruction = model.decoder(z)
        critic_real = model.discriminator(data)
        critic_fake = model.discriminator(reconstruction)
        #axs[0, i].set_ylim([0, 1])
        #axs[1, i].set_ylim([0, 1])
        axs[0, i].plot(np.squeeze(data.cpu()))
        axs[0, i].set_title(f"s: {critic_real[0].item()}")
        axs[1, i].plot(np.squeeze(reconstruction.cpu().detach().numpy()))
        axs[1, i].set_title(f"s: {critic_fake[0].item()}")

    return fig
    # plt.show()


def plot_vae_generate_new(model, n=10):
    fig, axs = plt.subplots(1, n, figsize=(40, 2))
    for i in range(n):
        device = torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu')
        z = torch.distributions.Normal(0, 1).sample(torch.Size([1, 3])).to(device)
        x = model.decoder(z.float())
        print(z.squeeze().cpu())
        critic = model.discriminator(x)
        axs[i].plot(x.squeeze().cpu().detach().numpy())
        axs[i].set_title(f"s: {critic[0].item()}")

    return fig
    # plt.show()


def figure_to_array(fig):
    canvas = FigureCanvasAgg(fig)
    canvas.draw()
    buf = canvas.buffer_rgba()
    plt.close(fig)
    # convert to a NumPy array
    img_arr = np.asarray(buf)
    img_arr = img_arr[:,:,:3]
    img_arr = np.moveaxis(img_arr, -1, 0)
    img_arr = np.expand_dims(img_arr, axis=0)

    return img_arr