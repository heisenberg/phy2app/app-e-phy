from loguru import logger
import numpy as np
from pathlib import Path
from torch import utils as torch_utils


def load_phase_dataset(path: Path):
    input_data = np.load(path)
    input_data = np.swapaxes(input_data, 1, 2)
    logger.info("Loaded phase dataset:")
    logger.info(f"Data: {np.shape(input_data)}")

    train_val_size = int(0.8 * input_data.shape[0])
    test_size = input_data.shape[0] - train_val_size

    train_val_dataset, test_dataset = torch_utils.data.random_split(input_data, [train_val_size, test_size])
    train_size = int(0.75 * len(train_val_dataset))
    val_size = len(train_val_dataset) - train_size

    train_dataset, val_dataset = torch_utils.data.random_split(train_val_dataset, [train_size, val_size])

    logger.info(f"Train: {train_size}")
    logger.info(f"Val: {val_size}")
    logger.info(f"Test: {test_size}")
    logger.info(f"Total: {train_size+val_size+test_size}")

    return train_dataset, val_dataset, test_dataset