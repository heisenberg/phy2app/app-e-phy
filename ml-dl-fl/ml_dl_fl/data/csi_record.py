import numpy as np
from os import fstat
from pathlib import Path

class CsiRecord():
    """
    Used to load and store csi data including csi header, phase and amplitude.
    Taken from https://git.imp.fu-berlin.de/heisenberg/autokey
    """
    def __init__(self):
        self.path = None
        self.csi = []
        self.headers = []
        self.amp = []
        self.phase = []

    def load(self):
        if self.path:
            with self.path.open('rb') as fd:
                file_size = fstat(fd.fileno()).st_size
                while fd.tell() < file_size:
                    self.headers.append(np.load(fd))
                    self.csi.append(np.load(fd))

                self.calc_phase_and_amp()

        else:
            print("Path is not set.")

    def store(self):
        pass

    def add(self, new_csi, new_csi_hdr):
        self.headers.append(new_csi_hdr)
        self.csi.append(new_csi)
        self.amp.append(np.absolute(new_csi[:][:][:]['real'] + 1.j*new_csi[:][:][:]['imag']))
        self.phase.append(np.unwrap(np.angle(new_csi[:][:][:]['real'] + 1.j*new_csi[:][:][:]['imag'])))

    def calc_phase_and_amp(self):
        # Don't compute dbm value (10*np.log10) for amplitude in favour of better visualization performance
        for i in range(len(self.csi)):
            self.amp.append(np.absolute(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag']))
            self.phase.append(np.unwrap(np.angle(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag'])))

        # If you want to compute the dbm value, you have to check the value before calling log10:
        #~ for i in range(len(self.csi)):
            #~ amplitudes_mat = []
            #~ csi = self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag']
            #~ for rx in range(self.headers[i]['nr'][0]):
                #~ amplitudes_row = []
                #~ for tx in range(self.headers[i]['nc'][0]):
                    #~ amplitudes_row.append([10*np.log10(np.absolute(subcarrier)) if np.absolute(subcarrier) > 1 else 0 for subcarrier in csi[rx][tx][:]])
                #~ amplitudes_mat.append(amplitudes_row)
            #~ self.amp.append(amplitudes_mat)
            #~ self.phase.append(np.unwrap(np.angle(csi)))

    def clear(self):
        self.path = None
        self.csi.clear()
        self.headers.clear()
        self.amp.clear()
        self.phase.clear()

    def set_path(self, path):
        csi_file = Path(path)
        if csi_file.is_file():
            self.path = csi_file
        else:
            raise Exception("No file to load has been selected. Check file path.")

