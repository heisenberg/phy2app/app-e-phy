# ml-dl-fl
Machine learning related code that is used to develop a model from CSI measurement to extract features

## Directory Structure
* data: Default location for data, either commited or created by scripts
* ml_dl_fl: Src location for the package.
* notebooks: Jupyter notebooks with demos and the like.
* scripts: Scripts that form the entry points for users.

## Setup
* Create/activate a the python virtual env you intend to work in.
* Change into the ml-dl-fl folder (not the ml_dl_fl subfolder containing the sources).
* Install the **ml-dl-fl** package with `pip install -e `.

## Usage
* Create a phase dataset contained in a single .npy with running `scripts/prepare_phase_dataset.py`.
    This might take a while.
* First demo of a VAE for phase data is contained in the notebooks folder.