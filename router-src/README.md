# Compass openwrt router firmware

- initial compilation with
	```bash
	make -j6
	```
- change C code
- in ./submodules/openwrt build with
	```bash
	make package/csi_probing/{clean,compile}
	```
	-> the compiled binary is located at: openwrt/bin/packages/mips_24kc/custom_packages/csi_probing_(...).ipk
- scp this ipk to both routers
- opkg remove csi_probing
- opkg install csi_probing_(...).ipk
- run with
	```bash
	random_probing_server
	```
	and
	```bash
	random_probing_client
	```
- all in one command:
	```bash
	make package/csi_probing/{clean,compile} && scp bin/packages/mips_24kc/custom_packages/csi_probing_1.0-1_mips_24kc.ipk root@192.168.3.1:/root && scp bin/packages/mips_24kc/custom_packages/csi_probing_1.0-1_mips_24kc.ipk root@192.168.3.2:/root
	```

- currently flashed version works without GUI

## Network re-establishment with WPA2 encryption & reverse

- after measuring and generating the key, Router (Bob) and Client (Alice) restart with WPA2 configuration and the generated key as passphrase
- your laptop should then disconnect as the network will be secured by the passphrase
- Alice should then connect automatically
- the current ssh terminals for Alice and Bob will freeze (due to lost wifi connection), but you will still see the generated passphrase. Copy it and use it to connect to the encrypted wifi network! Without this password, you will not be able to connect to the network and re-open it
- After connecting, use these commands to remove WPA2:
	- first Alice! (otherwise you will not be able to connect to Alice again if Bob is changed first)
		```bash
		uci set wireless.@wifi-iface[0].encryption=none
		uci commit wireless
		/etc/init.d/network reload
		```
	- then Bob
		```bash
		uci set wireless.@wifi-iface[0].encryption=none
		uci commit wireless
		/etc/init.d/network reload
		```
	- after ~1 minute the network will be re-established without any encryption