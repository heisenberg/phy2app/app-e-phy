/* TODO: Needs refactoring. */

#include "csi_measure.h"
#include "csi.h"
#include "process_payload.h"
#include "../utils/time_calc.h"
#include "../utils/print.h"
#include "../utils/transceive.h"
#include "../keygen/cpp/parameter_estimator.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <syslog.h>
#include <math.h>
#include <complex.h>
#include <inttypes.h>
#include <fcntl.h>

/* Time to wait in between sending two packets. */
static struct timespec signal_delay = {
    #if (SIGNAL_DELAY_MUS < 1000000)
    .tv_sec = 0, .tv_nsec = SIGNAL_DELAY_MUS * 1000
    #else
    .tv_sec = 1, .tv_nsec = 0
    #endif
};

/**
 * Do non linear least squares curve fitting to estimate parameters for key generation from csi phase measurements.
 * \param csi_pkt Points to the struct that contains the csi for the parameter estimation.
 */
static void estimate_parameters(struct csi *csi_pkt)
{
    double complex complex_csi_value;
    struct COMPLEX (*csi_matrix)[3][56] = csi_pkt->matrix;
    double csi_phase_matrix[csi_pkt->hdr.nr][csi_pkt->hdr.nc][csi_pkt->hdr.num_tones];
    double parameters[5];

    // Caculate phase values and call parameter estimation function.
    memset(csi_phase_matrix, 0, sizeof(csi_phase_matrix));
    for(uint8_t rx_antenna = 0; rx_antenna < csi_pkt->hdr.nr; ++rx_antenna) {
        for(uint8_t tx_antenna = 0; tx_antenna < csi_pkt->hdr.nc; ++tx_antenna) {
            for(uint8_t subcarrier_idx = 0; subcarrier_idx < csi_pkt->hdr.num_tones; ++subcarrier_idx) {

                complex_csi_value = (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].real
                                  + (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].imag * I;

                csi_phase_matrix[rx_antenna][tx_antenna][subcarrier_idx] = carg(complex_csi_value);

            }
            estimate_csi_parameters(csi_phase_matrix[rx_antenna][tx_antenna], parameters);
        }
    }
}

void calculate_phase(struct csi *csi_pkt, double csi_phase_matrix[3][3][56])
{
    double complex complex_csi_value;
    struct COMPLEX (*csi_matrix)[3][56];

    csi_matrix = csi_pkt->matrix;
    for(uint8_t rx_antenna = 0; rx_antenna < csi_pkt->hdr.nr; ++rx_antenna) {
        for(uint8_t tx_antenna = 0; tx_antenna < csi_pkt->hdr.nc; ++tx_antenna) {
            for(uint8_t subcarrier_idx = 0; subcarrier_idx < csi_pkt->hdr.num_tones; ++subcarrier_idx) {

                complex_csi_value = (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].real
                                  + (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].imag * I;

                csi_phase_matrix[rx_antenna][tx_antenna][subcarrier_idx] = carg(complex_csi_value);
            }
        }
    }
}

/**
 * The csi device records csi for sounding physical protocol data unit (PPDU) packets.
 * The calibration bits in the HT control field in the 802.11 MAC header indicate PPDU packets.
 *
 * In the ath9k driver the not sounding bit (Bit 4, starting to count from 0) of the DMA Rx Descriptor Format word 4
 * can be checked to see if the Rx frame is a sounding PPDU (not sounding bit == 0).
 *
 * The csi device will record csi for all received frames. Because of that, the number of returned measurements can be
 * greater than the number of packets that had been sent to collect csi. Because of this the point in time where the
 * csi was recorded on one communication side might not match the point in time where the csi was recorded on the other
 * side anymore. Because of this the "garbage measurement" with no corresponding measurement might needs to be removed.
 *
 * \param csi_pkt Points to the struct where the csi data will be stored.
 * \param csi_pkt_len The return value of get_csi is stored here.
 * \param pld_size Maximum expected payload size (XXX: minimum 4).
 * \return Length of user payload.
 * */
inline static uint32_t get_csi_by_user_payload(struct csi *csi_pkt, int32_t *csi_pkt_len, uint32_t pld_size)
{
    static uint8_t payload[CSI_PAYLOAD_SIZE];
    int8_t msr_msg_found = 0;
    uint32_t pld_len = 0;
    int32_t csi_len;

    do {
        csi_len = get_csi(csi_pkt);
        if(csi_len > 0) {
            pld_len = get_user_payload(csi_pkt->payload, csi_len, payload, CSI_PAYLOAD_SIZE);

            if(pld_len == 0) {
                PRINT(LOG_WARNING, "Received packet without payload.\n");
            } else if(pld_len >= pld_size) {
                /* XXX: Payload should be flexible. */
                if(payload[0] == 'M' && payload[1] == 'S' && payload[2] == 'R' && payload[3] == 0) {
                    msr_msg_found = 1;
                } else {
                    PRINT(LOG_WARNING, "Found unexpected string in csi buffer while searching for payload.\n");
                }
            } else {
                PRINT(LOG_WARNING, "Packet in csi buffer has unusual payload size of %d bytes.\n", pld_len);
            }
        }
    } while(msr_msg_found == 0 && csi_len != 0);
    *csi_pkt_len = csi_len;

    return pld_len;
}

enum state check_measurement(struct csi *csi_pkt)
{
    enum state status;

    /* Check if measurement was taken and has correct antenna combination and error code. */
    if(/* csi_pkt[i_pkt] != NULL */
        //csi_pkt->hdr.nr == 3
        //&& csi_pkt->hdr.nc == 3
        csi_pkt->hdr.phyerr == 0)
    {
        /* TODO: Extract parameters to check for outliners. */
        if(1) {
            status = SIGNAL_OK;
        } else {
            status = SIGNAL_REPEAT;
        }
    } else {
        status = SIGNAL_REPEAT;
    }
    return status;
}

/**
 *
 */
void signal_repeat(int socket)
{
    uint8_t buffer[STATE_SIZE];

    DEBUG_PRINT("Measurement REPEAT.\n");
    /* Send repeat message. */
    msg_send(socket, (uint8_t const *) MSR_REPEAT_MSG, STATE_SIZE);
    /* Receive and discard repeat or ok message. */
    msg_recv(socket, buffer, STATE_SIZE);
}

/**
 *
 */
uint8_t signal_ok(int socket, uint32_t i_pkt)
{
    uint8_t buffer[STATE_SIZE];
    uint32_t bytes_received;

    /* Send ok message. */
    msg_send(socket, (uint8_t const *) MSR_OK_MSG, STATE_SIZE);
    /* Receive repeat or ok message. */
    bytes_received = msg_recv(socket, buffer, STATE_SIZE);
    if(bytes_received > 0) {
        if(buffer[0] == 'A' && buffer[1] == 'C' && buffer[2] == 'K') {
            DEBUG_PRINT("Measurement %d OK.\n", i_pkt);
            return 1;
        } else {
            DEBUG_PRINT("Measurement %d REPEAT.\n", i_pkt);
            return 0;
        }
    } else {
        PRINT(LOG_ERROR, "Control connection broken.\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * TODO: Fix timeout issues. Refactoring.
 *
 * \param
 * \param
 * \param
 * \param
 * \param
 */
void measure(struct csi csi_pkt[], uint32_t measures_to_take, struct sockaddr_in *src_addr, struct sockaddr_in *dst_addr, int msr_socket, int plot_socket)
{
	static char snd_buffer[MEASURE_BUF_SIZE];
	static char rcv_buffer[MEASURE_BUF_SIZE];		// Buffer to store received measuring messages
    //~ static struct csi csi_pkt = {0};            // User structure where csi data is saved

	int32_t bytes_received;				// Number of bytes received (user payload)
    int32_t csi_pkt_len;
    uint32_t pld_len;					// Length of user payload
	struct timespec snd_time;			// Time when a measuring packet was sent
    struct timespec start_recv_time;
	struct timespec rcv_time;			// Time when a measuring packet was received
	struct timespec remote_send_time;	// Time when the client sent a measuring packet
    struct timediff wait_delay;
    uint32_t pkt_index = 0;
    uint32_t msg_len = 0;
    uint32_t nr_timeouts = 0;

    DEBUG_PRINT("Entered measuring function.\n");

    if(open_csi_dev(O_RDONLY | O_NONBLOCK) == -1) {
		PRINT(LOG_WARNING, "Could not open CSI device.\n");
		exit(EXIT_FAILURE);
	}

    DEBUG_PRINT("Try to take %" PRIu32 " measurements.\n", measures_to_take);
    while(measures_to_take > 0) {
		memset(rcv_buffer, 0, MEASURE_BUF_SIZE);
		memset(snd_buffer, 0, MEASURE_BUF_SIZE);

        DEBUG_PRINT("Send string 'MSR' + current time.\n");
		clock_gettime(CLOCK_REALTIME, &snd_time);
		memcpy(snd_buffer, MEASURE_MSG, STATE_SIZE);
		memcpy(&snd_buffer[STATE_SIZE], &snd_time.tv_sec, sizeof(snd_time.tv_sec));
		memcpy(&snd_buffer[STATE_SIZE + sizeof(snd_time.tv_sec)], &snd_time.tv_nsec, sizeof(snd_time.tv_nsec));
		sendto(msr_socket, snd_buffer, STATE_SIZE + sizeof(snd_time.tv_sec) + sizeof(snd_time.tv_nsec), 0, (const struct sockaddr *) dst_addr, sizeof(struct sockaddr_in));

        DEBUG_PRINT("Wait to receive message to collect measured csi.\n");
        clock_gettime(CLOCK_REALTIME, &start_recv_time);
        do {
            bytes_received = recvfrom(msr_socket, (void*) rcv_buffer, MEASURE_BUF_SIZE-1, 0, (struct sockaddr *) src_addr, &msg_len);
            clock_gettime(CLOCK_REALTIME, &rcv_time);
            time_sub(&wait_delay, &start_recv_time, &rcv_time);
            if(wait_delay.time.tv_sec > 0 || wait_delay.time.tv_nsec > RCV_TIMEOUT_NS) {
                bytes_received = 0;
                PRINT(LOG_NOTICE, "Receive timed out after %ld.%09ld seconds.\n", wait_delay.time.tv_sec, wait_delay.time.tv_nsec);
                if(++nr_timeouts > measures_to_take) {
                    PRINT(LOG_NOTICE, "Terminate measure loop after %" PRIu32 " timeouts.\n", nr_timeouts);
                    measures_to_take = 0;
                }
            }
        } while(bytes_received == -1);

        DEBUG_PRINT("Check received payload of size %" PRIi32 ".\n", bytes_received);
		if(bytes_received == (int32_t) (STATE_SIZE + sizeof(snd_time.tv_sec) + sizeof(snd_time.tv_nsec))
            && rcv_buffer[0] == 'M' && rcv_buffer[1] == 'S' && rcv_buffer[2] == 'R' && rcv_buffer[3] == 0) {

            // XXX
            measures_to_take -= 1;

            /* XXX: Needs propper (de-)serialization. */
            memcpy(&remote_send_time, &rcv_buffer[STATE_SIZE], sizeof(remote_send_time));
            //~ remote_send_time.tv_sec = *((time_t*) &rcv_buffer[STATE_SIZE]);
            //~ remote_send_time.tv_nsec = *((long*) &rcv_buffer[STATE_SIZE + sizeof(time_t)]);

            #if 0
            struct timediff send_receive_delay;	// Time in between sending a MSR message and receiving a MSR message
            struct timediff propagation_delay;	// Propagation delay from client to server

            time_sub(&send_receive_delay, &rcv_time, &snd_time);
			time_sub(&propagation_delay, &rcv_time, &remote_send_time);

            DEBUG_PRINT("packet send away at  : %ld.%09ld\n", snd_time.tv_sec, snd_time.tv_nsec);
			DEBUG_PRINT("propagation delay    : %ld.%09ld\n", propagation_delay.time.tv_sec, propagation_delay.time.tv_nsec);
			DEBUG_PRINT("send_receive_delay   : %ld.%09ld\n", send_receive_delay.time.tv_sec, send_receive_delay.time.tv_nsec);
            #endif

            /* Collect csi measurement. */
            DEBUG_PRINT("Try to read csi.\n");
            struct timespec start_csi_get_time = {0};
            struct timespec current_csi_get_time = {0};
            clock_gettime(CLOCK_REALTIME, &start_csi_get_time);
            do {
                #if (PARSE_CSI_PACKET_BUFFER)
                get_csi_by_user_payload(&csi_pkt[pkt_index], &csi_pkt_len, STATE_SIZE + sizeof(remote_send_time.tv_sec) + sizeof(remote_send_time.tv_nsec));
                #else
                csi_pkt_len = get_csi(&csi_pkt[pkt_index]);
                #endif
                clock_gettime(CLOCK_REALTIME, &current_csi_get_time);
                time_sub(&wait_delay, &start_csi_get_time, &current_csi_get_time);
                if(wait_delay.time.tv_sec > 0 || wait_delay.time.tv_nsec > GET_CSI_TIMEOUT) {
                    DEBUG_PRINT("get_csi timedout.\n");
                    csi_pkt_len = 0;
                }
            } while(csi_pkt_len == -1);

            /* Do something with the csi measurement. */
            if(csi_pkt_len > 0) {
                #if 0
                DEBUG_PRINT("Packet number %4" PRIu32 ":\n", pkt_index);
                print_csi_header(&csi_pkt[pkt_index].hdr);
                #endif

                #if (CSI_MULTIFRAME_PLOT == 1)
                if(CSI_SEND_TS) {
                    memcpy(snd_buffer, &rcv_time.tv_sec, sizeof(rcv_time.tv_sec));
                    memcpy(&snd_buffer[sizeof(rcv_time.tv_sec)], &rcv_time.tv_nsec, sizeof(rcv_time.tv_nsec));
                    send(plot_socket, snd_buffer, sizeof(rcv_time.tv_sec) + sizeof(rcv_time.tv_nsec), 0);
                }
                DEBUG_PRINT("Send csi data to PC.\n");
                send(plot_socket, &csi_pkt[pkt_index].hdr, sizeof(csi_pkt[pkt_index].hdr), 0);   /* Send csi header to PC. */
                send(plot_socket, csi_pkt[pkt_index].matrix, csi_pkt[pkt_index].hdr.csi_len, 0); /* Send raw packet with csi matrix to PC. */
                #endif

                #if (ESTIMATE_PARAMETERS == 1)
                DEBUG_PRINT("Try to estimate parameters from csi measurement.\n");
                estimate_parameters(&csi_pkt[pkt_index]);
                #endif

                pkt_index += 1;
            } else {
                DEBUG_PRINT("Csi device is empty.\n");
            }

            /* Wait some time until next packet is send. */
            nanosleep(&signal_delay, NULL);

		} else if(bytes_received >= STATE_SIZE && strncmp(rcv_buffer, SYNCHRONIZE_STATE, STATE_SIZE) == 0) {
			PRINT(LOG_WARNING, "Received synchronize request before last csi packets arrived.\n");
		} else if(bytes_received == -1) {
            DEBUG_PRINT("csi_measure.c, line 213: bytes_received == -1.\n");
            PRINT(LOG_WARNING, "%s.\n", strerror(errno));
		} else if(bytes_received == 0) {
            PRINT(LOG_NOTICE, "No bytes received until timeout occured.\n");
        } else {
			PRINT(LOG_ERR, "Received broken message with size of %d bytes.\n", bytes_received);
		}
	}

	close_csi_dev();
    DEBUG_PRINT("Leaving measuring function.\n");
}
