#ifndef _MSR_COMMON_CSI_
#define _MSR_COMMON_CSI_

#include "csi.h"
#include "../utils/time_calc.h"

#include <stdint.h>
#include <time.h>

#define MSR_MEASURE_MSG                 0x01
#define MSR_START_MSG                   0x02
#define MSR_CONSENT_MSG                 0x04
#define MSR_CONSENSUS_MSG               0x08
#define MSR_CLOSE_MSG                   0x10

#define MSR_N_MSGS                        10
#define MSR_N_CANDIDATES        2*MSR_N_MSGS
#define MSR_SND_RCV_TOUT_SEC               0
#define MSR_SND_RCV_TOUT_USEC           5000 // 2000
#define MSR_T_COHERENCE_NS           1000000

#define PLOT_ALL_MSR                       0
#define PLOT_MSR                           0

struct ts_csi {
    struct csi csi_pkt;
    struct timespec ts;
};

struct consent_tuple {
    uint16_t server_idx;
    uint16_t client_idx;
    struct timediff td;
};

uint8_t msr_find_csi_data(uint8_t *pld, uint32_t pld_len, struct csi *csi_pkt, int32_t csi_pkt_len);
int8_t msr_validate_msg(uint8_t *msg, uint32_t size);

#endif
