/* Needs fixing. */

#include "csi.h"
#include "connections.h"
#include "../utils/daemon.h"
#include "../utils/print.h"
#include "../utils/time_calc.h"
#include "../utils/server.h"
#include "../utils/transceive.h"
#include "../keygen/cpp/parameter_estimator.h"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Constants for compile time configuration. */
#define USAGE_STRING    ("usage:\n\
                        simple_probing <#measurements> <delay mus> <ipv4 dest addr> <ipv4 iface addr>\n\
                        \n\
                        [ipv4 iface addr] is INADDR_ANY if not specified\n\
                        maximum number of packets is 99999\n\
                        maximum delay is 9.999.999 microseconds (10s)\n")
                        
#define MSR_BUF_SIZE    	256
#define CSI_SEND_TS     	0
#define PLOT            	0
#define ESTIMATE_PARAMETERS 0
#define RCV_TIMEOUT_NS  	5000000
#define MAX_TIMEOUTS        1000

/* Function prototypes. */
static void print_usage_and_exit(void);
static void run(uint32_t measures_to_take, uint32_t inter_pkt_delay_us, uint8_t start_communication, struct sockaddr_in *dst_addr, struct sockaddr_in *src_addr);
static void measure(uint32_t inter_pkt_delay_us, uint32_t measures_to_take, struct sockaddr_in *src_addr, struct sockaddr_in *dst_addr, int msr_socket, int plot_socket);
static uint32_t create_message(char *msg_buf, void *data);
static void plot(void *plot_data);
static void init_inter_pkt_delay(struct timespec *inter_pkt_delay, uint32_t inter_pkt_delay_us);
static void estimate_parameters(struct csi *csi_pkt);
static void server_start();
static void client_start(struct sockaddr_in *dst_addr);

struct plot_data {
    uint32_t plot_socket;
    int32_t csi_pkt_len;
    struct csi *csi_pkt;
    uint32_t pkt_index;
    char *snd_buffer;
    struct timespec *rcv_time;
};

/**
 * Calls sendto and recvfrom in a loop.
 */
int main(int argc, char** argv) {
    long int nr_packets;
    long int delay;
    long int start_communication;
    char *endptr;
    struct sockaddr_in src_addr = {0};  /* Address of wifi interface used to send messages. */
    struct sockaddr_in dst_addr = {0};  /* Address of receiver. */
    
    /* Sanity checks for user arguments. */
    if(argc == 6) {
        char *arg_nr_pkts = argv[1];
        char *arg_delay_mus = argv[2];
        char *arg_start_communication = argv[3];
        char *arg_dst_addr = argv[4];
        char *arg_iface_addr = argv[5];
        
        /* Check user arguments for number of packets to send and delay between sending.
         * TODO: Check if enough stack memory is available to store requested amount of measurements in buffer.
         * TODO: Write a function to check this. */
        if(strnlen(arg_nr_pkts, 6) > 5) {
            print_usage_and_exit();
        }
        if(strnlen(arg_delay_mus, 8) > 7) {
            print_usage_and_exit();
        }
        if(strnlen(arg_start_communication, 2) > 1) {
            print_usage_and_exit();
        }
        nr_packets = strtol(arg_nr_pkts, &endptr, 10);
        if((errno == ERANGE && (nr_packets == LONG_MAX || nr_packets == LONG_MIN)) 
        || (errno != 0 && nr_packets == 0)
        || (endptr == arg_nr_pkts) 
        || *endptr != '\0') {
            print_usage_and_exit();
        }
        delay = strtol(arg_delay_mus, &endptr, 10);
        if((errno == ERANGE && (delay == LONG_MAX || delay == LONG_MIN)) 
        || (errno != 0 && delay == 0)
        || (endptr == arg_delay_mus) 
        || *endptr != '\0') {
            print_usage_and_exit();
        }
        start_communication = (uint8_t) strtol(arg_start_communication, &endptr, 10);
        if((errno == ERANGE && (start_communication == LONG_MAX || start_communication == LONG_MIN))
        || (errno != 0 && start_communication == 0)
        || (endptr == arg_start_communication) 
        || *endptr != '\0') {
            print_usage_and_exit();
        }
        
        /* Check user argument for destination address and initialize it. */
        if(strnlen(arg_dst_addr, INET_ADDRSTRLEN) >= INET_ADDRSTRLEN || strnlen(arg_dst_addr, 7) < 7) {
            print_usage_and_exit();
        }
        if(inet_aton(arg_dst_addr, &dst_addr.sin_addr) != 1) {
            print_usage_and_exit();
        }
        dst_addr.sin_family = AF_INET;
        dst_addr.sin_port = htons(MEASURE_PORT);
    
        /* Check user argument for interface address and initialize it. */
        if(strnlen(arg_iface_addr, INET_ADDRSTRLEN) >= INET_ADDRSTRLEN || strnlen(arg_iface_addr, 7) < 7) {
            print_usage_and_exit();
        }
        if(inet_aton(arg_iface_addr, &src_addr.sin_addr) != 1) {
            print_usage_and_exit();
        }
        src_addr.sin_family = AF_INET;
        src_addr.sin_port = htons(MEASURE_PORT);
        
        /* Start packet exchange to collect channel state information. */
        run(nr_packets, delay, (uint8_t) start_communication, &dst_addr, &src_addr);
    } else {
        print_usage_and_exit();
    }
    
    exit(EXIT_SUCCESS);
}

static void print_usage_and_exit(void) {
    PRINT(LOG_ERR, USAGE_STRING);
    exit(EXIT_FAILURE);
}

/**
 * Prepare sockets for communication then call measure function.
 * 
 * \param measures_to_take Number of measurements to take.   
 * \param inter_pkt_delay_us Time to wait before sending a packet after a packet has been received.
 * \param dst_addr Destination address.
 * \param src_addr Source address.
 */
static void run(uint32_t measures_to_take, uint32_t inter_pkt_delay_us, uint8_t start_communication, struct sockaddr_in *dst_addr, struct sockaddr_in *src_addr) {
    int msr_socket;
    int remote_plot_socket;

    #if (RUN_AS_DAEMON)
    create_daemon();
    PRINT(LOG_NOTICE, "Simple probing daemon started.");
    #endif

    #if (PLOT)
    struct sockaddr_in remote_plot_addr = {0};
    #endif
        
    /* Wait for TCP start message. */
    if(start_communication != 0) {
        /* TODO: Better connection to plotting service.
        *       E.g.: Plotting service could listen at fixed port and return port to use for communication.
        *       Or  : Implement "higher level protocol" measure messages to multiplex data.
        */
        #if (PLOT)
        remote_plot_socket = connect_to_pc(&remote_plot_addr, REMOTE_PLOT_PORT_SERVER);
        #endif
        server_start();
    } else {
        #if (PLOT)
        remote_plot_socket = connect_to_pc(&remote_plot_addr, REMOTE_PLOT_PORT_CLIENT);
        #endif
        client_start(dst_addr);
    }

    msr_socket = measure_connect(src_addr);
    measure(measures_to_take, inter_pkt_delay_us, src_addr, dst_addr, msr_socket, remote_plot_socket);
	close(msr_socket);
    close(remote_plot_socket);
}

static void server_start()
{
    struct server *server_p = create_server(50001, NULL);
    handle_events();
    destroy_server(server_p);
}

static void client_start(struct sockaddr_in *dst_addr)
{
    int sock;
    int no_delay = 1;
    int priority = 6;
    struct sockaddr_in address = {0};
    const uint8_t buf[] = "START";
    
	address.sin_addr.s_addr = dst_addr->sin_addr.s_addr;
	address.sin_family = AF_INET;
    address.sin_port = htons(50001);
    
    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
	while(connect(sock, (struct sockaddr*) &address, sizeof(struct sockaddr_in)) == -1) {
		PRINT(LOG_WARNING, "Connecting to server failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
	}
    
    msg_send(sock, buf, sizeof(buf));
    close(sock);
}

/**
 * Exchange packets to measure channel state information.
 * 
 * \param inter_pkt_delay_us Time in microseconds to wait after a packet has been received.
 * \param measures_to_take Number of csi measurements to make.
 * \param src_addr Source address of the interface for sending packets.
 * \param dst_addr Destination address.
 * \param msr_socket Socket for packet exchange (measuring).
 * \param plot_socket Socket for plotting.
 */
static void measure(uint32_t measures_to_take, uint32_t inter_pkt_delay_us, struct sockaddr_in *src_addr, struct sockaddr_in *dst_addr, int msr_socket, int plot_socket) {
	char snd_buffer[MSR_BUF_SIZE];
	char rcv_buffer[MSR_BUF_SIZE];		    // Buffer to store received measuring messages
    struct csi csi_pkt[measures_to_take];   // User structure where csi data is saved
    struct timespec inter_pkt_delay;
    struct timespec start_recv_time;
	struct timespec rcv_time;			    // Time when a measuring packet was received
    struct timediff wait_delay;
    socklen_t addrlen;
    int32_t bytes_received;				    // Number of bytes received (user payload)
    int32_t csi_pkt_len;
    uint32_t msg_size;
    uint32_t nr_timeouts = 0;
    uint32_t nr_successive_timeouts = 0;
    uint32_t pkt_index = 0;
    #if (PLOT)
    struct plot_data plot_data = {
        .plot_socket = plot_socket,
        .csi_pkt_len = 0,
        .csi_pkt = csi_pkt,
        .pkt_index = 0,
        .snd_buffer = &snd_buffer[0],
        .rcv_time = &rcv_time
    };
    #endif

    init_inter_pkt_delay(&inter_pkt_delay, inter_pkt_delay_us);
	memset(&csi_pkt, 0, sizeof(csi_pkt));
	if(open_csi_dev(O_RDONLY | O_NONBLOCK) == -1) {
		PRINT(LOG_WARNING, "Could not open CSI device.\n");
		exit(EXIT_FAILURE);
	}
	PRINT(LOG_NOTICE, "Start measuring channel state information for %d packets.\n", measures_to_take);
	
    /* Exchange messages in a loop. */
	while(measures_to_take > 0) {
		memset(rcv_buffer, 0, MSR_BUF_SIZE);
		memset(snd_buffer, 0, MSR_BUF_SIZE);

        /* Send message. */
        msg_size = create_message(snd_buffer, NULL);
		sendto(msr_socket, snd_buffer, msg_size, 0, (const struct sockaddr *) dst_addr, sizeof(struct sockaddr_in));
        
        /* Receive message, terminate loop in case of reception timeout. */
        clock_gettime(CLOCK_REALTIME, &start_recv_time);
        do {
            bytes_received = recvfrom(msr_socket, (void*) rcv_buffer, MSR_BUF_SIZE-1, 0, (struct sockaddr *) src_addr, &addrlen);
            clock_gettime(CLOCK_REALTIME, &rcv_time);
            time_sub(&wait_delay, &start_recv_time, &rcv_time);
            if(wait_delay.time.tv_sec > 0 || wait_delay.time.tv_nsec > RCV_TIMEOUT_NS) {
                nr_timeouts++;
                nr_successive_timeouts++;
                bytes_received = 0;
                PRINT(LOG_NOTICE, "Receive timed out after %ld.%09ld seconds.\n", wait_delay.time.tv_sec, wait_delay.time.tv_nsec);
            }
        } while(bytes_received == -1);
		
        /* Retrieve channel state information. */
        if(bytes_received > 0) {
            nr_successive_timeouts = 0;            
            
            do {
                csi_pkt_len = get_csi(&csi_pkt[pkt_index]);
            } while(csi_pkt_len == -1);
            
            if(csi_pkt_len > 0) {
                #if (PLOT)
                plot_data.csi_pkt_len = csi_pkt_len;
                plot_data.pkt_index = pkt_index;
                plot(&plot_data);
                #endif

                measures_to_take--;
                pkt_index++;
            } else {
				PRINT(LOG_WARNING, "Csi device returned bytes_read <= 0\n");
				PRINT(LOG_INFO, "Length of csi_pkt.hdr.csi_len: %d\n", csi_pkt[pkt_index].hdr.csi_len);
			}
        } else if(bytes_received == -1) {
            PRINT(LOG_WARNING, "%s.\n", strerror(errno));
		} else if(bytes_received == 0) {
            PRINT(LOG_NOTICE, "No bytes received until timeout occured.\n");
            
            if(nr_successive_timeouts >= MAX_TIMEOUTS) {
                PRINT(LOG_NOTICE, "Too many timeouts: abort.\n");
                break;
            }
        }
		
        nanosleep(&inter_pkt_delay, NULL);
	}
    PRINT(LOG_NOTICE, "Measuring finished.\n");
    #if (ESTIMATE_PARAMETERS)
    for(uint32_t i = 0; i < pkt_index; i++) {
        PRINT(LOG_NOTICE, "Parameters for packet %d:\n", i);
        estimate_parameters(&csi_pkt[i]);
    }
    #endif
	close_csi_dev();
}

/**
 * Fill the payload buffer. The pointer data can be used to adapt the function with user specific data.
 * 
 * \param msg_buf Pointer to the payload buffer to send.
 * \param plot_data Pointer to user data of known format.
 * \return The length of the message in msg_buf.
 */
static uint32_t create_message(char *msg_buf, void *plot_data) {
    if(msg_buf != NULL) {
		if(plot_data != NULL) {
			// TODO: Fill msg_buf
			#if(0)
			PRINT(LOG_WARNING, "create_message: plot_data is NULL.\n");
			#endif
			return 1;
		}
		else {
			// TODO: Fill msg_buf
			return 1;
		}
    } else {
        PRINT(LOG_ERR, "Payload buffer is NULL.\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * Send data to PC/Laptop for plotting.
 * The pointer plot_data needs to be cast to a struct that contains the data that is to be plotted.
 *
 * \param data Pointer to user data of known format.
 */
static void plot(void *plot_data) {
    struct plot_data *data = (struct plot_data *) plot_data;
    
    if(data != NULL) {
        uint32_t plot_socket      = data->plot_socket;
        int32_t csi_pkt_len       = data->csi_pkt_len;
        struct csi *csi_pkt       = data->csi_pkt;
        uint32_t pkt_index        = data->pkt_index;
        char *snd_buffer          = data->snd_buffer;
        struct timespec *rcv_time = data->rcv_time;
        
        if(csi_pkt_len > 0) {
            /* timestamp */
            if(CSI_SEND_TS) {
                memcpy(snd_buffer, &rcv_time->tv_sec, sizeof(rcv_time->tv_sec));
                memcpy(&snd_buffer[sizeof(rcv_time->tv_sec)], &rcv_time->tv_nsec, sizeof(rcv_time->tv_nsec));
                send(plot_socket, snd_buffer, sizeof(rcv_time->tv_sec) + sizeof(rcv_time->tv_nsec), 0);
            }
            /* header */
            send(plot_socket, &csi_pkt[pkt_index].hdr, sizeof(csi_pkt->hdr), 0);
            /* raw packet with csi matrix */
            send(plot_socket, &csi_pkt[pkt_index].matrix, csi_pkt->hdr.csi_len, 0);
        }
    } else {
        PRINT(LOG_WARNING, "Logging is enabled but plot_data is NULL.\n");
    }
}

/**
 * Initialize the inter packet delay in nanoseconds.
 * 
 * \param inter_pkt_delay The structure that contains the delay.
 * \param inter_pkt_delay_us The delay specified in microseconds.
 */
static void init_inter_pkt_delay(struct timespec *inter_pkt_delay, uint32_t inter_pkt_delay_us) {
    if(inter_pkt_delay != NULL) {
        if(inter_pkt_delay_us < 1000000) {
            inter_pkt_delay->tv_sec = 0;
            inter_pkt_delay->tv_nsec = inter_pkt_delay_us * 1000;
        } else {
            inter_pkt_delay->tv_sec = inter_pkt_delay_us / 1000000;
            inter_pkt_delay->tv_nsec = (inter_pkt_delay_us % 1000000) * 1000;
        }
    } else {
        PRINT(LOG_ERR, "Error in simple_probing.c, init_wait_time(...).\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * Do non linear least squares curve fitting to estimate parameters for key generation from csi phase measurements.
 * 
 * \param csi_pkt Pointer to struct csi that contains the csi header and matrix.
 */
static void estimate_parameters(struct csi *csi_pkt) {
    double complex complex_csi_value;
    struct COMPLEX (*csi_matrix)[3][56] = csi_pkt->matrix;
    double csi_phase_matrix[csi_pkt->hdr.nr][csi_pkt->hdr.nc][csi_pkt->hdr.num_tones];
    double parameters[5];
    
    // Caculate phase values and call parameter estimation function.
    memset(csi_phase_matrix, 0, sizeof(csi_phase_matrix));
    for(uint8_t rx_antenna = 0; rx_antenna < csi_pkt->hdr.nr; ++rx_antenna) {
        for(uint8_t tx_antenna = 0; tx_antenna < csi_pkt->hdr.nc; ++tx_antenna) {
            for(uint8_t subcarrier_idx = 0; subcarrier_idx < csi_pkt->hdr.num_tones; ++subcarrier_idx) {
                
                complex_csi_value = (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].real
                                  + (double complex) csi_matrix[rx_antenna][tx_antenna][subcarrier_idx].imag * I;
                
                csi_phase_matrix[rx_antenna][tx_antenna][subcarrier_idx] = carg(complex_csi_value);
            
            }
            estimate_csi_parameters(csi_phase_matrix[rx_antenna][tx_antenna], parameters);
            PRINT(LOG_INFO, "%f, %f, %f, %f, %f\n", parameters[0], parameters[1], parameters[2], parameters[3], parameters[4]);
        }
    }
}
