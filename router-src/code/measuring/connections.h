#ifndef _CONNECTIONS_H_
#define _CONNECTIONS_H_

#include "connections.h"
#include <netinet/in.h>

#define SERVER_IP           "192.168.2.1"
#define CLIENT_IP           "192.168.2.2"
#define REMOTE_PLOT_IP    "192.168.3.214"
#define REMOTE_APP_IP     "192.168.2.198"

static uint32_t const MEASURE_PORT            = 50001;
static uint32_t const REMOTE_PLOT_PORT_SERVER = 51111;
static uint32_t const REMOTE_PLOT_PORT_CLIENT = 52222;
static uint32_t const REMOTE_APP_PORT = 51111;

int connect_tcp_server(struct sockaddr_in *);
int connect_tcp_client(struct sockaddr_in *);

/**
 * Setup a TCP socket.
 *
 * \param remote_addr Address to PC/Laptop.
 * \param port Port number to use.
 */
int connect_to_pc(struct sockaddr_in *remote_addr, uint32_t port);

/**
 * Setup a UDP socket.
 *
 * \param address Destination address.
 * \return Returns a UDP socket.
 */
int measure_connect(struct sockaddr_in *address);

int connect_to_app(struct sockaddr_in *remote_addr, uint32_t port);
int connect_to_sta(struct sockaddr_in *remote_addr, uint32_t port);

#endif
