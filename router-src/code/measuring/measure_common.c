#include "measure_common.h"
#include "process_payload.h"
#include "csi.h"

#include <string.h>
#include <stdint.h>

static uint8_t payload[CSI_PAYLOAD_SIZE];

uint8_t msr_find_csi_data(uint8_t *pld, uint32_t pld_len, struct csi *csi_pkt, int32_t csi_pkt_len) {
    uint16_t csi_pld_len;
    uint8_t data_found = 0;
    
    csi_pld_len = get_user_payload(csi_pkt->payload, csi_pkt_len, payload, CSI_PAYLOAD_SIZE);
    if(csi_pld_len > 0 && (uint32_t) csi_pld_len >= pld_len) {
        data_found = 1;
        // TODO: use two indices to still find a match in case payload is superset of pld_len.
        for(uint32_t i = 0; i < pld_len; i++) {
            if(payload[i] != pld[i]) {
                data_found = 0;
                break;
            }
        }
    }
    
    return data_found;
}

int8_t msr_validate_msg(uint8_t *msg, uint32_t size) {    
    // TODO: implement
    if(msg != NULL && size > 0) {}
    return 1;
}
