#ifndef _CSI_MEASURE_H
#define _CSI_MEASURE_H

#include "csi.h"
#include <stdint.h>
#include <netinet/in.h>

#define CONNECT_SERVER_TO_APP            0
#define CONNECT_CLIENT_TO_STA            0
#define CONNECT_SERVER_TO_PC            0
#define CONNECT_CLIENT_TO_PC            0
#define SEND_COLLECTED_CSI_TO_PC        0
#define CSI_MULTIFRAME_PLOT 	        0
#define CSI_SEND_TS                     0
#define PARSE_CSI_PACKET_BUFFER         0
#define ESTIMATE_PARAMETERS             0

#define MEASURE_BUF_SIZE              256
#define CONTROL_BUF_SIZE              256
#define JOB_DELAY_SEC                   0 // 0
#define JOB_DELAY_NSEC           20000000 // 20000000
#define RCV_TIMEOUT_NS          10000000000 // 100000000
#define GET_CSI_TIMEOUT          50000000 // 25000000
#define SIGNAL_DELAY_MUS              200 //500
#define MEASURES_TO_TAKE              128

enum state {SYNCHRONIZE, MEASURE, CHECK_MEASUREMENT, SIGNAL_OK, SIGNAL_REPEAT, GENERATE_KEY, VALIDATE_KEY, TERMINATE};

static int32_t const STATE_SIZE               = 4;
static char const *const SYNCHRONIZE_STATE    = "TIM";
static char const *const MEASURE_STATE        = "JOB";
static char const *const MEASURE_MSG          = "MSR";
static char const *const MSR_REPEAT_MSG       = "REP";
static char const *const MSR_OK_MSG           = "ACK";
static char const *const DEBUG_MSG            = "BUG";

void calculate_phase(struct csi *csi_pkt, double csi_phase_matrix[3][3][56]);
void measure(struct csi csi_pkt[], uint32_t, struct sockaddr_in *, struct sockaddr_in *, int, int);
enum state check_measurement(struct csi *csi_pkt);
void signal_repeat(int socket);
uint8_t signal_ok(int socket, uint32_t i_pkt);

#endif
