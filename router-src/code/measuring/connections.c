#include "connections.h"
#include "../timesync/timesync.h"
#include "../utils/print.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>

int connect_tcp_server(struct sockaddr_in *server_addr) {
    int listen_socket;
    int accept_socket;
    int priority = 6;
    int no_delay = 1;
    int reuse_addr = 1;

    memset(server_addr, 0, sizeof(struct sockaddr_in));

	inet_aton(SERVER_IP, &server_addr->sin_addr);
    server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(TIME_SYNC_PORT);

    listen_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(listen_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(listen_socket, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr)) == -1) {
        PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
    }

    if(bind(listen_socket, (struct sockaddr*) server_addr, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(listen(listen_socket, 1)) {
        PRINT(LOG_ERROR, "Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

	do {
		accept_socket = accept(listen_socket, 0, 0);
		if(accept_socket == -1) {
			PRINT(LOG_WARNING, "Accepting connection failed: %s.\n", strerror(errno));
		}
	} while(accept_socket == -1);

    /* Set socket options.
     * TCP_NODELAY : Turn off any Nagle-like algorithm.
     * SO_PRIORITY : Set the protocol-defined priority for all packets to be sent on this socket.
     *               Low priotity 0 to high priority 6 (7 with CAP_NET_ADMIN capability).
     *               Only works if network interface qdisc (queuing discipline) supports prioritys.
     */
    if(setsockopt(accept_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(accept_socket, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    close(listen_socket);

    return accept_socket;
}

int connect_tcp_client(struct sockaddr_in *server_addr) {
    int sock;
    int priority = 6;
    int no_delay = 1;

	*server_addr = (const struct sockaddr_in) {0};
	inet_aton(SERVER_IP, &server_addr->sin_addr);
	server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(TIME_SYNC_PORT);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(sock, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }

	while(connect(sock, (struct sockaddr*) server_addr, sizeof(struct sockaddr_in)) == -1) {
		PRINT(LOG_WARNING, "Connecting to server failed: %s.\n", strerror(errno));
	}

    return sock;
}

int measure_connect(struct sockaddr_in *address) {
    int server_msr_socket;
    int priority = 6;
    //~ struct timeval timeout;

    //~ timeout.tv_sec = SND_RCV_TIMEOUT_SEC;
    //~ timeout.tv_usec = SND_RCV_TIMEOUT_USEC;

    server_msr_socket = socket(PF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
    if(server_msr_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(server_msr_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    //~ if(setsockopt(server_msr_socket, SOL_SOCKET, SO_RCVTIMEO, (const void *) &timeout, sizeof(timeout)) == -1) {
        //~ //printf("setsockopt: %s.\n", strerror(errno));
    //~ }
    //~ if(setsockopt(server_msr_socket, SOL_SOCKET, SO_SNDTIMEO, (const void *) &timeout, sizeof(timeout)) == -1) {
        //~ //printf("setsockopt: %s.\n", strerror(errno));
    //~ }

    if(bind(server_msr_socket, (struct sockaddr*) address, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    return server_msr_socket;
}

int connect_to_pc(struct sockaddr_in *remote_addr, uint32_t port) {
    int res;
    int remote_plot_socket;

	inet_aton(REMOTE_PLOT_IP, &remote_addr->sin_addr);
	remote_addr->sin_family = AF_INET;
    remote_addr->sin_port = htons(port);

    remote_plot_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(remote_plot_socket == -1) {
        PRINT(LOG_ERROR, "Remote plot socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    do {
        res = connect(remote_plot_socket, (struct sockaddr*) remote_addr, sizeof(struct sockaddr_in));
        if(res == -1) {
            PRINT(LOG_WARNING, "Connecting to remote plot host failed: %s.\n", strerror(errno));
            usleep(250000);
        }
    } while(res == -1);

    return remote_plot_socket;
}

int connect_to_app(struct sockaddr_in *remote_addr, uint32_t port) {
    int res;
    int remote_app_socket;
    struct sockaddr_in client, app;

	//inet_aton(REMOTE_PLOT_IP, &remote_addr->sin_addr);
    inet_aton(SERVER_IP, &remote_addr->sin_addr);
	remote_addr->sin_family = AF_INET;
    remote_addr->sin_port = htons(port);

    remote_app_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(remote_app_socket == -1) {
        PRINT(LOG_ERROR, "Remote plot socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Bind
	if( bind(remote_app_socket, (struct sockaddr *) remote_addr, sizeof(struct sockaddr_in)) < 0)
	{
		// print the error message
		perror("bind failed. Error");
		return -1;
	}
	//puts("bind done");

    // Listen
	listen(remote_app_socket, 3);
	
	// Accept and incoming connection
	puts("Waiting for incoming connections...");
	int c = sizeof(struct sockaddr_in);
    int app_def = -1;
    int sta_def = -1;
	
	//accept connection from an incoming client
	int client_sock = accept(remote_app_socket, (struct sockaddr *)&client, (socklen_t*)&c);
    
	if (client_sock < 0)
	{
		perror("accept failed");
		return -1;
	}
	puts("Connection accepted");

    char i1[15];
    if(strcmp(inet_ntop(AF_INET, &client.sin_addr, i1, sizeof(i1)), REMOTE_APP_IP) == 0) {
        app_def = client_sock;
        puts("App connected. Waiting for STA to connect...");
        puts(inet_ntop(AF_INET, &client.sin_addr, i1, sizeof(i1)));
    }
    else {
        sta_def = client_sock;
        puts("STA connected. Waiting for app to start process...");
        puts(inet_ntop(AF_INET, &client.sin_addr, i1, sizeof(i1)));
    }

	int a = sizeof(struct sockaddr_in);
	
	//accept connection from app
	int app_sock = accept(remote_app_socket, (struct sockaddr *)&app, (socklen_t*)&a);
	if (app_sock < 0)
	{
		perror("accept failed");
		return -1;
	}
	puts("Connection accepted");
    
    char i2[15];
    if(strcmp(inet_ntop(AF_INET, &app.sin_addr, i2, sizeof(i2)), REMOTE_APP_IP) == 0) {
        app_def = client_sock;
        puts("App connected.");
        puts(inet_ntop(AF_INET, &app.sin_addr, i2, sizeof(i2)));
    }
    else {
        sta_def = client_sock;
        puts("STA connected.");
        puts(inet_ntop(AF_INET, &app.sin_addr, i2, sizeof(i2)));
    }
	
    int read_size;
    char app_message[100];
	// Receive a message from client
	while( (read_size = recv(app_sock, app_message, 2000, 0)) > 0 )
	{
        if(strncmp(app_message, "startAP", 7) == 0) {
            printf("Received startAP.\n");
            char *buff = "startSTA";
            write(client_sock, buff, strlen(buff));
            return 0;
        }
	}
    printf("Something went wrong.");
    return -1;
}

int connect_to_sta(struct sockaddr_in *remote_addr, uint32_t port) {
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
    char server_reply[100];
   
    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERVER_IP);
    servaddr.sin_port = htons(port);
   
    // connect the client socket to server socket
    while(connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) {
        printf("AP not found, retrying in 5 seconds\n");
        sleep(5);
    }
    printf("Connected to AP.\n");

    recv(sockfd, server_reply, sizeof(server_reply), 0);
    if(strncmp(server_reply, "startSTA", 8) == 0) {
        printf("Received startSTA.\n");
        return 0;
    }

    return -1;
}


char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;

        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;

        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }

    return s;
}