#include "../measure_common.h"
#include "../csi_measure.h"
#include "../csi.h"
#include "../connections.h"
#include "../process_payload.h"
#include "../../utils/message.h"
#include "../../utils/time_calc.h"

#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/net_tstamp.h>               // SO_FLAGS
#include <linux/errqueue.h>                 // struct scm_timestamping
//~ #include <linux/ktime.h>

// For performance testing purposes only
#define CONNECT_TO_PC				1
#define IGNORE_PAYLOAD				1
#define IGNORE_TIMESTAMP			1
#define DEBUG						1

#define BUFFER_SIZE					512

static void print_usage_and_exit(void);
static void csi_receiver(int argc, char** argv);
static void csi_sender(int argc, char** argv, long int nr_packets, long int delay);
static int socket_init(struct sockaddr_in *addr);

inline static void set_csi_timestamps(struct timespec *csi_rcv_old, struct timespec *csi_rcv_new, struct ts_csi *ts_csi_pkt);
inline static int process_payload(int32_t csi_pkt_len, struct ts_csi *ts_csi_pkt, struct timespec *remote_send_time, struct timespec *csi_rcv_old, struct timespec *csi_rcv_new);
inline static void process_kernel_timestamp(struct msghdr *msgh, int pc_socket, struct ts_csi *ts_csi_pkt, struct timespec *csi_rcv_old, struct timespec *csi_rcv_new);

/**
 * Application for one way csi measurements.
 * 
 * \param argc Number of arguments.
 * \param argv The zeroth argument is the name of the program (like always...).
 * 			   The first argument is the mode: -s for sending and -r for receiving.
 * 			   The remaining arguments depend on the chosen mode.
 * 		       Send mode:
 *  			   The second argument is the number of packets.
 *  			   The third argument is the time to wait after a packet was send.
 *	 	 		   The fourth argument is the destination address.
 *				   The fifth argument is the (optional) sending address. It can be used to select the interface for sending packets.
 * 			   Receive mode:
 * 				   The second argument is the receive address. It can be used to select the interface to receive packets.
 */
int main(int argc, char** argv) {
    long int nr_packets;
    long int delay;
    char *endptr;
    
    if(argc < 2) {
        print_usage_and_exit();
    }
    
    /* XXX: Check arguments and start csi_sender or csi_receiver (incomplete). */
    if((argc >= 2 && argc <= 3) && (strncmp(argv[1], "-r", 3) == 0 || strncmp(argv[1], "--receive", 10) == 0)) {
        if(argc == 3) {
            if(strnlen(argv[2], INET_ADDRSTRLEN) >= INET_ADDRSTRLEN || strnlen(argv[2], 7) < 7) {
                print_usage_and_exit();
            }
        }
        csi_receiver(argc, argv);
    }
    else if((argc >= 5 && argc <= 6) && (strncmp(argv[1], "-s", 3) == 0 || strncmp(argv[1], "--send", 7) == 0)) {
        if(argc == 6) {
            if(strnlen(argv[5], INET_ADDRSTRLEN) >= INET_ADDRSTRLEN || strnlen(argv[5], 7) < 7) {
                print_usage_and_exit();
            }
        }
        if(strnlen(argv[4], INET_ADDRSTRLEN) >= INET_ADDRSTRLEN || strnlen(argv[4], 7) < 7) {
            print_usage_and_exit();
        }
        if(strnlen(argv[2], 6) > 5) {
            print_usage_and_exit();
        }
        if(strnlen(argv[3], 8) > 7) {
            print_usage_and_exit();
        }
        nr_packets = strtol(argv[2], &endptr, 10);
        if((errno == ERANGE && (nr_packets == LONG_MAX || nr_packets == LONG_MIN)) 
        || (errno != 0 && nr_packets == 0)
        || (endptr == argv[2]) 
        || *endptr != '\0') {
            print_usage_and_exit();
        }
        delay = strtol(argv[3], &endptr, 10);
        if((errno == ERANGE && (nr_packets == LONG_MAX || nr_packets == LONG_MIN)) 
        || (errno != 0 && nr_packets == 0)
        || (endptr == argv[3]) 
        || *endptr != '\0') {
            print_usage_and_exit();
        }
        csi_sender(argc, argv, nr_packets, delay);
    }
    else {
        print_usage_and_exit();
    }
    
    exit(EXIT_SUCCESS);
}

/**
 * This function prints a usage string to the user.
 */
static void print_usage_and_exit(void) {
    printf("usage:\n\
        -r || --receive  [ipv4 iface addr]                                          recv pkts [from interface with addr]\n\
        -s || --send     <#packets> <delay mus> <ipv4 dest addr> [ipv4 iface addr]  send # pkts to dest addr\n\
        \n\
        [ipv4 iface addr] is INADDR_ANY if not specified\n\
        maximum number of packets is 99999\n\
        maximum delay is 9.999.999 microseconds (10s)\n");
        exit(EXIT_FAILURE);
}

/**
 * Receive packets and measure csi in a loop.
 * TODO: Refactor program signature.
 *  
 * \param argc
 * \param argv
 */
static void csi_receiver(int argc, char** argv) {
    int pc_socket;
    uint8_t msg_found;                                  // Flag to tell if message was found
    int32_t csi_pkt_len;                                // Length of csi packet
    uint32_t bytes_received = 0;                        // Number of bytes received
    struct sockaddr_in src_addr;                        // Address of sender
    uint8_t msg_buf[BUFFER_SIZE] = {0};                 // Buffer space for messages to receive
    uint8_t ctrl_buf[BUFFER_SIZE] = {0};                // XXX: Buffer space for control data in cmsghdr
    struct timespec csi_rcv_new = {0};
    struct timespec csi_rcv_old = {0};
    struct ts_csi ts_csi_pkt = {                        // Contains msg with payload, csi and user space reception ts
        .csi_pkt = {
            .hdr = {
                .csi_len = 4032,                        // Dummy data for testing purposes
                .nc = 3,                                // Dummy data for testing purposes
                .nr = 3,                                // Dummy data for testing purposes
                .num_tones = 56                         // Dummy data for testing purposes
            }
        }
    };
    struct sockaddr_in own_addr = {                     // Address of wifi interface to receive messages
        .sin_family = AF_INET,
        .sin_port = htons(MEASURE_PORT),
        .sin_addr = {
            .s_addr = htonl(INADDR_ANY)
        }
    };
    struct iovec iov = {                                // Vector buffer for messages to receive
        .iov_base = msg_buf,                            // Starting address
        .iov_len  = sizeof(msg_buf)                     // Number of bytes to transfer
    };                                 
    struct msghdr msgh = {                              // Structure for message with ancillary data
        .msg_name = &src_addr,                          // Optional address
        .msg_namelen = sizeof(struct sockaddr_in),      // Size of address
        .msg_iov = &iov,                                // Scatter/gather array
        .msg_iovlen = 1,                                // # elements in msg_iov
        .msg_control = ctrl_buf,                        // Ancillary data
        .msg_controllen = sizeof(ctrl_buf)              // Ancillary data buffer length
    };    
    #if (IGNORE_TIMESTAMP == 1)
        uint32_t count = 0;
        uint32_t bytes = 0;
    #endif
    
    #if (IGNORE_PAYLOAD == 0)
        struct timespec *remote_send_time;                  // User space time when message was send
    #endif
    
    #if (CONNECT_TO_PC == 1)
        struct sockaddr_in pc_addr;                         // Address of pc to send data for evaluation
    #endif
    //~ struct timediff ts_diff;                            // Difference of sent and receive timestamp
    
    if(argc == 3) {
        if(inet_aton(argv[2], &own_addr.sin_addr) != 1) {
            print_usage_and_exit();
        }
    }

	int csi_dev_fd = open_csi_dev(O_RDONLY | O_NONBLOCK);
	if(csi_dev_fd == -1) {
		printf("Could not open CSI device.\n");
		printf("%s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
    
    #if (CONNECT_TO_PC == 1)
        pc_socket = connect_to_pc(&pc_addr, REMOTE_PLOT_PORT_SERVER);
    #endif
    
    // Setup UDP socket for measuring
    if(argc == 3) {
        if(inet_aton(argv[2], &own_addr.sin_addr) != 1) {
            print_usage_and_exit();
        }
    }
    
    int udp_sock = socket_init(&own_addr);  /* Socket used to receive messages to get CSI. */
    
    printf("Waiting to receive packets from sender.\n");
    while(1) {
        bytes_received = recvmsg(udp_sock, &msgh, 0);
        clock_gettime(CLOCK_REALTIME, &ts_csi_pkt.ts);
        
        if(bytes_received) {			
            if(msgh.msg_flags & MSG_CTRUNC)
				printf("Control message buffer is too short to store all messages.\n");
            
            #if (IGNORE_PAYLOAD == 0)
                remote_send_time = (struct timespec *) &msg_buf[STATE_SIZE];
            #endif
            
            msg_found = 0;
            do {
                csi_pkt_len = get_csi(&ts_csi_pkt.csi_pkt);
                if(csi_pkt_len > 0) {
                    #if (IGNORE_PAYLOAD == 1)
                        set_csi_timestamps(&csi_rcv_old, &csi_rcv_new, &ts_csi_pkt);
                        msg_found = 1;
                    #else
                        msg_found = process_payload(csi_pkt_len, &ts_csi_pkt, remote_send_time, &csi_rcv_old, &csi_rcv_new);
                    #endif
                }
            } while(msg_found == 0 && (csi_pkt_len > 0 || csi_pkt_len == -1));
            
            //~ time_sub(&ts_diff, &ts_csi_pkt.ts, remote_send_time);
            //~ printf("user snd to user rcv dt: %ld.%09ld\n", ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
            
            #if (IGNORE_TIMESTAMP == 1)
                #if (CONNECT_TO_PC == 1)
                    #if DEBUG == 1
                    printf("Send %d bytes header to PC.\n", sizeof(ts_csi_pkt.csi_pkt.hdr));
                    #endif
                    send(pc_socket, &ts_csi_pkt.csi_pkt.hdr, sizeof(ts_csi_pkt.csi_pkt.hdr), 0);
                    
                    #if DEBUG == 1
                    printf("Send %d bytes csi matrix to PC.\n", ts_csi_pkt.csi_pkt.hdr.csi_len);
                    #endif
                    send(pc_socket, ts_csi_pkt.csi_pkt.matrix, ts_csi_pkt.csi_pkt.hdr.csi_len, 0);
				#endif
                    count++;
                    bytes += bytes_received;
                    if(count % 100 == 0) printf("%d %d\n", count, bytes);                
            #else
                process_kernel_timestamp(&msgh, pc_socket, &ts_csi_pkt, &csi_rcv_old, &csi_rcv_new);
            #endif
        }
    }
    
    close_csi_dev();
}

/**
 * Send ht packets to measure csi.
 * 
 * TODO: refactor argc and argv parameters.
 * 
 * \param argc
 * \param argv
 * \param nr_packets Number of packets to send.
 * \param delay Time to wait after a packet has been sent.
 */
static void csi_sender(int argc, char** argv, long int nr_packets, long int delay) {
    ssize_t bytes_sent;                                 // Number of bytes sent
    struct timespec snd_time;                           // User space time of sending message
    uint8_t msg_buf[STATE_SIZE +                        // Buffer space for messages to receive
                    sizeof(snd_time.tv_sec) + 
                    sizeof(snd_time.tv_nsec)];
    //~ uint8_t msg_buf[BUFFER_SIZE];
    struct sockaddr_in own_addr = {                     // Address of wifi interface used to send messages
        .sin_family = AF_INET,
        .sin_port = htons(0),
        .sin_addr = {
            .s_addr = htonl(INADDR_ANY)
        }
    };
    struct sockaddr_in dst_addr = {                     // Address of receiver
        .sin_family = AF_INET,
        .sin_port = htons(MEASURE_PORT),
        .sin_addr = {
            .s_addr = 0                                 // Address is assigned later
        }
    };                                
    struct iovec iov = {                                // Vector buffer for messages to send
        .iov_base = msg_buf,                            // Starting address
        .iov_len  = sizeof(msg_buf)                     // Number of bytes to transfer
    };
    struct msghdr msgh = {                              // Structure for message with ancillary data
        .msg_name = &dst_addr,                          // Optional address
        .msg_namelen = sizeof(struct sockaddr_in),      // Size of address
        .msg_iov = &iov,                                // Scatter/gather array
        .msg_iovlen = 1                                 // # elements in msg_iov
    };   
    
    memset(msg_buf, 0, sizeof(msg_buf));
    
    /* Initialize destination address. */
    if(inet_aton(argv[4], &dst_addr.sin_addr) != 1) {
        print_usage_and_exit();
    }

    /* Setup sender UDP socket */
    if(argc == 6) {
        if(inet_aton(argv[5], &own_addr.sin_addr) != 1) {
            print_usage_and_exit();
        }
    }
    int udp_sock = socket_init(&own_addr);
    
    /* Copy default string to msg_buf. */
    memcpy(msg_buf, MEASURE_MSG, STATE_SIZE);
    
    /* Sending loop. */
    printf("Send %ld packets to receiver.\n", nr_packets);
    for(long int turns = 0; turns < nr_packets; turns++) {
        clock_gettime(CLOCK_REALTIME, &snd_time);
        memcpy(&msg_buf[STATE_SIZE], &snd_time.tv_sec, sizeof(snd_time.tv_sec));
        memcpy(&msg_buf[STATE_SIZE + sizeof(snd_time.tv_sec)], &snd_time.tv_nsec, sizeof(snd_time.tv_nsec));
        bytes_sent = sendmsg(udp_sock, &msgh, 0);
        printf("bytes_sent: %d\n", bytes_sent);
        if(bytes_sent == -1) {
            printf("%s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        
        if(delay) usleep(delay);
    }
}

/**
 * Setup udp socket to receive kernel timestamps.
 * 
 * On some NICs the following additional flags might be available:
 * SOF_TIMESTAMPING_RX_HARDWARE  |  // "Request rx timestamps generated by the network adapter."
 * SOF_TIMESTAMPING_TX_HARDWARE  |  // "Request tx timestamps generated by the network adapter."
 * SOF_TIMESTAMPING_TX_SOFTWARE  |  // "Request tx timestamps (in device driver) when data leaves the kernel."
 * SOF_TIMESTAMPING_TX_SCHED     |  // "Request tx timestamps prior to entering the packet scheduler."
 * SOF_TIMESTAMPING_RAW_HARDWARE |  // "Report hardware timestamps as generated by SOF_TIMESTAMPING_TX_HARDWARE when available."
 * SOF_TIMESTAMPING_OPT_TX_SWHW  |  // "Request both hardware and software timestamps for outgoing packets when SOF_TIMESTAMPING_TX_HARDWARE and SOF_TIMESTAMPING_TX_SOFTWARE are enabled at the same time."
 * See https://www.kernel.org/doc/Documentation/networking/timestamping.txt for details.
 * 
 * \param addr
 */
static int socket_init(struct sockaddr_in *addr) {
    int udp_sock;
    int priority = 6;
    int enable = 1;
                                                    // https://www.kernel.org/doc/Documentation/networking/timestamping.txt
    int ts_flags = SOF_TIMESTAMPING_RX_SOFTWARE  |  // "Request rx timestamps (in device driver) when data enters the kernel."
                   SOF_TIMESTAMPING_SOFTWARE     |  // "Report any software timestamps when available."                   
                   SOF_TIMESTAMPING_OPT_ID       |  // "Generate a unique identifier along with each packet."
                   SOF_TIMESTAMPING_OPT_TSONLY   |  // "Applies to transmit timestamps only. Makes the kernel return the timestamp as a cmsg alongside an empty packet, as opposed to alongside the original packet."
                   0;
                   
    udp_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(udp_sock == -1) {
        printf("Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(setsockopt(udp_sock, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(udp_sock, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(udp_sock, SOL_SOCKET, SO_TIMESTAMPING, (const void *) &ts_flags, sizeof(ts_flags)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    
    if(bind(udp_sock, (struct sockaddr*) addr, sizeof(struct sockaddr_in)) == -1) {
        printf("Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    return udp_sock;
}

/**
 * TODO: Comment.
 * 
 * \param csi_rcv_old
 * \param csi_rcv_new
 * \param ts_csi_pkt
 */
inline static void set_csi_timestamps(struct timespec *csi_rcv_old, struct timespec *csi_rcv_new, struct ts_csi *ts_csi_pkt) {
    csi_rcv_old->tv_sec = csi_rcv_new->tv_sec;
    csi_rcv_old->tv_nsec = csi_rcv_new->tv_nsec;
    csi_rcv_new->tv_sec  = ts_csi_pkt->csi_pkt.hdr.tstamp / 1000000;
    csi_rcv_new->tv_nsec = (ts_csi_pkt->csi_pkt.hdr.tstamp % 1000000) * 1000;
}

/**
 * TODO: Comment.
 * 
 * \param csi_pkt_len 
 * \param ts_csi_pkt 
 * \param remote_send_time 
 * \param csi_rcv_old 
 * \param csi_rcv_new 
 */
inline static int process_payload(int32_t csi_pkt_len, struct ts_csi *ts_csi_pkt, struct timespec *remote_send_time, struct timespec *csi_rcv_old, struct timespec *csi_rcv_new) {
    uint8_t payload[CSI_PAYLOAD_SIZE];                  // Buffer for usr payload in csi msg
    uint32_t pld_len;					                // Length of user payload
    struct timespec *remote_send_time_cpy;              // User space time when message was send
    int msg_found = 0;
    
    pld_len = get_user_payload(ts_csi_pkt->csi_pkt.payload, csi_pkt_len, payload, CSI_PAYLOAD_SIZE);
    if(pld_len == 0) {
        printf("Received packet without payload.\n");
    }
    else if(pld_len >= 4 + sizeof(ts_csi_pkt->ts.tv_sec) + sizeof(ts_csi_pkt->ts.tv_nsec)) {
        if(payload[0] == 'M' && payload[1] == 'S' && payload[2] == 'R' && payload[3] == 0) {
            remote_send_time_cpy = (struct timespec *) &payload[STATE_SIZE];
            if(remote_send_time->tv_sec == remote_send_time_cpy->tv_sec && remote_send_time->tv_nsec == remote_send_time_cpy->tv_nsec) {
                //~ csi_rcv = (struct timespec) ktime_to_timespec((ktime_t) ts_csi_pkt->csi_pkt.hdr.tstamp);
                set_csi_timestamps(csi_rcv_old, csi_rcv_new, ts_csi_pkt);
                printf("MATCH\n");
                msg_found = 1;
            }
            else {
                printf("MISSMATCH\n");
            }
        }
        else {
            printf("Unexpected string of size: %d\n", pld_len);
            for(uint i = 0; i < pld_len && i < CSI_PAYLOAD_SIZE; i++) {
                printf("%c", payload[i]);
            }
            printf("\n");
        }
    }
    else {
        printf("Unexpected string of size: %d.\n", pld_len);
    }
    
    return msg_found;
}

/**
 * TODO: Comment
 * 
 * \param msgh
 * \param pc_socket
 * \param ts_csi_pkt
 * \param csi_rcv_old
 * \param csi_rcv_new
 */
inline static void process_kernel_timestamp(struct msghdr *msgh, int pc_socket, struct ts_csi *ts_csi_pkt, struct timespec *csi_rcv_old, struct timespec *csi_rcv_new) {
    static uint32_t pkt_count = 1;                      // Keeps track of number of packets received
    static struct timespec kernel_rcv_old = {           // Used to keep track of time between messages
        .tv_sec = 0,
        .tv_nsec = 0
    };
    struct timespec kernel_rcv_new = {                  // Used to keep track of time between messages
        .tv_sec = 0,
        .tv_nsec = 0
    };
    struct cmsghdr *cmsg;                               // Contains protocol control-related messages or miscellaneous ancillary data.
    struct scm_timestamping *scm_ts;                    // Pointer to struct of up to three ts in cmsg
    struct timediff ts_diff;                            // Difference of sent and receive timestamp
    
    /* man recvmsg: "Ancillary data should be accessed only by the macros defined in cmsg(3)" */
    for(cmsg = CMSG_FIRSTHDR(msgh); cmsg != NULL; cmsg = CMSG_NXTHDR(msgh, cmsg)) {
        if(cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_RECVERR) continue;
        if(cmsg->cmsg_level != SOL_SOCKET) continue;

        switch(cmsg->cmsg_type) {
            case SO_TIMESTAMPING:
                scm_ts = (struct scm_timestamping *) CMSG_DATA(cmsg);
                memcpy(&kernel_rcv_new, &scm_ts->ts[0], sizeof(kernel_rcv_new));
                
                #if (DEBUG == 1)
				printf("Packet nr.: %d\n", pkt_count++);
				
					//~ time_sub(&ts_diff, &kernel_rcv_new, remote_send_time);
					//~ printf("user snd to kernel rcv dt %d: %ld.%09ld\n", pkt_count, ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
					//~ time_sub(&ts_diff, &ts_csi_pkt.ts, &kernel_rcv_new);
					//~ printf("kernel rcv to user rcv dt %d: %ld.%09ld\n", pkt_count, ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
				
				//~ time_sub(&ts_diff, csi_rcv_new, csi_rcv_old);
				//~ printf("csi    rcv new:           %ld.%09ld\n", csi_rcv_new->tv_sec, csi_rcv_new->tv_nsec);
				//~ printf("csi    rcv old to new dt    : %ld.%09ld\n", ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
			
					//~ printf("csi    rcv ts : %llu\n", ts_csi_pkt.csi_pkt.hdr.tstamp);
			
				//~ time_sub(&ts_diff, &kernel_rcv_new, &kernel_rcv_old);
				//~ printf("kernel rcv new    : %ld.%09ld\n", kernel_rcv_new.tv_sec, kernel_rcv_new.tv_nsec);
				//~ printf("kernel rcv old to new dt    : %ld.%09ld\n\n", ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
				#endif
                
                /* Update timestamp. */
                memcpy(&kernel_rcv_old, &scm_ts->ts[0], sizeof(kernel_rcv_old));
                
                // TODO: remove this part from this function
                #if (CONNECT_TO_PC == 1)
				//~ memcpy(snd_buffer, &kernel_rcv_new.tv_sec, sizeof(kernel_rcv_new.tv_sec));
				//~ memcpy(&snd_buffer[sizeof(kernel_rcv_new.tv_sec)], &kernel_rcv_new.tv_nsec, sizeof(kernel_rcv_new.tv_nsec));
				//~ send(pc_socket, snd_buffer, sizeof(snd_buffer), 0);
				send(pc_socket, &ts_csi_pkt->csi_pkt.hdr, sizeof(ts_csi_pkt->csi_pkt.hdr), 0);
				send(pc_socket, ts_csi_pkt->csi_pkt.matrix, ts_csi_pkt->csi_pkt.hdr.csi_len, 0);
                #endif
                break;
            default:
                break;
        }
    }
}
