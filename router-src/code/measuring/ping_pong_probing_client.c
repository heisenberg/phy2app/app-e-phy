/* Needs fixing. */

#include "measure_common.h"
#include "csi.h"
#include "connections.h"
#include "../utils/message.h"
#include "../utils/transceive.h"
#include "../utils/ringbuffer.h"
#include "../utils/time_calc.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#define MSR_TURNS           1
#define MSR_TURN_DELAY_MUS  3000000
#define SORT_CANDIDATES     1

static void msr_ping(void);
static void msr_measure(int measure_socket, int consent_socket, int plot_socket, struct ts_csi *ts_csi_pkt_buf);
static uint16_t msr_consent(int consent_socket, int measure_socket, int plot_socket, struct sockaddr_in *msr_server_addr, struct ts_csi *ts_csi_pkt_buf);
static uint16_t msr_find_consensus(int sockfd, struct ts_csi *ts_csi_pkt_buf, struct consent_tuple *best_consensus, struct consent_tuple *candidates);
static int compare_candidates(const void *candidate_1, const void *candidate_2);

int main(void) {
    if(open_csi_dev(O_RDONLY | O_NONBLOCK) == -1) {
		PRINT(LOG_WARNING, "Could not open CSI device.\n");
		exit(EXIT_FAILURE);
	}
    msr_ping();
    close_csi_dev();

    return 0;
}

void msr_ping(void) {
    int consent_socket;
    int measure_socket;
    int plot_socket;
    struct sockaddr_in plot_server_addr;
    struct sockaddr_in msr_server_addr;
    struct sockaddr_in msr_client_addr;
    struct ts_csi ts_csi_pkt_buf[MSR_N_MSGS];
    fd_set active_fd_set;
    fd_set read_fd_set;
    uint16_t n_candidates;
    
    memset(&ts_csi_pkt_buf, 0, sizeof(ts_csi_pkt_buf));
    
    // Setup plot socket
    if(PLOT_MSR) {
        plot_socket = connect_to_pc(&plot_server_addr, REMOTE_PLOT_PORT_CLIENT);
    }
        
    for(int turns = 0; turns < MSR_TURNS; turns++) {
        printf("turn %d\n", turns);
        usleep(MSR_TURN_DELAY_MUS);
        
        // Setup consent TCP socket
        memset(&msr_server_addr, 0, sizeof(struct sockaddr_in));
        inet_aton(SERVER_IP, &msr_server_addr.sin_addr);    // TODO: SO_BINDTODEVICE
        msr_server_addr.sin_family = AF_INET;
        msr_server_addr.sin_port = htons(MEASURE_PORT);
        consent_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(consent_socket == -1) {
            printf("Socket creation failed: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        while(connect(consent_socket, (struct sockaddr*) &msr_server_addr, sizeof(struct sockaddr_in)) == -1) {
            printf("Connecting to server failed: %s.\n", strerror(errno));
            sleep(1);
        }
        
        // Setup measure UDP socket
        memset(&msr_client_addr, 0, sizeof(struct sockaddr_in));
        inet_aton(CLIENT_IP, &msr_client_addr.sin_addr);    // TODO: SO_BINDTODEVICE
        msr_client_addr.sin_family = AF_INET;
        msr_client_addr.sin_port = htons(MEASURE_PORT);
        measure_socket = socket(PF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
        if(measure_socket == -1) {
            printf("Socket creation failed: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        if(bind(measure_socket, (struct sockaddr*) &msr_client_addr, sizeof(struct sockaddr_in)) == -1) {
            printf("Socket binding failed: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        
        FD_ZERO(&active_fd_set);
        FD_SET(consent_socket, &active_fd_set);
        FD_SET(measure_socket, &active_fd_set);
        
        // TODO: controlled shutdown if server dies
        // TODO: make sure when reading from measure socket that UDP addr == TCP addr
        printf("Entering measure loop.\n");
        n_candidates = 0;
        while(n_candidates == 0) {
            read_fd_set = active_fd_set;
            // TODO: nfds = max fd + 1        
            if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
                printf("Select error: %s.\n", strerror(errno));
            }
            for(int fd_i = 0; fd_i < FD_SETSIZE; fd_i++) {
                if(FD_ISSET(fd_i, &read_fd_set)) {
                    if(fd_i == measure_socket) {
                        msr_measure(measure_socket, consent_socket, plot_socket, ts_csi_pkt_buf);   
                    }
                    else if(fd_i == consent_socket){
                        n_candidates = msr_consent(consent_socket, measure_socket, plot_socket, &msr_server_addr, ts_csi_pkt_buf);
                    }
                    else {
                        printf("Error: unkown fd is set.\n");
                        exit(EXIT_FAILURE);
                    }
                }
                fflush(stdout);
            }
        }
    
        // TODO: Signal server that connection can be closed
        if(close(consent_socket) == -1) printf("Error closing consent_socket: %s.\n", strerror(errno));
        if(close(measure_socket) == -1) printf("Error closing measure_socket: %s.\n", strerror(errno));
        fflush(stdout);
    }
}

static void msr_measure(int measure_socket, int consent_socket, int plot_socket, struct ts_csi *ts_csi_pkt_buf) {
    int32_t csi_pkt_len;
    uint32_t msg_size;
    uint8_t recv_buf[sizeof(struct msg)];
    uint8_t send_buf[sizeof(struct msg)];
    struct ts_csi ts_csi_pkt;
    struct msg msg_in;
    struct msg msg_out;
    struct timespec start_rcv_loop_ts;
    struct timespec current_loop_ts;
    struct timediff loop_timeout;
    struct sockaddr_in src_addr;
    socklen_t addr_len = sizeof(struct sockaddr_in);
    int32_t bytes_sent;
    ssize_t bytes_received = 0;
    uint32_t msgs_received = 0;
    
    // TODO: Better timeout behaviour
    // TODO: keep track of state to avoid beeing send into this loop unintentionally
    while(msgs_received < MSR_N_MSGS) {
        clock_gettime(CLOCK_REALTIME, &start_rcv_loop_ts);  // TODO: error handling
        do {
            clock_gettime(CLOCK_REALTIME, &current_loop_ts);    // TODO: error handling
            bytes_received = recvfrom(measure_socket, recv_buf, sizeof(struct msg), 0, (struct sockaddr *) &src_addr, &addr_len);
            if(bytes_received == -1) {
                if(errno == EAGAIN || errno == EWOULDBLOCK) {
                    time_sub(&loop_timeout, &start_rcv_loop_ts, &current_loop_ts);
                    if(loop_timeout.time.tv_sec > 0 || loop_timeout.time.tv_nsec > MSR_T_COHERENCE_NS) {
                        bytes_received = 0;
                        printf("Timeout after %ld.%09ld seconds.\n", loop_timeout.time.tv_sec, loop_timeout.time.tv_nsec);
                    }
                }
                else {
                    printf("Receive error occured in measurement loop: %s.\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
            }
            else if(bytes_received == 0) {
                printf("Received 0 bytes. Shutting down.\n");
                exit(EXIT_FAILURE);
            }
        } while(bytes_received == -1);
        
        if(bytes_received > 0) {
            if(deserialize_msg(&msg_in, recv_buf, (uint32_t) bytes_received, NULL)) { // TODO: handle deserialization error
                switch(msg_in.msg_type) {
                    case MSR_MEASURE_MSG:
                        
                        // Search for CSI packet that contains payload in CSI buffer
                        // TODO: make payload search optional
                        ts_csi_pkt.ts.tv_sec = current_loop_ts.tv_sec;
                        ts_csi_pkt.ts.tv_nsec = current_loop_ts.tv_nsec;
                        do {
                            clock_gettime(CLOCK_REALTIME, &current_loop_ts);    // TODO: error handling
                            time_sub(&loop_timeout, &start_rcv_loop_ts, &current_loop_ts);
                            if(loop_timeout.time.tv_sec > 0 || loop_timeout.time.tv_nsec > MSR_T_COHERENCE_NS) {
                                csi_pkt_len = 0;
                                printf("Timeout while searching for payload in CSI buffer\n");
                                fflush(stdout);
                                break;
                            }
                            csi_pkt_len = get_csi(&ts_csi_pkt.csi_pkt);
                        } while(csi_pkt_len != 0 && msr_find_csi_data(recv_buf, bytes_received, &ts_csi_pkt.csi_pkt, csi_pkt_len) == 0);
                        
                        // Store CSI data and send it to PC if PLOT_ALL_MSR is set.
                        if(csi_pkt_len > 0) {
                            printf("Recorded CSI for incoming packet %d.\n", msgs_received);
                            memcpy(&ts_csi_pkt_buf[msgs_received], &ts_csi_pkt, sizeof(struct ts_csi));
                        
                            // all measurements can be send instead of making a timestamp based selection
                            if(PLOT_MSR && PLOT_ALL_MSR) {
                                send(plot_socket, &ts_csi_pkt.csi_pkt.hdr, sizeof(ts_csi_pkt.csi_pkt.hdr), 0);
                                send(plot_socket, &ts_csi_pkt.csi_pkt.matrix, ts_csi_pkt.csi_pkt.hdr.csi_len, 0);
                            }
                        
                            msgs_received++;
                        }
                        
                        break;
                    default:
                        printf("Invalid message type!\n");
                        exit(EXIT_FAILURE);
                        break;
                }
            }
        }
        
        // Response
        if(msgs_received < MSR_N_MSGS) {
            msg_out.msg_type = MSR_MEASURE_MSG;
            clock_gettime(CLOCK_REALTIME, &msg_out.timestamp); // TODO: error handling
            msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));
            bytes_sent = sendto(measure_socket, send_buf, msg_size, 0, (struct sockaddr *) &src_addr, addr_len);
            if(bytes_sent == -1) {
                printf("%s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }            
        }
    }
    
    printf("Sending consent request.\n");
    msg_out.msg_type = MSR_CONSENT_MSG;
    clock_gettime(CLOCK_REALTIME, &msg_out.timestamp); // TODO: error handling
    msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));
    msg_send(consent_socket, send_buf, msg_size);
}

static int compare_candidates(const void *candidate_1, const void *candidate_2) {
    int result = 0;
    const struct consent_tuple *c_1 = (const struct consent_tuple *) candidate_1;
    const struct consent_tuple *c_2 = (const struct consent_tuple *) candidate_2;
    const struct timespec *ts1 = &c_1->td.time;
    const struct timespec *ts2 = &c_2->td.time;
    
    if(ts1->tv_sec < ts2->tv_sec) {
        result = -1;
    }
    else if(ts1->tv_sec == ts2->tv_sec) {
        if(ts1->tv_nsec < ts2->tv_nsec) {
            result = -1;
        }
        else if(ts1->tv_nsec == ts2->tv_nsec) {
            result = 0;
        }
        else {
            result = 1;
        }
    }
    else {
        result = 1;
    }
    
    return result;
}

static uint16_t msr_consent(int consent_socket, int measure_socket, int plot_socket, struct sockaddr_in *msr_server_addr, struct ts_csi *ts_csi_pkt_buf) {
    uint32_t msg_size;
    uint8_t recv_buf[sizeof(struct msg)];
    uint8_t send_buf[sizeof(struct msg)];
    struct msg msg_in;
    struct msg msg_out;
    struct consent_tuple candidates[MSR_N_CANDIDATES];
    struct consent_tuple *best_consensus = &candidates[0];
    uint8_t idx_buf[MSR_N_CANDIDATES * sizeof(candidates[0].server_idx)];
    int32_t bytes_sent;
    uint16_t n_candidates = 0;
    
    msg_size = msg_recv(consent_socket, recv_buf, sizeof(struct msg));
    if(deserialize_msg(&msg_in, recv_buf, msg_size, NULL)) { // TODO: handle deserialization error
        switch(msg_in.msg_type) {
            case MSR_START_MSG:
                printf("Start measurement.\n");
                msg_out.msg_type = MSR_MEASURE_MSG;
                clock_gettime(CLOCK_REALTIME, &msg_out.timestamp); // TODO: error handling
                msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));           
                bytes_sent = sendto(measure_socket, send_buf, msg_size, 0, (struct sockaddr *) msr_server_addr, sizeof(struct sockaddr_in));
                if(bytes_sent == -1) {
                    printf("%s.\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            
            case MSR_CONSENT_MSG:
                printf("Searching for consensus.\n");
                n_candidates = msr_find_consensus(consent_socket, ts_csi_pkt_buf, best_consensus, candidates); 
                if(n_candidates > 0) {                                        
                    // signal server that consensus has been found
                    msg_out.msg_type = MSR_CONSENSUS_MSG;
                    clock_gettime(CLOCK_REALTIME, &msg_out.timestamp); // TODO: error handling
                    msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));
                    msg_send(consent_socket, send_buf, msg_size);
                    
                    // send number of candidate indices to receive to server
                    idx_buf[0] = (uint8_t) n_candidates;
                    idx_buf[1] = (uint8_t) (n_candidates >> 8);
                    msg_send(consent_socket, idx_buf, 2);
                    
                    if(SORT_CANDIDATES) {
                        qsort(candidates, n_candidates, sizeof(struct consent_tuple), compare_candidates);
                    }
                    
                    printf("Candidates:\n");
                    for(int i = 0; i < n_candidates; i++) {
                        printf("(%03d,%03d): %ld.%09ld\n", candidates[i].client_idx, candidates[i].server_idx, candidates[i].td.time.tv_sec, candidates[i].td.time.tv_nsec);
                    }

                    // send (sorted) list of candidate indices to server
                    for(uint16_t i = 0; i < n_candidates; i++) {
                        idx_buf[i*2] = (uint8_t) candidates[i].server_idx;
                        idx_buf[i*2 + 1] = (uint8_t) (candidates[i].server_idx >> 8);
                    }
                    msg_send(consent_socket, idx_buf, n_candidates * sizeof(candidates[0].server_idx));
                    
                    // Send all candidates to PC
                    if(PLOT_MSR && !PLOT_ALL_MSR) {
                        for(uint16_t i = 0; i < n_candidates; i++) {
                            send(plot_socket, &ts_csi_pkt_buf[candidates[i].client_idx].csi_pkt.hdr, sizeof(ts_csi_pkt_buf[candidates[i].client_idx].csi_pkt.hdr), 0);      // header
                            send(plot_socket, &ts_csi_pkt_buf[candidates[i].client_idx].csi_pkt.matrix, ts_csi_pkt_buf[candidates[i].client_idx].csi_pkt.hdr.csi_len, 0);   // raw packet with csi matrix
                        }
                    }
                }
                else {
                    printf("Could not find consensus.\n");
                    // TODO: restart
                    // TODO: restart
                    // TODO: restart
                    // TODO: restart
                }
                break;
                
            case MSR_CLOSE_MSG:
                printf("Received close message. Shutting down.\n");
                if(close(consent_socket) == -1) printf("Error closing consent_socket: %s.\n", strerror(errno));
                if(close(measure_socket) == -1) printf("Error closing measure_socket: %s.\n", strerror(errno));
                fflush(stdout);
                exit(EXIT_SUCCESS);
                break;
                
            default:
                printf("Invalid message type: %d!\n", msg_in.msg_type);
                break;
        }
    }
    
    return n_candidates;
}

// TODO: still return best pair in case none was taken within MSR_T_COHERENCE_NS
static uint16_t msr_find_consensus(int sockfd, struct ts_csi *ts_csi_pkt_buf, struct consent_tuple *best_consensus, struct consent_tuple *candidates) {
    uint8_t ts_recv_buf[sizeof(struct timespec)];
    struct timespec ts[MSR_N_MSGS];
    struct timespec *recv_ts;
    struct timediff ts_diff;
    uint16_t server_idx = 0;
    uint16_t client_idx = 0;
    uint16_t candidate_idx = 0;
    uint16_t n_candidates = 0;
    
    for(uint32_t i = 0; i < MSR_N_MSGS; i++) {
        msg_recv(sockfd, ts_recv_buf, sizeof(ts_recv_buf));
        recv_ts = (struct timespec *) ts_recv_buf;
        //~ printf("%ld.%09ld\n", recv_ts->tv_sec, recv_ts->tv_nsec);
        ts[i].tv_sec  = recv_ts->tv_sec;
        ts[i].tv_nsec = recv_ts->tv_nsec;
    }
    
    while(server_idx < MSR_N_MSGS && client_idx < MSR_N_MSGS) {
        time_sub(&ts_diff, &ts[server_idx], &ts_csi_pkt_buf[client_idx].ts);
        
        // A measurement pair is a candidate for reconcilliation if it was taken within MSR_T_COHERENCE_NS
        //~ printf("%ld.%09ld\n", ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
        if(ts_diff.time.tv_sec == 0 && ts_diff.time.tv_nsec <= MSR_T_COHERENCE_NS) {
            if(candidate_idx < MSR_N_CANDIDATES) {
                candidates[candidate_idx].server_idx = server_idx;
                candidates[candidate_idx].client_idx = client_idx;
                memcpy(&candidates[candidate_idx].td, &ts_diff, sizeof(struct timediff));
                if(best_consensus->td.time.tv_nsec >= ts_diff.time.tv_nsec) {
                    best_consensus = &candidates[candidate_idx];
                    //~ printf("(%03d,%03d): %ld.%09ld <----\n", client_idx, server_idx, ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
                }
                //~ else {
                    //~ printf("(%03d,%03d): %ld.%09ld\n", client_idx, server_idx, ts_diff.time.tv_sec, ts_diff.time.tv_nsec);
                //~ }
                n_candidates++;
                candidate_idx++;
            }
            else {
                printf("Can't store more candidates.\n");
            }
        }
        (ts_diff.sign == -1) ? server_idx++ : client_idx++;
    }
    
    printf("Found %d candidates for reconcilliation.\n", n_candidates);    
    return n_candidates;
}
