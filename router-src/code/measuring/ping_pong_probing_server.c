/* Needs fixing. */

#include "measure_common.h"
#include "csi.h"
#include "connections.h"
#include "../utils/message.h"
#include "../utils/transceive.h"
#include "../utils/ringbuffer.h"    /* TODO: Stupid, broken implementation, use utils/queue.h instead or C stdlib... */
#include "../utils/time_calc.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>  // for int getrlimit(int resource, struct rlimit *rlim);
#include <fcntl.h>

static uint32_t pkt_count = 0;

inline static void msr_srv(int measure_socket, int plot_socket, struct ringbuffer *);

static int msr_new_connection(int listen_socket);
static int msr_probe_init(struct sockaddr_in *addr);
static void msr_start_srv(int consent_socket);
static int8_t msr_consent_srv(int consent_socket, int plot_socket, struct ringbuffer *);
static void msr_consent(int consent_socket, struct ringbuffer *csi_msg_rbuf);
static void msr_consensus(int consent_socket, int plot_socket, struct ringbuffer *csi_msg_rbuf);

void msr_service(void);

int main(void) {
    // Get stack size
    //~ struct rlimit rlim;    
    //~ getrlimit(RLIMIT_STACK, &rlim);
    //~ printf("Current stack size: %8lld bytes\n", rlim.rlim_cur);
    //~ printf("Max stack size    : %8lld bytes\n", rlim.rlim_max);
    
	if(open_csi_dev(O_RDONLY | O_NONBLOCK) == -1) {
		PRINT(LOG_WARNING, "Could not open CSI device.\n");
		exit(EXIT_FAILURE);
	}
    msr_service();
    close_csi_dev();
    
    return 0;
}

void msr_service(void) {
    int listen_socket;
    int measure_socket;
    int plot_socket;
    struct sockaddr_in plot_server_addr;
    struct sockaddr_in consent_service_addr;
    struct sockaddr_in server_msr_addr;
    fd_set active_fd_set;
    fd_set read_fd_set;
    // TODO: Server can store more messages in case of lost responses
    // TODO: define struct with userland csi receive ts
    uint8_t csi_msg_space[MSR_N_MSGS * sizeof(struct ts_csi)];  // Ringbuffer
    struct ringbuffer_node csi_msg_list[MSR_N_MSGS];            // Ringbuffer
    struct ringbuffer csi_msg_rbuf;                             // Ringbuffer
    int enable = 1;
    int consent_socket = -1;
    
    //~ printf("msg space         : %8d bytes\n", sizeof(csi_msg_space));
    //~ printf("msg list space    : %8d bytes\n", sizeof(csi_msg_list));
    
    // Setup plot socket
    if(PLOT_MSR) {
        plot_socket = connect_to_pc(&plot_server_addr, REMOTE_PLOT_PORT_SERVER);
    }
    
    // TCP socket for consent
    memset(&consent_service_addr, 0, sizeof(struct sockaddr_in));
	inet_aton(SERVER_IP, &consent_service_addr.sin_addr);
    consent_service_addr.sin_family = AF_INET;
    consent_service_addr.sin_port = htons(MEASURE_PORT);
    listen_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(listen_socket == -1) {
        printf("Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(setsockopt(listen_socket, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    if(bind(listen_socket, (struct sockaddr*) &consent_service_addr, sizeof(struct sockaddr_in)) == -1) {
        printf("Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(listen(listen_socket, 1)) {
        printf("Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    // UDP socket for measuring
    memset(&server_msr_addr, 0, sizeof(struct sockaddr_in));
    inet_aton(SERVER_IP, &server_msr_addr.sin_addr);
    server_msr_addr.sin_family = AF_INET;
    server_msr_addr.sin_port = htons(MEASURE_PORT);
    measure_socket = msr_probe_init(&server_msr_addr);
    
    FD_ZERO(&active_fd_set);
    FD_SET(listen_socket, &active_fd_set);
    FD_SET(measure_socket, &active_fd_set);
    
    printf("Entering service loop.\n");
    while(1) {
        read_fd_set = active_fd_set;
        
        // TODO: nfds = max fd + 1        
        if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
            printf("Select error: %s.\n", strerror(errno));
        }
        
        // TODO: possibility to close consent_socket
        // TODO: make sure when reading from measure socket that UDP addr == TCP addr
        for(int fd_i = 0; fd_i < FD_SETSIZE; fd_i++) {
            if(FD_ISSET(fd_i, &read_fd_set)) {
                if(fd_i == listen_socket) {
                    consent_socket = msr_new_connection(fd_i);
                    if(consent_socket != -1) {
                        FD_SET(consent_socket, &active_fd_set);
                        printf("New connection established.\n");
                        ringbuffer_init(MSR_N_MSGS, sizeof(struct ts_csi), csi_msg_space, &csi_msg_rbuf, (struct ringbuffer_node *const) &csi_msg_list);
                        msr_start_srv(consent_socket);
                    }
                    else {
                        // TODO: handle accept error
                        exit(EXIT_FAILURE);
                    }
                }
                else if(fd_i == measure_socket) {
                    printf("New service request: MEASURE\n");
                    fflush(stdout);
                    msr_srv(fd_i, plot_socket, &csi_msg_rbuf);
                }
                else {
                    printf("New service request: CONSENT\n");
                    fflush(stdout);
                    if(msr_consent_srv(fd_i, plot_socket, &csi_msg_rbuf) == 0) {
                        FD_CLR(consent_socket, &active_fd_set);
                        if(close(consent_socket) == -1) printf("Error closing consent_socket: %s.\n", strerror(errno));
                    }
                }
            }
            fflush(stdout);
        }
    }
}

static void msr_start_srv(int consent_socket) {
    uint32_t msg_size;
    uint8_t send_buf[sizeof(struct msg)];
    struct msg response;
    
    memset((void *) send_buf, 0, sizeof(send_buf));
    response.msg_type = MSR_START_MSG;
    if(clock_gettime(CLOCK_REALTIME, &response.timestamp) == -1) {
        printf("clock_gettime: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    msg_size = serialize_msg(send_buf, &response, sizeof(send_buf));
    if(msg_size > 0) {
        msg_send(consent_socket, send_buf, msg_size);
    }
    else {
        printf("Can't serialize start measurement message!\n");
        exit(EXIT_FAILURE);
    }
}

inline static void msr_srv(int measure_socket, int plot_socket, struct ringbuffer *csi_msg_rbuf) {
    uint32_t msg_size;
    int32_t csi_pkt_len;
    uint8_t recv_buf[sizeof(struct msg)];
    uint8_t send_buf[sizeof(struct msg)];
    struct msg msg_out;
    struct msg msg_in;
    struct ts_csi ts_csi_pkt;
    struct sockaddr_in src_addr;
    socklen_t addr_len = sizeof(struct sockaddr_in);
    struct timespec start_rcv_loop_ts;
    struct timespec current_loop_ts;
    struct timediff loop_timeout;
    int32_t bytes_sent;
    uint32_t bytes_received = 0;

    memset((void *) send_buf, 0, sizeof(send_buf));
    memset((void *) recv_buf, 0, sizeof(recv_buf));
    
    // TODO: timeout, falls client aufhört zu senden
    bytes_received = recvfrom(measure_socket, recv_buf, sizeof(struct msg), 0, (struct sockaddr *) &src_addr, &addr_len);
    clock_gettime(CLOCK_REALTIME, &ts_csi_pkt.ts);  // TODO: error handling
    // TODO: if(!timeout) {
    if(deserialize_msg(&msg_in, recv_buf, bytes_received, msr_validate_msg) == 1) {
        switch(msg_in.msg_type) {
            case MSR_MEASURE_MSG:
                msg_out.msg_type = MSR_MEASURE_MSG;
                clock_gettime(CLOCK_REALTIME, &msg_out.timestamp);  // TODO: error handling
                msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));

                bytes_sent = sendto(measure_socket, send_buf, msg_size, 0, (struct sockaddr *) &src_addr, addr_len);
                if(bytes_sent == -1) {
                    printf("%s.\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
                
                clock_gettime(CLOCK_REALTIME, &start_rcv_loop_ts);  // TODO: error handling
                do {
                    csi_pkt_len = get_csi(&ts_csi_pkt.csi_pkt);
                    clock_gettime(CLOCK_REALTIME, &current_loop_ts);    // TODO: error handling
                    time_sub(&loop_timeout, &start_rcv_loop_ts, &current_loop_ts);
                    if(loop_timeout.time.tv_sec > 0 || loop_timeout.time.tv_nsec > MSR_T_COHERENCE_NS) {
                        csi_pkt_len = 0;
                        printf("Timeout while searching for payload in CSI buffer\n");
                        fflush(stdout);
                        break;
                    }
                } while(csi_pkt_len != 0 && msr_find_csi_data(recv_buf, bytes_received, &ts_csi_pkt.csi_pkt, csi_pkt_len) == 0);
                
                if(csi_pkt_len > 0) {
                    printf("Recorded CSI for incoming packet %d.\n", ++pkt_count);
                    printf("Add ts to ringbuffer: %ld.%09ld\n\n", ts_csi_pkt.ts.tv_sec, ts_csi_pkt.ts.tv_nsec);
                    ringbuffer_add((struct ringbuffer *const) csi_msg_rbuf, (void const *const) &ts_csi_pkt);
                    
                    // Send data to PC
                    if(PLOT_MSR && PLOT_ALL_MSR) {
                        send(plot_socket, &ts_csi_pkt.csi_pkt.hdr, sizeof(ts_csi_pkt.csi_pkt.hdr), 0);
                        send(plot_socket, &ts_csi_pkt.csi_pkt.matrix, ts_csi_pkt.csi_pkt.hdr.csi_len, 0);
                    }
                }
                
                break;
            default:
                printf("Invalid message type!\n");
                exit(EXIT_FAILURE);
                break;
        }
    }
    else {
        // TODO: handle broken/invalid message
        printf("Invalid message type!\n");
        exit(EXIT_FAILURE);
    }
}

static int8_t msr_consent_srv(int consent_socket, int plot_socket, struct ringbuffer *csi_msg_rbuf) {
    uint32_t msg_size;
    uint8_t recv_buf[sizeof(struct msg)];
    struct msg msg_in;
    int8_t result = 1;

    memset((void *) recv_buf, 0, sizeof(recv_buf));
    
    // TODO: timeout, falls client aufhört zu senden!
    msg_size = msg_recv(consent_socket, recv_buf, sizeof(struct msg));
    
    if(msg_size > 0) {
        if(deserialize_msg(&msg_in, recv_buf, msg_size, msr_validate_msg) == 1) {
            
            printf("Message size: %d\n", msg_size);
            printf("Message type: %d\n", msg_in.msg_type);
            
            switch(msg_in.msg_type) {
                case MSR_CONSENT_MSG:
                    msr_consent(consent_socket, csi_msg_rbuf);
                    break;
                case MSR_CONSENSUS_MSG:
                    msr_consensus(consent_socket, plot_socket, csi_msg_rbuf);
                    break;
                default:
                    printf("Invalid message type!\n");
                    exit(EXIT_FAILURE);
                    break;
            }
        }
        else {
            // TODO: handle broken/invalid message
            printf("Invalid message type!\n");
            exit(EXIT_FAILURE);
        }
    }
    else {
        printf("Connection has been closed by client.\n");
        result = 0;
    }
    
    return result;
}

static void msr_consent(int consent_socket, struct ringbuffer *csi_msg_rbuf) {
    int i = 0;
    struct msg msg_out;
    struct ringbuffer_node *e;
    uint32_t msg_size;
    uint8_t send_buf[sizeof(struct msg) + MSR_N_MSGS * sizeof(struct timespec)];
    
    memset((void *) send_buf, 0, sizeof(send_buf));
    
    msg_out.msg_type = MSR_CONSENT_MSG;
    clock_gettime(CLOCK_REALTIME, &msg_out.timestamp);  // TODO: error handling
    msg_size = serialize_msg(send_buf, &msg_out, sizeof(send_buf));
    e = csi_msg_rbuf->head;
    do {
        //~ printf("%ld.%09ld\n", ((struct ts_csi *)e->element)->ts.tv_sec, ((struct ts_csi *)e->element)->ts.tv_nsec);
        memcpy(&send_buf[msg_size + i++ * sizeof(struct timespec)], &(((struct ts_csi *)e->element)->ts), sizeof(struct timespec));
        e = e->next;
    } while(e != csi_msg_rbuf->head);
    msg_send(consent_socket, send_buf, sizeof(send_buf));
}

static void msr_consensus(int consent_socket, int plot_socket, struct ringbuffer *csi_msg_rbuf) {
    // TODO: Way to signal that no consensus was found
    uint16_t candidates_idx[MSR_N_CANDIDATES];
    uint8_t idx_buf[MSR_N_CANDIDATES * sizeof(candidates_idx[0])];
    struct ringbuffer_node *e;
    uint16_t n_candidates = 0;
    
    printf("Consensus found!\n");
    
    // receive number of reconcilliation candidates to receive
    msg_recv(consent_socket, idx_buf, 2);
    n_candidates += idx_buf[0];
    n_candidates += idx_buf[1] << 8;
    
    printf("Waiting to receive %d reconcilliation candidate indices.\n", n_candidates);
    
    if(n_candidates <= MSR_N_CANDIDATES) {
        // receive indices of reconcilliation candidates (optionally sorted by message exchange delay)
        msg_recv(consent_socket, idx_buf, n_candidates*sizeof(candidates_idx[0]));
        
        // TODO: Handle endianess
        for(uint16_t i = 0; i < n_candidates; i++) {
            candidates_idx[i] = idx_buf[i*2] + (idx_buf[i*2 + 1] << 8);
            printf("Use CSI index: %d\n", candidates_idx[i]);
        }
        //~ printf("Use CSI that was recorded for message received at: %ld.%09ld\n", ((struct ts_csi *)e->element)->ts.tv_sec, ((struct ts_csi *)e->element)->ts.tv_nsec);
        
        // Send csi measurements to PC
        // TODO: better implementation
        if(PLOT_MSR && !PLOT_ALL_MSR) {
            for(uint16_t candidate = 0; candidate < n_candidates; candidate++) {
                e = csi_msg_rbuf->head;
                for(uint16_t idx = 0; idx < candidates_idx[candidate]; idx++) {
                    e = e->next;
                }
                send(plot_socket, &((struct ts_csi *)e->element)->csi_pkt.hdr, sizeof(((struct ts_csi *)e->element)->csi_pkt.hdr), 0);      // header
                send(plot_socket, &((struct ts_csi *)e->element)->csi_pkt.matrix, ((struct ts_csi *)e->element)->csi_pkt.hdr.csi_len, 0);   // raw packet with csi matrix
            }
        }
    }
    else {
        printf("Number of reconcillition candidate indices to receive is too long.\n");
    }
}

static int msr_new_connection(int listen_socket) {
    int accept_socket;
    
    // TODO: validate_request()
    accept_socket = accept(listen_socket, 0, 0);
    if(accept_socket == -1) {
        printf("Accepting connection failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    return accept_socket;
}

static int msr_probe_init(struct sockaddr_in *addr) {
    int server_msr_socket;
    int priority = 6;
    int enable = 1;
    
    server_msr_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(server_msr_socket == -1) {
        printf("Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(setsockopt(server_msr_socket, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(server_msr_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
    }
    
    if(bind(server_msr_socket, (struct sockaddr*) addr, sizeof(struct sockaddr_in)) == -1) {
        printf("Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    return server_msr_socket;
}
