#include "process_payload.h"

#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

//~ #define DEBUG   1

static inline uint8_t* handle_mac_frame(uint8_t const *const, int32_t const);
static inline uint16_t handle_ip_pkt(uint8_t const *const, int32_t const, uint8_t const **);
static inline uint16_t handle_sctp_pkt(uint8_t const *const, uint8_t const **, uint16_t);
static inline uint16_t handle_tcp_pkt(uint8_t const *const, uint8_t const **, uint16_t);
static inline uint16_t handle_udp_pkt(uint8_t const *const , uint8_t const **, uint16_t);
static inline bool ieee80211_is_data_present(uint16_t);
static inline bool ieee80211_is_data_qos(uint16_t);

uint16_t get_user_payload(uint8_t const *const raw_payload, int32_t const raw_len, uint8_t *const user_payload_buffer, uint16_t up_buf_len) {
	uint16_t user_payload_len;
	uint8_t const *ip_pkt;
	uint8_t const *user_payload;
	
	ip_pkt = NULL;
	user_payload = NULL;    
	user_payload_len = 0;
	
	ip_pkt = handle_mac_frame(raw_payload, raw_len);
	
	if(ip_pkt != NULL) {
		user_payload_len = handle_ip_pkt(ip_pkt, raw_len, &user_payload);        
        if(user_payload != NULL && user_payload_len > 0) {
            if(user_payload_len > up_buf_len) {
                //printf("\n\nWARNING: discovered unusal payload size of %d bytes. "
                    //~ "Setting payload size to %d bytes. \n\n", user_payload_len, up_buf_len);
                //fflush(stdout);
                user_payload_len = up_buf_len;
            }
            memcpy(user_payload_buffer, user_payload, user_payload_len);
        }
	}

	return user_payload_len;
}

static inline uint8_t* handle_mac_frame(uint8_t const *const mac_frame, int32_t const raw_len) {
	uint8_t *ip_pkt;
	struct ieee80211_ht_hdr const *mac_header;
	
    ip_pkt = NULL;
    
    if(raw_len > 0) {
        if((uint32_t) raw_len >= sizeof(struct ieee80211_ht_hdr const)) {
            mac_header = (struct ieee80211_ht_hdr const *) mac_frame;
            
            // "bit 6 is set to 1 in data subtypes that contain no Frame Body field" (IEEE Std 802.11 Part 11)
            if(ieee80211_is_data_present(mac_header->frame_control)) {
                
                //~ #ifdef DEBUG
                //~ //printf("frame control        : %2x\n", mac_header->frame_control);
                //~ //printf("duration / ID        : %2x\n", mac_header->duration_id);
                
                //~ for(uint8_t addr = 0; addr < 3; addr++) {
                    //~ //printf("addr %d               : ", addr+1);
                    
                    //~ for(uint8_t i = 0; i < ETH_ALEN; i++) {
                        //~ //printf("%2x ", mac_header->addr1[(addr*ETH_ALEN) + i]);
                    //~ }
                    //~ //printf("\n");
                //~ }
                //~ //printf("addr 4               : ");
                
                //~ for(uint8_t i = 0; i < ETH_ALEN; i++) {
                    //~ //printf("%2x ", mac_header->addr4[i]);
                //~ }
                //~ //printf("\n");
                //~ //printf("sequence control     : %2x\n", mac_header->seq_ctrl);
                //~ //fflush(stdout);
                //~ #endif
                
                if(ieee80211_is_data_qos(mac_header->frame_control)) {
                    ip_pkt = (uint8_t *) &mac_frame[sizeof(struct ieee80211_ht_hdr)];
                    
                    #ifdef DEBUG
                    //printf("QoS control          : %2x\n", mac_header->qos_ctrl);
                    //printf("HT control           : %2x\n", mac_header->ht_ctrl);
                    //fflush(stdout);
                    #endif
                }
            }
        }
    }
    
	return ip_pkt;
}

static inline uint16_t handle_ip_pkt(uint8_t const *const ip_pkt, int32_t const raw_len, uint8_t const **user_payload) {
	struct iphdr const *ip_header;
    uint8_t const *transport_pkt;
	uint16_t transport_pkt_len;
	uint16_t user_payload_len;
    uint16_t ip_hdr_len;
	
    user_payload_len = 0;
    *user_payload = NULL;	
    
    if(raw_len > 0) {
        if((uint32_t) raw_len >= sizeof(struct ieee80211_ht_hdr const) + sizeof(struct iphdr const)) {
            ip_header = (struct iphdr const *) ip_pkt;
            
            if(ip_header->version == 0x04) {
                ip_hdr_len = ((uint16_t) ip_header->ihl) * 4;
                transport_pkt_len = ip_header->tot_len - ip_hdr_len;	
                transport_pkt = (uint8_t const *) &ip_pkt[ip_hdr_len];
                
                #ifdef DEBUG
                //printf("transport_pkt addr   : %p\n", transport_pkt);
                //printf("ip_hdr_len           : %d\n", ip_hdr_len);
                //printf("tranport_pkt_len     : %d\n", transport_pkt_len);
                //fflush(stdout);
                #endif
                
                switch(ip_header->protocol) {
                    //~ case IPPROTO_SCTP:
                        //~ //printf("SCTP packet found in CSI packet buffer.\n");
                        //~ user_payload_len = handle_sctp_pkt(transport_pkt, user_payload, transport_pkt_len);
                        //~ break;
                
                    //~ case IPPROTO_TCP:
                        //~ #ifdef DEBUG
                        //~ //printf("TCP packet found in CSI packet buffer.\n");
                        //~ #endif
                        //~ user_payload_len = handle_tcp_pkt(transport_pkt, user_payload, transport_pkt_len);
                        //~ break;
                
                    case IPPROTO_UDP:
                        #ifdef DEBUG
                        //printf("UDP packet found in CSI packet buffer.\n");
                        #endif
                        user_payload_len = handle_udp_pkt(transport_pkt, user_payload, transport_pkt_len);
                        break;
                    
                    default:
                        break;
                }
            }
            else {
                //printf("Non IPv4 packet with length: %d\n", raw_len);
                for(uint32_t i = 0; i < sizeof(struct iphdr const); i++) {
                    if(i % 35 == 0) {
                        //printf("\n");
                    }
                    //printf("%02" PRIx8 " ", ip_pkt[i]);
                }
                //printf("\n");
            }
        }
    }

	return user_payload_len;
}

static inline uint16_t handle_tcp_pkt(uint8_t const *const tcp_pkt, uint8_t const **user_payload, uint16_t tcp_pkt_len) {
	uint16_t user_payload_len;
    uint8_t tcp_header_len;
	
	*user_payload = NULL;
	user_payload_len = 0;
	
	if(tcp_pkt != NULL && tcp_pkt_len > 0) {
        tcp_header_len = (((uint8_t const *) tcp_pkt)[12] & 0xF0) >> 2;
        user_payload_len = tcp_pkt_len - tcp_header_len;
        
        #ifdef DEBUG
        //printf("TCP header length   : %d\n", tcp_header_len);
        //printf("TCP payload length  : %d\n", user_payload_len);
        #endif
        
        *user_payload = &(((uint8_t const *)tcp_pkt)[tcp_header_len]);
    }

    return user_payload_len;
}

static inline uint16_t handle_udp_pkt(uint8_t const *const udp_pkt, uint8_t const **user_payload, uint16_t udp_pkt_len) {
	uint16_t user_payload_len;
	
	*user_payload = NULL;
	user_payload_len = 0;
	
	if(udp_pkt != NULL && udp_pkt_len > 0) {
        user_payload_len = ((((uint16_t)udp_pkt[4]) << 8) + udp_pkt[5]) - 8;
        
        #ifdef DEBUG
        //printf("UDP packet length   : %d\n", udp_pkt_len);
        //printf("UDP payload length  : %d\n", user_payload_len);
        #endif
        
        *user_payload = &(((uint8_t const *)udp_pkt)[8]);
    }

    return user_payload_len;
}

static inline uint16_t handle_sctp_pkt(uint8_t const *const sctp_pkt, uint8_t const **user_payload, uint16_t sctp_pkt_len) {
	int32_t debug;
	int32_t bytes_remaining;
	uint16_t user_payload_len;
	struct sctp_chunkhdr const *sctp_chunk;
	struct sctp_data_chunk const *data_chunk;
	
	*user_payload = NULL;
	user_payload_len = 0;
	
	if(sctp_pkt != NULL && sctp_pkt_len > 0) {
		bytes_remaining = sctp_pkt_len - sizeof(struct sctphdr);	
		sctp_chunk = (struct sctp_chunkhdr const *) &sctp_pkt[sizeof(struct sctphdr)];
		
		while(bytes_remaining > 0) {
			
			if(sctp_chunk->chunk_type == SCTP_DATA) {
				data_chunk = (struct sctp_data_chunk const *) sctp_chunk;
				debug = (int32_t) data_chunk->chunk_length - (int32_t) (sizeof(struct sctp_data_chunk) - sizeof(uint8_t *));
				
				if(debug > 0) {
					user_payload_len = data_chunk->chunk_length - (sizeof(struct sctp_data_chunk) - sizeof(uint8_t *));
				}
				else {
					//printf("\nProblem with SCTP Chunk. chunklen - data_chunk_hdr = %d, chunklen = %d, bytes_remaining = %d.\n", debug, data_chunk->chunk_length, bytes_remaining);
					
					for(int32_t i = 0; i < sctp_pkt_len; i++) {
						//printf("%2x ", ((uint8_t const *)sctp_pkt)[i]);
					}
					
					//printf("\n\n");
					
					for(int32_t i = 0; i < bytes_remaining; i++) {
						//printf("%2x ", ((uint8_t const *)sctp_chunk)[i]);
					}
					//fflush(stdout);
					user_payload_len = 0;
					
					// TODO: debug code remove this
					exit(EXIT_FAILURE);
				}
				
				if(user_payload_len > 0) {
					*user_payload = (uint8_t const *) &data_chunk->chunk_payload;
				}
				bytes_remaining = 0;
			}
			else {
                #ifdef DEBUG
                //printf("Found no data chunk: %d bytes. Skipping.\n", sctp_chunk->chunk_length);
                #endif
				
                bytes_remaining -= sctp_chunk->chunk_length;
                
                #ifdef DEBUG
                //printf("Remainder length: %d bytes.\n", bytes_remaining);
                #endif
				
                sctp_chunk = (struct sctp_chunkhdr const *) &((uint8_t const *) sctp_chunk)[sctp_chunk->chunk_length]; // next chunk
			}
		}
	}
	
	return user_payload_len;	
}

static inline bool ieee80211_is_data_present(uint16_t frame_control) {
	return (frame_control & (IEEE80211_FCTL_FTYPE | 0x40)) == IEEE80211_FTYPE_DATA;
}

static inline bool ieee80211_is_data_qos(uint16_t frame_control) {
	return (frame_control & (IEEE80211_FCTL_FTYPE | IEEE80211_STYPE_QOS_DATA)) == (IEEE80211_FTYPE_DATA | IEEE80211_STYPE_QOS_DATA);
}
