#ifndef __DAEMON__H__
#define __DAEMON__H__

/**
 * Turns the program into a daemon. 
 */
void create_daemon(void);

#endif
