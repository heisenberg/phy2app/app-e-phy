#include "daemon.h"

#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>

#define DAEMON_DIR      "/root/projects/autokey/code/measuring"
#define DAEMON_IDENT    "Probing daemon"

/**
 * Turns the program into a daemon. 
 */
void create_daemon(void) {
    pid_t pid;
    int res;
    
    pid = fork();
    // Fork failed.
    if(pid < 0) { exit(EXIT_FAILURE); }
    // Terminate parent process.
    if(pid > 0) { exit(EXIT_SUCCESS); }
    
    // The calling process will be the only process in the new process group 
    // and in the new session. The new session has no controlling terminal.
    // This dissociates the child from its terminal.
    pid = setsid();
    if(pid == -1) { exit(EXIT_FAILURE); }
    
    // TODO: handle/ignore signals
    // sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
    
    // Second fork ensures that grandchild is not session leader.
    pid = fork();
    // Second fork failed.
    if(pid < 0) { exit(EXIT_FAILURE); }
    // Terminate child.
    if(pid > 0) { exit(EXIT_SUCCESS); }
    
    // Now the grandchild is running as a daemon without console.
    // TODO: Set grandchilds file mode creation mask.
    // --> TODO: Privilege separation!

    // Change working directory to projects directory
    res = chdir(DAEMON_DIR);
    if(res != 0) { exit(EXIT_FAILURE); }
    
    // Close all open file descriptors
    for(long fd = sysconf(_SC_OPEN_MAX); fd >= 0; fd--) {
        close(fd);
    }
    
    openlog(DAEMON_IDENT, LOG_PID, LOG_DAEMON);
}
