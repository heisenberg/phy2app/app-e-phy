#include "queue.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>

inline static uint32_t increase(uint32_t index) {
    return ((index + 1) & Q_MAX_ELEMENTS) ? ++index : 0;
}

void init_queue(struct queue *q) {
    q->head = 0;
    q->last = 0;
}

uint8_t is_empty(struct queue *q) {
    return (q->head ^ q->last) ? 0 : 1;
}

inline uint8_t is_full(struct queue *q) {
	return (q->head ^ increase(q->last)) ? 0 : 1;
}

uint8_t enqueue(struct queue *q, Q_TYPE *element) {
    if(is_full(q)) {
        Q_PRINT("Queue is full.\n");
        return 0;
    } else {
        Q_PRINT("Copy %d to last at %p.\n", *element, (void*) &q->q[q->last]);
        memcpy(&q->q[q->last], element, sizeof(Q_TYPE));
        q->last = increase(q->last);
        return 1;
    }
}

uint8_t dequeue(struct queue *q, Q_TYPE *element) {
    if(is_empty(q)) {
        Q_PRINT("Queue is empty.\n");
        return 0;
    } else {
        memcpy(element, &q->q[q->head], sizeof(Q_TYPE));
        q->head = increase(q->head);
        return 1;
    }
}
