#include "message.h"

extern int8_t deserialize_msg(struct msg *dest, uint8_t *src, uint32_t src_size, int8_t (*validate)(uint8_t *, uint32_t));
extern uint32_t serialize_msg(uint8_t *dest, struct msg *src, uint32_t dst_size);
