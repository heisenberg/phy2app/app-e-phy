#ifndef _TIME_CALC_
#define _TIME_CALC_

#include <time.h>

#define MAX_NANOSLEEP_NSEC			999999999

struct timediff {
	struct timespec time;
	int sign;
};

/**
 * Substract two timespecs and store the result in a timediff.
 * */
void time_sub(struct timediff *, struct timespec const *const, struct timespec const *const);

/**
 * Add two timespecs, result is also in a timespec since it can't be negative.
 * */
void time_add(struct timespec *, struct timespec const *const, struct timespec const *const);

/**
 * Substract two timediffs and store the tesult in another timediff.
 * */
void timediff_substract(struct timediff *, struct timediff const *const, struct timediff const *const);

#endif
