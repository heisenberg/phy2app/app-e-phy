#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

#include <sys/types.h>
#include <stdint.h>

// declare to use:
//~ uint8_t rbuf_space[n * sizeof(struct )];
//~ struct ringbuffer_node rbuf_list[n];
//~ struct ringbuffer rbuf;

struct ringbuffer {
    uint8_t  shift;
    uint16_t n_elements;
    uint16_t element_size;
    uint32_t space;
    uint8_t  *space_loc;
    struct ringbuffer_node *rbuf_list;
    struct ringbuffer_node *head;
    struct ringbuffer_node *end;
};

struct ringbuffer_node {
    void *element;
    struct ringbuffer_node *next;
};

void ringbuffer_init(uint16_t n, uint16_t element_size, uint8_t *const rbuf_space, struct ringbuffer *const rbuf, struct ringbuffer_node *const rbuf_list);
void ringbuffer_add(struct ringbuffer *const rbuf, void const *const element);
void ringbuffer_switch(struct ringbuffer_node *n1, struct ringbuffer_node *n2);
void ringbuffer_iterate(struct ringbuffer *const rbuf, void (*f)(void*));

#endif
