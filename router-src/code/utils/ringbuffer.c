#include "ringbuffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ringbuffer_init(uint16_t n, uint16_t element_size, uint8_t *const rbuf_space, struct ringbuffer *const rbuf, struct ringbuffer_node *const rbuf_list) {
    if(rbuf_space == NULL) {
        printf("Ringbuffer space was not specified.\n");
        exit(EXIT_FAILURE);
    }
    if(n <= 1) {
        printf("Ringbuffer size must be at least twice as big as element size\n");
        exit(EXIT_FAILURE);
    }

    rbuf->space = n * element_size;
    if(rbuf->space % n != 0) {
        printf("Ringbuffer size must be multiple of element size.\n");
        exit(EXIT_FAILURE);
    }
    
    rbuf->shift = 0;
    rbuf->n_elements = n;
    rbuf->element_size = element_size;
    rbuf->space_loc = rbuf_space;
    rbuf->rbuf_list = rbuf_list;
    rbuf->head = rbuf_list;
    rbuf->end = &rbuf_list[n - 1];
    
    memset(rbuf->space_loc, 0, rbuf->space);
    
    // Create circular list of pointers to elements space
    uint32_t i;
    for(i = 0; i < (uint16_t)(n - 1); i++) {
        rbuf_list[i].element = &rbuf_space[i * element_size];
        rbuf_list[i].next = &rbuf_list[i+1];
    }
    rbuf_list[i].element = &rbuf_space[i * element_size];
    rbuf_list[i].next = &rbuf_list[0];
}

void ringbuffer_add(struct ringbuffer *const rbuf, void const *const element) {
    rbuf->end = rbuf->end->next;
    memcpy(rbuf->end->element, element, rbuf->element_size);
    if(rbuf->shift && rbuf->end == rbuf->head) {
        rbuf->head = rbuf->head->next;
    }
    rbuf->shift = (rbuf->end->next == rbuf->head) ? 1 : 0;
}

void ringbuffer_switch(struct ringbuffer_node *n1, struct ringbuffer_node *n2) {
    void *tmp;
    
    tmp = n1->element;
    n1->element = n2->element;
    n2->element = tmp;
}

void ringbuffer_iterate(struct ringbuffer *const rbuf, void (*f)(void*)) {
    struct ringbuffer_node *e = rbuf->head;
    
    do {
        f(e->element);
        e = e->next;
    } while(e != rbuf->head);
}
