#include "transceive.h"
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

int main(int argc, char **argv) {
    int sock;
    int no_delay = 1;
    int priority = 6;
    struct sockaddr_in address = {0};    
    char buf[] = "Hello world!";
    
	address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	address.sin_family = AF_INET;
    address.sin_port = htons(50001);
    
    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }    
    if(setsockopt(sock, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
	while(connect(sock, (struct sockaddr*) &address, sizeof(struct sockaddr_in)) == -1) {
		PRINT(LOG_WARNING, "Connecting to server failed: %s.\n", strerror(errno));
	}
    
    while(1) {
        msg_send(sock, buf, sizeof(buf));
        PRINT(LOG_INFO, "Sent %d bytes.\n", sizeof(buf));
        sleep(6);
    }

    exit(EXIT_SUCCESS);
}
