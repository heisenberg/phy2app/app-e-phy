#include "ringbuffer.h"

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>

#define N_STUFF  10

struct stuff {
    uint16_t a;
    uint32_t b;
    uint64_t c;
};

static uint16_t increment = 0;

void fill(void *stuff_element);
void show(void *stuff_element);
void show_head_and_end(struct ringbuffer *rbuf);

int main(void) {
    uint8_t rbuf_space[N_STUFF * sizeof(struct stuff)];
    struct ringbuffer_node rbuf_list[N_STUFF];
    struct ringbuffer rbuf;    
    
    ringbuffer_init(N_STUFF, sizeof(struct stuff), rbuf_space, &rbuf, (struct ringbuffer_node *const) rbuf_list);
    printf("Init OK.\n");
    
    printf("Fill it via iterating. Iterating doesn't change head or end.\n");
    ringbuffer_iterate((struct ringbuffer *const) &rbuf, fill);
    ringbuffer_iterate((struct ringbuffer *const) &rbuf, show);
    show_head_and_end(&rbuf);
    printf("\n");
    
    {
        struct stuff new_stuff = {
            .a = 0,
            .b = 0,
            .c = 0
        };
        
        for(int i = 0; i < N_STUFF / 2; i++) {
            new_stuff.b = increment++;
            ringbuffer_add((struct ringbuffer *const) &rbuf, &new_stuff);
        }
        ringbuffer_iterate((struct ringbuffer *const) &rbuf, show);        
        show_head_and_end(&rbuf);
        printf("\n");
        
        for(int i = 0; i < N_STUFF / 3; i++) {
            new_stuff.c = increment++;
            ringbuffer_add((struct ringbuffer *const) &rbuf, &new_stuff);
        }
        ringbuffer_iterate((struct ringbuffer *const) &rbuf, show);
        show_head_and_end(&rbuf);
        printf("\n");
        
        for(int i = 0; i < N_STUFF / 2; i++) {
            new_stuff.a = increment++;
            ringbuffer_add((struct ringbuffer *const) &rbuf, &new_stuff);
        }
        ringbuffer_iterate((struct ringbuffer *const) &rbuf, show);
        show_head_and_end(&rbuf);
        printf("\n");
        
        new_stuff.a = 0;
        new_stuff.b = 0;
        new_stuff.c = 0;
        for(uint32_t i = 0; i < 1000000000; i++) {
            new_stuff.b = i;
            ringbuffer_add((struct ringbuffer *const) &rbuf, &new_stuff);
        }
    }
    
    ringbuffer_iterate((struct ringbuffer *const) &rbuf, show);
    show_head_and_end(&rbuf);
    
    exit(EXIT_SUCCESS);
}

void fill(void *stuff_element) {
    struct stuff *stuffp;
    
    stuffp = ((struct stuff*) stuff_element);
    stuffp->a = 123;
    stuffp->b = 123;
    stuffp->c = 123;
}

void show(void *stuff_element) {
    struct stuff *stuffp;
    
    stuffp = ((struct stuff*) stuff_element);
    printf("%" PRIu16
            "  %" PRIu32
            "  %" PRIu64
            "\n",
            stuffp->a,
            stuffp->b,
            stuffp->c);
}

void show_head_and_end(struct ringbuffer *rbuf) {
    printf("Head: %" PRIu16
            ",%" PRIu32
            ",%" PRIu64
            "\n",
            ((struct stuff *) rbuf->head->element)->a,
            ((struct stuff *) rbuf->head->element)->b,
            ((struct stuff *) rbuf->head->element)->c);
    printf("End: %" PRIu16
            ",%" PRIu32
            ",%" PRIu64
            "\n",
            ((struct stuff *) rbuf->end->element)->a,
            ((struct stuff *) rbuf->end->element)->b,
            ((struct stuff *) rbuf->end->element)->c);    
}
