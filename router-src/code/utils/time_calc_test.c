#include <stdio.h>
#include <stdlib.h>
#include "time_calc.h"

void time_sub_test();
void timediff_substract_test();

/**
 * This is a quickly hacked checkup, not a proper test for all the eventualities.
 * 
 * The results of time_sub_test have to be checked by the user.
 * 
 * timediff_substract_test checks only the sign of the result.
 * The user has to check if the absolute value of the result is correct.
 * */
int main(void) {	
	printf("Test time_sub:\n");
	time_sub_test();
	fflush(stdout);
	printf("Test timediff_substract:\n");
	timediff_substract_test();
	return(EXIT_SUCCESS);
}

void time_sub_test() {
	struct timespec t1;
	struct timespec t2;
	struct timediff result;
	t1.tv_sec = 10;
	t1.tv_nsec = MAX_NANOSLEEP_NSEC;
	t2.tv_sec = 11;
	t2.tv_nsec = 123456789;
		
	time_sub(&result, &t1, &t2);
	printf(" %ld.%09ld\n",t1.tv_sec,t1.tv_nsec);
	printf("-%ld.%09ld\n",t2.tv_sec,t2.tv_nsec);
	printf("-------------\n");
	if(result.sign == 1) {
		printf(" %ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("-%ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n");
	
	t1.tv_sec = 10;
	t1.tv_nsec = 987654321;
	t2.tv_sec = 11;
	t2.tv_nsec = MAX_NANOSLEEP_NSEC;
		
	time_sub(&result, &t1, &t2);
	printf(" %ld.%09ld\n",t1.tv_sec,t1.tv_nsec);
	printf("-%ld.%09ld\n",t2.tv_sec,t2.tv_nsec);
	printf("-------------\n");
	if(result.sign == 1) {
		printf(" %ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("-%ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n");
	
	t1.tv_sec = 13;
	t1.tv_nsec = 123456789;
	t2.tv_sec = 11;
	t2.tv_nsec = MAX_NANOSLEEP_NSEC;
	time_sub(&result, &t1, &t2);
	printf(" %ld.%09ld\n",t1.tv_sec,t1.tv_nsec);
	printf("-%ld.%09ld\n",t2.tv_sec,t2.tv_nsec);
	printf("-------------\n");
	if(result.sign == 1) {
		printf(" %ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("-%ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n");
	
	t1.tv_sec = 13;
	t1.tv_nsec = MAX_NANOSLEEP_NSEC;
	t2.tv_sec = 11;
	t2.tv_nsec = 123456789;
	time_sub(&result, &t1, &t2);
	printf(" %ld.%09ld\n",t1.tv_sec,t1.tv_nsec);
	printf("-%ld.%09ld\n",t2.tv_sec,t2.tv_nsec);
	printf("-------------\n");
	if(result.sign == 1) {
		printf(" %ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("-%ld.%09ld\n",result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n\n");
}

void timediff_substract_test() {
	struct timediff td1;
	struct timediff td2;
	struct timediff result;
	
	td1.time.tv_sec = 10;
	td1.time.tv_nsec = 111235687;
	td1.sign = 1;
	td2.time.tv_sec = 11;
	td2.time.tv_nsec = 964237842;
	td2.sign = 1;
	timediff_substract(&result, &td1, &td2);
	if(result.sign == 1) {
		printf("ERROR: result should be negative\n");
	}
	else {
		printf(" %ld.%09ld\n-%ld.%09ld\n-------------\n-%ld.%09ld\n",td1.time.tv_sec,td1.time.tv_nsec,td2.time.tv_sec,td2.time.tv_nsec,result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n");
	
	timediff_substract(&result, &td2, &td1);
	if(result.sign == 1) {
		printf(" %ld.%09ld\n-%ld.%09ld\n-------------\n %ld.%09ld\n",td2.time.tv_sec,td2.time.tv_nsec,td1.time.tv_sec,td1.time.tv_nsec,result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("ERROR: result should be positive\n");
	}
	
	printf("\n");
	
	td1.time.tv_sec = 10;
	td1.time.tv_nsec = 111235687;
	td1.sign = -1;
	td2.time.tv_sec = 11;
	td2.time.tv_nsec = 964237842;
	td2.sign = 1;
	timediff_substract(&result, &td1, &td2);
	if(result.sign == 1) {
		printf("ERROR: result should be negative\n");
	}
	else {
		printf("-%ld.%09ld\n-%ld.%09ld\n-------------\n-%ld.%09ld\n",td1.time.tv_sec,td1.time.tv_nsec,td2.time.tv_sec,td2.time.tv_nsec,result.time.tv_sec,result.time.tv_nsec);
	}
	
	printf("\n");
	
	timediff_substract(&result, &td2, &td1);
	if(result.sign == 1) {
		printf(" %ld.%09ld\n-(-%ld.%09ld)\n-------------\n %ld.%09ld\n",td2.time.tv_sec,td2.time.tv_nsec,td1.time.tv_sec,td1.time.tv_nsec,result.time.tv_sec,result.time.tv_nsec);
	}
	else {
		printf("ERROR: result should be positive\n");
	}
	
	printf("\n\n");
}
