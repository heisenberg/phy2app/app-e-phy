#include "transceive.h"

#include <stdint.h>

extern uint32_t msg_recv(int sockfd, uint8_t *msg_buf, size_t len);
extern uint32_t msg_send(int sockfd, uint8_t const *msg_buf, size_t len);
