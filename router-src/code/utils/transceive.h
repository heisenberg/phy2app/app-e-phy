#ifndef _TRANSCEIVE_H_
#define _TRANSCEIVE_H_

#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#include "print.h"

#if __GNUC__
inline uint32_t msg_recv(int sockfd, uint8_t *msg_buf, size_t len) __attribute__((always_inline));
inline uint32_t msg_send(int sockfd, uint8_t const *msg_buf, size_t len) __attribute__((always_inline));
#endif

inline uint32_t msg_recv(int sockfd, uint8_t *msg_buf, size_t len) {
    uint32_t bytes_left = len;
    uint32_t bytes_received_total = 0;
    int32_t bytes_received = 0;
    
    while(bytes_left > 0) {
        bytes_received = recv(sockfd, (void *) &msg_buf[bytes_received_total], bytes_left, 0);
        if(bytes_received == -1) {
            PRINT(LOG_ERROR, "%s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        else if(bytes_received == 0) {
            break;
        }
        bytes_left -= bytes_received;
        bytes_received_total += bytes_received;
    }
    
    return bytes_received_total;
}

inline uint32_t msg_send(int sockfd, uint8_t const *msg_buf, size_t len) {
    uint32_t bytes_left = len;
    uint32_t bytes_sent_total = 0;
    int32_t bytes_sent = 0;
    
    while(bytes_sent_total < len) {
        bytes_sent = send(sockfd, (void const *) &msg_buf[bytes_sent_total], bytes_left, 0);
        if(bytes_sent == -1) {
            PRINT(LOG_ERROR, "%s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        bytes_left -= bytes_sent;
        bytes_sent_total += bytes_sent;
    }
    
    return bytes_sent_total;
}

#endif
