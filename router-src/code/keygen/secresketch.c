/* TODO: - Use bits instead of bytes!
 *       - Check security aspect of block approach.
 **/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <inttypes.h>

#include "secresketch.h"
#include "bch_codec.h"
#include "cpp/parameter_estimator.h"
#include "../utils/print.h"

extern unsigned int calculate_nr_block_bits(struct bch_control *bch);
extern unsigned int calculate_nr_blocks(struct bch_control *bch);

/**
 * Generates Vector with random binary entries.
 *
 * @param len Length of Vector.
 * @param data Reference to the vector that the result will be written to.
 */
inline static void generate_random_vector(uint8_t *data, size_t len)
{
	for (size_t i = 0; i < len; i++) {
        data[i] = rand() % 2;
    }
}

/**
 * Bitwise XOR message with given vector
 *
 * @param len Length of message (also vector and data).
 * @param message Reference to the message vector.
 * @param vector Reference to the vector containing random bits.
 * @param data Reference to the vector that the result will be written to.
 */
inline static void generate_xor_vector(int len, uint8_t *message, uint8_t *vector, uint8_t *data)
{
	for (int i = 0;	i < len; ++i) {
        data[i] = message[i] ^ vector[i];
    }
}
/**
 * Bitwise Multiplication data1 with data2
 *
 * @param len Length of message (also vector and data).
 * @param data1 Reference to the message vector.
 * @param data2 Reference to the vector containing random bits.
 * @param result Reference to the vector that the result will be written to.
 */
inline static void generate_mul_vector(int len, uint8_t *data1, uint8_t *data2, uint8_t *result)
{
	for (int i = 0;	i < len; ++i) {
        result[i] = data1[i] * data2[i];
    }
}
/**
 * Flipping Bits in the given data vector.
 *
 * @param bitflip Vector containing the positions at which the bits will be flipped.
 * @param ncorrupt Number of Errors (Length of bitflip).
 * @param data Reference to the vector that the result will be written to.
 */
inline static void corrupt_data(unsigned int *bitflip, uint8_t *data, unsigned int ncorrupt)
{
	for (unsigned int i = 0; i < ncorrupt; ++i) {
        if(data[bitflip[i]] == 1){
            data[bitflip[i]] = 0;
        } else {
            data[bitflip[i]] = 1;
        }
	}
}

/**
* Convert Binary array to HEX
*/





/**
 * Moving window quantization.
 */
void parameter_quantization(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56])
{
    double params[5 * WINDOW_SIZE];
    double sliding_average;

    /* TODO: Calculate sliding window average of parameters for all antenna combinations. */
  	for (int i = 0; i < nr_packets / WINDOW_SIZE; ++i) {
        /* Caculate window average. */
        sliding_average = 0;
		for(int j = 0; j < WINDOW_SIZE; ++j) {
            printf("Bit: %d\n", i*WINDOW_SIZE+j);
            estimate_csi_parameters(&csi_phase_data[i * WINDOW_SIZE + j][0][0][0], &params[j * 5]);
            sliding_average += params[j * 5];
		}
        sliding_average /= WINDOW_SIZE;
        DEBUG_PRINT("Average for window %d: %f\n", i, sliding_average);

        /* Quantize. */
    	for(int j = 0; j < WINDOW_SIZE; ++j) {
            /* XXX: This will make a sequence of length window_size consisting only of 1s impossible.
             * XXX: Reduces randomness!
             **/
			message[i * WINDOW_SIZE + j] = (params[j * 5] > sliding_average) ? 1 : 0;
    	}
	}
}

static int csi_compare(const void *a, const void *b)
{
  return ( *(double*)a - *(double*)b );
}

/**
 * Median based quantization. Just for testing.
 */
void parameter_quantization_2(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56])
{
    double median;
    double params[5 * nr_packets];
    double sorted_first_params[nr_packets];

  	for(int i = 0; i < nr_packets; ++i) {
        estimate_csi_parameters(&csi_phase_data[i][0][0][0], &params[i * 5]);
    }

    for(int i = 0; i < nr_packets; ++i) {
        sorted_first_params[i] = params[i * 5];
    }
    qsort(sorted_first_params, nr_packets, sizeof(double), csi_compare);
    median = sorted_first_params[nr_packets/2];

    DEBUG_PRINT("Median parameter: %f\n", median);

    /* Quantize. */
    for(int i = 0; i < nr_packets; ++i) {
        message[i] = (params[i * 5] > median) ? 1 : 0;
    }
}

/**
 * Raw csi phase based quantization. Just for testing.
 */
void csi_quantization(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56])
{
    //~ double median;
    double average;
    //~ double sorted_avg_phase_data[nr_packets];
    double avg_phase_data[nr_packets];

    /* Calculate average phase value for each packet. */
    for(int i = 0; i < nr_packets; ++i) {
        average = 0;
        for(int j = 0; j < 56; ++j) {
            average += csi_phase_data[i][0][0][j];
        }
        avg_phase_data[i] = average / 56;
    }

    //~ memcpy(sorted_avg_phase_data, avg_phase_data, nr_packets * sizeof(double));
    //~ qsort(sorted_avg_phase_data, nr_packets, sizeof(double), csi_compare);
    //~ median = sorted_avg_phase_data[nr_packets/2];

    average = 0;
    for(int i = 0; i < nr_packets; ++i) {
        average += avg_phase_data[i];
    }
    average /= nr_packets;

    for(int i = 0; i < nr_packets; ++i) {
        message[i] = (avg_phase_data[i] > average) ? 1 : 0;
	}
}

/**
 *
 */
static void initialize_bch(struct bch_control **bch)
{
    {
        int len = 0;
        assert((POLY_DEGREE >= 5) && (POLY_DEGREE <= 15));
        if (len == 0) {
            len = 1 << (POLY_DEGREE - 4);
        }
        assert((ERROR_CORR_CAP > 0) && (ERROR_CORR_CAP <= MAX_ERRORS));
        assert((len > 0) && (8*len + POLY_DEGREE * ERROR_CORR_CAP <= (1 << POLY_DEGREE) - 1));
    }
	*bch = init_bch(POLY_DEGREE, ERROR_CORR_CAP, GENERATOR);
}

/**
 *
 * TODO: Use bits instead of bytes to use AND instead of multiplication for better space and time efficiency.
 */
struct key_recovery_data* encode(uint8_t message[], unsigned int msg_len)
{
    struct bch_control *bch;
    initialize_bch(&bch);

    unsigned int nr_block_bits = calculate_nr_block_bits(bch);
    unsigned int nr_blocks = calculate_nr_blocks(bch);
    uint16_t vector_length = bch->n - bch->ecc_bits;

    /* Lengths for key recovery data. */
    uint16_t sketch_len = vector_length * nr_blocks;
    uint16_t ecc_len = bch->ecc_bits * nr_blocks;

    /* Create key recovery data struct on heap. */
    uint8_t *key_r_data = malloc(sizeof(struct key_recovery_data) + 2 * sketch_len + ecc_len);
    struct key_recovery_data *key_r = (struct key_recovery_data *) key_r_data;
    key_r->sketch_len = sketch_len;
    key_r->rand_x_len = sketch_len;
    key_r->ecc_len = ecc_len;
    uint8_t *sketch_s = key_r_data + sizeof(struct key_recovery_data);
    uint8_t *rand_x = sketch_s + sketch_len;
    uint8_t *ecc = rand_x + sketch_len;

    uint8_t rand_k[vector_length];
    uint8_t bit_mul[vector_length];
    uint8_t data_block[vector_length];

    assert(nr_block_bits <= vector_length);

    srand((unsigned int) time(NULL)); /* Seed for the random number generator */

    /* Add zero padding to end of data block, so it can be XOR'd with the sketch block. */
    memset(data_block + nr_block_bits, 0, vector_length - nr_block_bits);

    /* TODO: Check if this is really necessary. */
    memset(sketch_s, 0, sketch_len);

    generate_random_vector(rand_x, sketch_len);

	for (unsigned int i = 0; i < nr_blocks; ++i) {
		/* Multiplication of random vectors x and k will reduce randomness. */
        /* TODO: No function calls to avoid iterating twice. */
        generate_random_vector(rand_k, vector_length);
        generate_mul_vector(vector_length, rand_x + i * (vector_length), rand_k, bit_mul);

		/* Encode bit_mul */
        encodebits_bch(bch, bit_mul, ecc + i * (bch->ecc_bits));

        /* Copy block of data from message to data_block with zero padding. */
        memcpy(data_block, message + i * nr_block_bits, nr_block_bits);

        /* XOR data blocks with bit_mul to produce sketch_s block */
        generate_xor_vector(vector_length, data_block, bit_mul, sketch_s + i * (vector_length));
	}

    return (struct key_recovery_data*) key_r_data;
}

/**
 *
 * TODO: Use bits instead of bytes to use AND instead of multiplication for better space and time efficiency.
 */
void decode(uint8_t message[], unsigned int msg_len, struct key_recovery_data *key_r_data)
{
    struct bch_control *bch;
    initialize_bch(&bch);

    unsigned int nr_block_bits = calculate_nr_block_bits(bch);
    unsigned int nr_blocks = calculate_nr_blocks(bch);
    unsigned int nr_errors;
    unsigned int errloc[ERROR_CORR_CAP];
    uint16_t vector_length = bch->n - bch->ecc_bits;

    /* Access data for key recovery. */
    uint8_t *sketch_s = key_r_data->data;
    uint8_t *rand_x = sketch_s + key_r_data->sketch_len;
    uint8_t *ecc_a = rand_x + key_r_data->sketch_len;

    uint8_t data_block[vector_length];
    assert(nr_block_bits <= vector_length);
    /* Add zero padding to end of data block, so it can be XOR'd with the sketch block. */
    memset(data_block + nr_block_bits, 0, vector_length - nr_block_bits);

	uint8_t recov_data_block[vector_length];
    uint8_t r1_b[vector_length];
    uint8_t bit_mul_b[vector_length];
    //~ uint8_t ecc_b[bch->ecc_bits];

	for(unsigned int i = 0; i < nr_blocks; ++i) {
        /* Copy block of data from message to data_block with zero padding. */
        memcpy(data_block, message + i * nr_block_bits, nr_block_bits);

		/* Generate r1 by XOR data_block and sketch_a_s_block. */
		generate_xor_vector(vector_length, data_block, sketch_s + i * vector_length, r1_b);

		/* BCH decode r1 with the recieved ecc_a */
		memset(errloc, 0, ERROR_CORR_CAP * sizeof(unsigned int));
		nr_errors = decodebits_bch(bch, r1_b, ecc_a + i * (bch->ecc_bits), errloc);
		// printf("\nNumber of Error in this block: %d\n", nr_errors);

		/* Correct  r1, it becomes r2, variable name DOES NOT change! */
		corrupt_data(errloc, r1_b, nr_errors); // After correction r1_b is as same as bit_mul
        //printf("corrupt_data");
		/* BCH encode r2,  it becomes r3 , variable name DOES NOT change! */
		//~ memset(ecc_b, 0, bch->ecc_bits);
		//~ encodebits_bch(bch, r1_b, ecc_b);

		/* Multiplication of r3 and rand_a_x_block */
		generate_mul_vector(vector_length, r1_b, rand_x + i * vector_length, bit_mul_b);
        //printf("generate_mul_vector");
		/* XOR sketch_a_s_block with bit_mul_b to genrate recov_data_block */
		generate_xor_vector(vector_length, sketch_s + i * vector_length, bit_mul_b, recov_data_block);
        //printf("generate_xor_vector");
		/* Recovering data_block */
		for (unsigned int j =  (i * nr_block_bits) ; j < nr_block_bits * (i + 1); ++j) {
			message[j] = recov_data_block[j - (i * nr_block_bits)];
		}
	}

}
