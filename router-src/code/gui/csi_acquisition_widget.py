#!/usr/bin/env python3

from gui_data import GuiData
from receiver_controller import ReceiverController

from PyQt5 import QtCore, QtGui

### probing settings interface
class CSIAcquisitionWidget(QtGui.QWidget):
    start_threads_signal = QtCore.pyqtSignal()
    
    def __init__(self, parent, gui_data, print_queue):        
        super().__init__(parent)
        
        self.gui_data = gui_data
        self.print_queue = print_queue
        
        self.__view()
        self.__control()
        
    def __view(self):
        ###############
        ### Widgets ###
        ###############
        # Packet exchange strategy
        self.strategy_cb = QtGui.QComboBox()
        self.strategy_cb.addItem("One-way")
        self.strategy_cb.addItem("Ping-pong")
        self.strategy_cb.addItem("Random")
        
        # Time between sending packets
        self.packet_delay = QtGui.QDoubleSpinBox()
        self.packet_delay.setRange(0,100000000)
        self.packet_delay.setDecimals(0)
        self.packet_delay.setSingleStep(10)
        self.packet_delay.setValue(500)
        
        # Process payload option: check packet's payload for certain string or take any CSI packet.
        self.pp_chb = QtGui.QCheckBox()
        
        # Timestamp options
        self.kt_chb = QtGui.QCheckBox()
        self.uts_chb = QtGui.QCheckBox()        
        self.utr_chb = QtGui.QCheckBox()
        
        # Logging checkbox and filters
        self.store_chb = QtGui.QCheckBox()
        self.pkt_time_diff_cb = QtGui.QCheckBox()
        self.pkt_time_diff_dsb = QtGui.QDoubleSpinBox()
        self.pkt_time_diff_dsb.setEnabled(False)
        self.pkt_time_diff_dsb.setRange(0,10000000)
        self.pkt_time_diff_dsb.setDecimals(0)
        self.pkt_time_diff_dsb.setSingleStep(10)
        self.pkt_time_diff_dsb.setValue(4000)
        
        self.pkt_correlation_cb = QtGui.QCheckBox()
        self.pkt_correlation_dsb = QtGui.QDoubleSpinBox()  
        self.pkt_correlation_dsb.setEnabled(False)     
        self.pkt_correlation_dsb.setRange(-1.00,1.00)
        self.pkt_correlation_dsb.setDecimals(2)
        self.pkt_correlation_dsb.setSingleStep(0.01)
        self.pkt_correlation_dsb.setValue(0.95)
        
        # Button that starts the measurement applications on the routers
        self.probing_button = QtGui.QPushButton("Start probing")
        
        ##############
        ### Layout ###
        ##############
        self.csi_acquisition_layout = QtGui.QVBoxLayout(self)

        # Probing settings
        self.probing_layout = QtGui.QFormLayout()
        self.probing_layout.addRow(QtGui.QLabel("Probing strategy"), self.strategy_cb)
        self.probing_layout.addRow(QtGui.QLabel("Message delay [microseconds]"), self.packet_delay)
        self.probing_layout.addRow(QtGui.QLabel("Process payload"), self.pp_chb)
        self.probing_layout.addRow(QtGui.QLabel("Kernel space receive timestamps"), self.kt_chb)
        self.probing_layout.addRow(QtGui.QLabel("User space send timestamps"), self.uts_chb)
        self.probing_layout.addRow(QtGui.QLabel("User space receive timestamps"), self.utr_chb)
        self.probing_layout.addRow(QtGui.QLabel("Store measurements"), self.store_chb)
        self.probing_group = QtGui.QGroupBox("Probing settings")
        self.probing_group.setLayout(self.probing_layout)
        
        # TODO: Move logging group to own dock widget.
        # TODO: Make 'logging group' available in offline mode and rename it to 'filter group'.
        self.timestamp_filter_widget = QtGui.QWidget()
        self.timestamp_filter_layout = QtGui.QHBoxLayout(self.timestamp_filter_widget)
        self.timestamp_filter_layout.addWidget(self.pkt_time_diff_cb)
        self.timestamp_filter_layout.addWidget(self.pkt_time_diff_dsb)
        self.correlation_filter_widget = QtGui.QWidget()
        self.correlation_filter_layout = QtGui.QHBoxLayout(self.correlation_filter_widget)
        self.correlation_filter_layout.addWidget(self.pkt_correlation_cb)
        self.correlation_filter_layout.addWidget(self.pkt_correlation_dsb)
        self.logging_layout = QtGui.QFormLayout()
        self.logging_layout.addRow(QtGui.QLabel("Timestamp dt [microseconds]"), self.timestamp_filter_widget)
        self.logging_layout.addRow(QtGui.QLabel("Correlation threshold"), self.correlation_filter_widget)
        self.logging_group = QtGui.QGroupBox("Filter settings")
        self.logging_group.setLayout(self.logging_layout)
        
        self.csi_acquisition_layout.addWidget(self.probing_group)
        self.csi_acquisition_layout.addWidget(self.logging_group)
        self.csi_acquisition_layout.addWidget(self.probing_button)
        
    def __control(self):
        self.receiver_controller = ReceiverController(self.gui_data, self.print_queue)
        self.start_threads_signal.connect(self.start_threads)
        
        self.store_chb.stateChanged.connect(self.receiver_controller.toggle_logging)
        self.pkt_time_diff_cb.stateChanged.connect(lambda: self.pkt_time_diff_dsb.setEnabled(self.pkt_time_diff_cb.isChecked()))
        self.pkt_correlation_cb.stateChanged.connect(lambda: self.pkt_correlation_dsb.setEnabled(self.pkt_correlation_cb.isChecked()))

    def start_threads(self):
        self.receiver_controller.start.emit()
