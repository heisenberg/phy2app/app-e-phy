#!/usr/bin/env python3

import numpy as np
import scipy.io as sio
from scipy.optimize import curve_fit
from pathlib import Path

# TODO: Center frequency doesn't matter?
SUBCARRIER_SPACING = 0.3125

class ParameterEstimator():
    def __init__(self):
        # function to fit
        self.param_names = ['gain_mismatch', 'timing_offset', 'phase_mismatch', 'tof_delay', 'phase_offset_error']
        # ~ self.initial_guess = [0.512, -0.02812, -0.006355, -0.02762, 0.1326]
        self.initial_guess = [0.512, -0.02857143, -0.006355, -0.02762, 0.1326]
        self.params_a = []
        self.params_b = []
        self.rx_antenna = None
        self.tx_antenna = None        
        
        # define x values
        self.subcarrier_indices = (np.arange(57).astype(float)-28.)        
        self.mask = np.ones((57,)).astype(bool)
        # remove the zero entry
        self.mask[28] = False
        self.subcarrier_indices = self.subcarrier_indices[self.mask]
        
        # TODO: Why is this unused?
        # ~ self.frequency_offsets = SUBCARRIER_SPACING * self.subcarrier_indices
        
        self.csi_a = None
        self.csi_b = None
    
    def load_csi(self, csi_a, csi_b):
        self.csi_a = csi_a
        self.csi_b = csi_b
    
    def extract(self, rx_antenna, tx_antenna):
        # TODO: Maximum size of array can be computed before execution to avoid copying all the data at the end.
        #       min(len(self.csi_a.phase), len(self.csi_b.phase)) * nr_params
        #       The size shrinks by one for each time an exception occurs because parameters could not be found.
        self.params_a = []
        self.params_b = []
        self.rx_antenna = rx_antenna
        self.tx_antenna = tx_antenna
        errors = 0
        for k in range(min(len(self.csi_a.phase), len(self.csi_b.phase))):
            # define y values
            phase_alice = self.csi_a.phase[k][self.rx_antenna, self.tx_antenna]
            phase_bob = self.csi_b.phase[k][self.rx_antenna, self.tx_antenna]
            phase_alice = phase_alice - np.mean(phase_alice)
            phase_bob = phase_bob - np.mean(phase_bob)
            try:
                popt_a, pcov_a = curve_fit(self.fitting_function, self.subcarrier_indices, phase_alice, self.initial_guess, method='lm', maxfev = 1200)
                popt_b, pcov_b = curve_fit(self.fitting_function, self.subcarrier_indices, phase_bob, self.initial_guess, method='lm', maxfev = 1200)
                
                # TODO: Remove debug code?
                # ~ f_a = self.fitting_function(self.subcarrier_indices, *popt_a)
                # ~ f_b = self.fitting_function(self.subcarrier_indices, *popt_b)
                
                self.params_a.append(popt_a.reshape((1, -1)))
                self.params_b.append(popt_b.reshape((1, -1)))
            except Exception as error:
                # TODO: Handle maxfev exception and general exception.
                #self.params_a.append(popt_a.reshape((1, -1)))
                #self.params_b.append(popt_b.reshape((1, -1)))
                self.params_a.append(np.zeros(5).reshape((1, -1)))
                self.params_b.append(np.zeros(5).reshape((1, -1)))
                errors += 1
                continue

        self.params_a = np.concatenate(self.params_a, axis=0)        
        self.params_b = np.concatenate(self.params_b, axis=0)
        print("errors for tx", tx_antenna, "rx", rx_antenna, errors, "/", min(len(self.csi_a.phase), len(self.csi_b.phase)))
        return self.params_a, self.params_b
    
    def export(self, rx_antenna, tx_antenna):
        # store csi for matlab
        #~ sio.savemat('csi_a.mat', {'csi_a': self.csi_a.csi})
        #~ sio.savemat('csi_b.mat', {'csi_b': self.csi_b.csi})
        
        # store parameters for matlab
        self.rx_antenna = rx_antenna
        self.tx_antenna = tx_antenna
        
        file_path = Path('./csi_parameters')
        file_path.mkdir(exist_ok = True)
        file_a = Path('./csi_parameters/params_a' + str(self.rx_antenna) + str(self.tx_antenna) + '.mat')
        file_b = Path('./csi_parameters/params_b' + str(self.rx_antenna) + str(self.tx_antenna) + '.mat')
        sio.savemat(file_a, {'params_a': self.params_a})
        sio.savemat(file_b, {'params_b': self.params_b})

    def fitting_function(self, x, gain_mismatch, timing_offset, phase_mismatch, tof_delay, phase_offset_error):
        fs = SUBCARRIER_SPACING #MHz
        return np.arctan(gain_mismatch
            * np.sin(2 * np.pi * timing_offset * fs * x + phase_mismatch) \
            / np.cos(2 * np.pi * timing_offset * fs * x)) \
            - 2 * np.pi * tof_delay * fs * x \
            + phase_offset_error * np.ones(56)

    # TODO: Why is this unused?
    def jacobian(self, x, gain_mismatch, timing_offset, phase_mismatch, tof_delay, phase_offset_error):
        fs = SUBCARRIER_SPACING #MHz
        
        d1 = -4 * np.sin(np.pi / 4 - np.pi * timing_offset * fs * x) \
            * np.sin(np.pi * timing_offset * fs * x + np.pi / 4) \
            * np.sin(2 * np.pi * timing_offset * fs * x + phase_mismatch) \
            / (gain_mismatch**2 * np.cos(4 * np.pi * timing_offset * fs * x + 2 * phase_mismatch) \
            - gain_mismatch**2 -np.cos(4 * np.pi * timing_offset * fs * x) -1)
        
        d2 = -4*np.pi*gain_mismatch*fs*x*np.cos(phase_mismatch)/(gain_mismatch**2*(np.cos(4*np.pi*timing_offset**fs*x+2*phase_mismatch)-1)-np.cos(4*np.pi*gain_mismatch*fs*x)-1)
        
        d3 = gain_mismatch*np.cos(2*np.pi*timing_offset*fs*x)*np.cos(2*np.pi*timing_offset*fs*x+phase_mismatch)/(gain_mismatch**2*np.sin(2*np.pi*timing_offset*fs*x+phase_mismatch)**2+
                                                                     np.cos(2*np.pi*timing_offset*fs*x)**2)
        
        d4 = -2*np.pi*fs*x
        
        d5 = np.ones(x.shape)
        return np.concatenate([d1.reshape((-1,1)), 
                               d2.reshape((-1,1)), 
                               d3.reshape((-1,1)), 
                               d4.reshape((-1,1)), 
                               d5.reshape((-1,1))], axis=1)
