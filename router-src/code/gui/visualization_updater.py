#!/usr/bin/env python3

import csi_types as ct

import queue
import numpy as np
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot

# TODO: rename this class, since it is reading, filtering and reformatting CSI data before it sends a signal
#       that is processed by the visualisation settings widget in order to redraw ("update") the visualization.
class VisualizationUpdater(QtCore.QObject):
    data_available = QtCore.pyqtSignal()
    
    def __init__(self, gui_data, print_queue):
        super().__init__()
        self.gui_data = gui_data
        self.print_queue = print_queue
        self.max_rcv_time_diff = 100000
        self.running = False
    
    def update_data(self):        
        self.gui_data.alice_csi.clear()
        self.gui_data.bob_csi.clear()
        self.gui_data.measurement_nr_a = 0
        self.gui_data.measurement_nr_b = 0
        
        self._update_data()
    
    def _update_data(self):        
        try:
            # Get csi headers
            a_hdr = np.frombuffer(self.gui_data.csi_vis_queue_a.get(timeout = 2), dtype = ct.DTYPE_CSI_HDR)
            b_hdr = np.frombuffer(self.gui_data.csi_vis_queue_b.get(timeout = 2), dtype = ct.DTYPE_CSI_HDR)
        
        except queue.Empty:
            if(self.running):
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue get timeout while running 1.")
                QtCore.QTimer.singleShot(0, self._update_data)
            else:
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue get timeout. Exit update_data loop.")
                return
        
        except ValueError:
            # TODO: Better error handling
            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue data corrupted. Expected header but got something else.")
        
        else:
            # Filter packets by timestamp
            order = True
            while order and self.running:
                t_a = a_hdr[0][0]
                t_b = b_hdr[0][0]
                td = int(t_a) - int(t_b)
                if td < -self.max_rcv_time_diff:
                    try:
                        self.gui_data.csi_vis_queue_a.get(timeout = 1)  # Drop Alice's csi 
                        a_hdr = np.frombuffer(self.gui_data.csi_vis_queue_a.get(timeout = 1), dtype = ct.DTYPE_CSI_HDR) # Get next header
                    except queue.Empty:
                        if(self.running):
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Alice's queue timed out while running.")
                            try:
                                self.gui_data.csi_vis_queue_b.get(timeout = 1)  # Drop Bob's csi too 
                            except Exception:
                                pass
                            QtCore.QTimer.singleShot(0, self._update_data)
                            return
                        else:
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Alice's queue timed out. Exit update_data loop.")
                            return
                    # ~ except ValueError as error:
                    except Exception as error:
                        print(error)
                        self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Alice's queue is missing a header.")
                        if(self.running):
                            QtCore.QTimer.singleShot(0, self._update_data)
                            return
                        else:
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Exit update_data loop.")
                            return
                elif td > self.max_rcv_time_diff:
                    try:
                        self.gui_data.csi_vis_queue_b.get(timeout = 1)  # Drop Bob's csi
                        b_hdr = np.frombuffer(self.gui_data.csi_vis_queue_b.get(timeout = 1), dtype = ct.DTYPE_CSI_HDR) # Get next header
                    except queue.Empty:
                        if(self.running):
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Bob's queue timed out while running.")
                            try:
                                self.gui_data.csi_vis_queue_a.get(timeout = 1)  # Drop Alice's csi too 
                            except Exception:
                                pass
                            QtCore.QTimer.singleShot(0, self._update_data)
                            return
                        else:
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Bob's queue timed out. Exit update_data loop.")
                            return
                    # ~ except ValueError as error:
                    except Exception as error:
                        print(error)
                        self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Bob's queue is missing a header.")
                        if(self.running):
                            QtCore.QTimer.singleShot(0, self._update_data)
                            return
                        else:
                            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Exit update_data loop.")
                            return
                else:
                    order = False

            # Get antenna configuration and number of subcarriers
            rx_a = a_hdr[0][8]
            tx_a = a_hdr[0][9]
            num_tones_a = a_hdr[0][7]
            rx_b = b_hdr[0][8]
            tx_b = b_hdr[0][9]
            num_tones_b = b_hdr[0][7]
            
            # Get csi data
            try:
                a_csi = np.frombuffer(self.gui_data.csi_vis_queue_a.get(timeout = 1), dtype = ct.DTYPE_CSI_COMPLEX)
                b_csi = np.frombuffer(self.gui_data.csi_vis_queue_b.get(timeout = 1), dtype = ct.DTYPE_CSI_COMPLEX)
                a_csi = a_csi.reshape(rx_a, tx_a, num_tones_a)
                b_csi = b_csi.reshape(rx_b, tx_b, num_tones_b)

                # Add csi data and header to csi record 
                self.gui_data.alice_csi.add(a_csi, a_hdr)
                self.gui_data.bob_csi.add(b_csi, b_hdr)
                
                self.data_available.emit()
            
            except queue.Empty:
                if(self.running):
                    self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue get timeout while running 4.")
                    QtCore.QTimer.singleShot(0, self._update_data)
                else:
                    self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue get timeout. Exit update_data loop.")
                    return
            
            except ValueError as error:
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Queue data corrupted. Expected csi data but got something else.")
            
            if(self.running):
                QtCore.QTimer.singleShot(0, self._update_data)
        
    # TODO: rename function
    def reconnect(self):
        self.running = True
        QtCore.QTimer.singleShot(0, self.update_data)
    
    # TODO: rename function
    @pyqtSlot()
    def disconnect(self):
        self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": disconnect.")
        self.running = False
