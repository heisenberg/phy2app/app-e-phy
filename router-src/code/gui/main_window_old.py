#!/usr/bin/env python3
from gui_data import GuiData
from visualization_plot_widget import VisualizationPlotWidget
from visualization_settings_widget import VisualizationSettingsWidget
from csi_acquisition_widget import CSIAcquisitionWidget
from visualization_updater import VisualizationUpdater
from queue_print_worker import QueuePrintWorker

from parameter_estimator import ParameterEstimator
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph.pyqtgraph as pg
import queue
import socket

# The main window class contains all the widgets that are displayed
class MainWindow(QtWidgets.QMainWindow):
    visualization_updater_thread = QtCore.QThread()
    info_text_thread = QtCore.QThread()

    def __init__(self):
        super().__init__()

        self.gui_data = GuiData()
        self.print_queue = queue.Queue()

        self.__view()
        self.__control()

    def __view(self):
        ######################################
        ### Initialize GUIs central widget ###
        ######################################
        self.current_plots = pg.GraphicsLayoutWidget(show=False)
        row = 0
        col = 0
        for i in range(9):
            p = VisualizationPlotWidget(rx = row, tx = col, title = "RX" + str(col+1) + " TX" + str(row+1), gui_data = self.gui_data)
            self.current_plots.addItem(p, row = row, col = col)
            if(row < 2):
                row += 1
            else:
                row = 0
                col += 1
        self.setCentralWidget(self.current_plots)

        ########################
        ### Left side of GUI ###
        ########################
        # Visualization settings frame
        self.visualization_settings_widget = VisualizationSettingsWidget(self, self.current_plots, self.gui_data, self.print_queue)
        self.visualization_scroll_area = QtGui.QScrollArea()
        self.visualization_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.visualization_scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.visualization_scroll_area.setWidget(self.visualization_settings_widget)
        self.visualization_settings_dock_widget = QtGui.QDockWidget("Visualization settings")
        self.visualization_settings_dock_widget.setWidget(self.visualization_scroll_area)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.visualization_settings_dock_widget)
        # Probing settings frame
        self.csi_acquisition_widget = CSIAcquisitionWidget(self, self.gui_data, self.print_queue)
        self.csi_acquisition_dock_widget = QtGui.QDockWidget("CSI acquisition")
        self.csi_acquisition_scroll_area = QtGui.QScrollArea()
        self.csi_acquisition_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.csi_acquisition_scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.csi_acquisition_scroll_area.setWidget(self.csi_acquisition_widget)
        self.csi_acquisition_dock_widget.setWidget(self.csi_acquisition_scroll_area)

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.csi_acquisition_dock_widget)

        #########################
        ### Right side of GUI ###
        #########################
        # Metadata frame
        self.metadata_layout = QtGui.QVBoxLayout(self.visualization_settings_widget.metadata_table_widget)
        self.metadata_dock_widget = QtGui.QDockWidget("Metadata")
        self.metadata_dock_widget.setWidget(self.visualization_settings_widget.metadata_table_widget)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.metadata_dock_widget)
        # Info box frame
        self.info_text = QtWidgets.QPlainTextEdit()
        self.info_text.setReadOnly(True)
        self.info_text_layout = QtGui.QVBoxLayout(self.info_text)
        self.info_text_dock_widget = QtGui.QDockWidget("Info box")
        self.info_text_dock_widget.setWidget(self.info_text)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.info_text_dock_widget)

        #####################
        ### Bottom of GUI ###
        #####################
        # Key generation frame
        self.parameter_estimator = ParameterEstimator()
        self.keygen_widget = QtGui.QWidget(self)
        self.keygen_layout = QtGui.QVBoxLayout(self.keygen_widget)
        self.key_gen_button = QtGui.QPushButton("Generate")

        self.param_extract_button = QtGui.QPushButton("Extract")
        self.keygen_form = QtGui.QFormLayout()
        self.keygen_form.addRow(QtGui.QLabel("Extract parameter"), self.param_extract_button)
        self.keygen_form.addRow(QtGui.QLabel("Generate key"), self.key_gen_button)
        self.keygen_layout.addLayout(self.keygen_form)
        self.keygen_dock_widget = QtGui.QDockWidget("Key generation")
        self.keygen_dock_widget.setWidget(self.keygen_widget)
        self.keygen_dock_widget.setMaximumWidth(self.visualization_settings_dock_widget.width() - 255)
        self.keygen_dock_widget.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.keygen_dock_widget)

        # Key information frame
        self.key_table_widget = QtGui.QTableWidget()
        self.key_table_widget.setRowCount(3)
        self.key_table_widget.setColumnCount(3)
        self.key_table_widget.setHorizontalHeaderLabels(['RX1','RX2','RX3'])
        self.key_table_widget.setVerticalHeaderLabels(['TX1','TX2','TX3'])
        self.key_table_widget.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.key_table_widget.setFocusPolicy(QtCore.Qt.NoFocus)
        self.key_table_widget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.key_table_widget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.key_table_widget.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)
        self.key_table_widget.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
        self.key_table_widget.horizontalHeader().setResizeMode(2, QtGui.QHeaderView.Stretch)

        self.key_font = QtGui.QFont("Monospace", 10, QtGui.QFont.Bold)
        self.key_font.setStyleHint(QtGui.QFont.TypeWriter)

        self.key_status_dock_widget = QtGui.QDockWidget("Key information")
        self.key_status_dock_widget.setWidget(self.key_table_widget)
        self.key_status_dock_widget.setMinimumHeight(150)
        self.key_status_dock_widget.setMaximumHeight(150)
        self.key_status_dock_widget.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.key_status_dock_widget)

    def __control(self):
        self.setup_threads()

        self.key_gen_button.clicked.connect(self.key_generation)
        self.param_extract_button.clicked.connect(self.parameter_estimation)

        ##########################
        ### Menu configuration ###
        ##########################

        fullscreen_action = QtGui.QAction("Fullsreen", self)
        fullscreen_action.setShortcut("F11")
        fullscreen_action.triggered.connect(self.toggleView)
        quit_action = QtGui.QAction("Quit", self)
        quit_action.setShortcut("Ctrl+Q")
        quit_action.triggered.connect(QtCore.QCoreApplication.quit)
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('File')
        file_menu.addAction(quit_action)
        view_menu = main_menu.addMenu('View')
        view_menu.addAction(fullscreen_action)

        ########################################
        ### State machine for probing button ###
        ########################################

        self.pb_state_machine = QtCore.QStateMachine()
        self.pb_state_offline = QtCore.QState()
        self.pb_state_online = QtCore.QState()

        # State transitions
        self.pb_state_offline.addTransition(self.csi_acquisition_widget.probing_button.clicked, self.pb_state_online)
        self.pb_state_online.addTransition(self.csi_acquisition_widget.probing_button.clicked, self.pb_state_offline)

        # Offline state properties
        self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_a, "enabled", True)
        self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_b, "enabled", True)
        #~ self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_e, "enabled", True)
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.probing_button, "text", "Start probing")
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.probing_group, "enabled", True)
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.logging_group, "enabled", True)

        # Online state properties
        self.pb_state_online.assignProperty(self.visualization_settings_widget.all_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.alice_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.bob_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_a, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_b, "enabled", False)
        #~ self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_e, "enabled", False)
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_button, "text", "Stop probing")
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_button, "enabled", False)
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_group, "enabled", False)

        # Offline --> Online state transition signals
        self.pb_state_offline.exited.connect(self.visualization_updater.reconnect)
        self.pb_state_offline.exited.connect(self.csi_acquisition_widget.receiver_controller.reconnect)
        self.pb_state_offline.exited.connect(lambda: QtCore.QTimer.singleShot(2000, lambda: self.csi_acquisition_widget.probing_button.setEnabled(True)))
        self.pb_state_offline.exited.connect(self.apply_logging_filters)

        # Online --> Offline state transition signals
        self.pb_state_online.exited.connect(lambda: self.visualization_settings_widget.enable_spin_boxes(True))
        self.pb_state_online.exited.connect(self.visualization_updater.disconnect)
        self.pb_state_online.exited.connect(self.csi_acquisition_widget.receiver_controller.disconnect)
        self.pb_state_online.exited.connect(lambda: self.csi_acquisition_widget.probing_button.setEnabled(False))
        self.pb_state_online.exited.connect(lambda: QtCore.QTimer.singleShot(2000, lambda: self.csi_acquisition_widget.probing_button.setEnabled(True)))

        # Add states to machine and start it
        self.pb_state_machine.addState(self.pb_state_offline)
        self.pb_state_machine.addState(self.pb_state_online)
        self.pb_state_machine.setInitialState(self.pb_state_offline)
        self.pb_state_machine.start()

        # Start thread
        self.csi_acquisition_widget.start_threads_signal.emit()

    def apply_logging_filters(self):
        if self.csi_acquisition_widget.pkt_time_diff_cb.isChecked:
            self.csi_acquisition_widget.receiver_controller.set_logging_dt_filter(int(self.csi_acquisition_widget.pkt_time_diff_dsb.text()))
        if self.csi_acquisition_widget.pkt_correlation_cb.isChecked:
            self.csi_acquisition_widget.receiver_controller.set_logging_corr_filter(float(self.csi_acquisition_widget.pkt_correlation_dsb.text()))

    def update_info_text(self, text):
        self.info_text.appendPlainText(text)

    def toggleView(self, index):
        if self.windowState() != QtCore.Qt.WindowFullScreen:
            self.showFullScreen()
        else:
            self.showMaximized()

    def parameter_estimation(self):
        #~ self.key_gen_button.setEnabled(True)
        self.parameter_estimator.load_csi(self.gui_data.alice_csi, self.gui_data.bob_csi)
        for k in range(self.gui_data.alice_csi.phase[0].shape[0]):
                for j in range(self.gui_data.alice_csi.phase[0].shape[1]):
                        params_a, params_b = self.parameter_estimator.extract(k,j)

                        for i in range(min(len(params_a),len(params_b))):
                            self.print_queue.put("Alice's parameters:" + str(params_a[i]))
                            self.print_queue.put("Bob's parameters : " + str(params_b[i]))
                            self.print_queue.put("")

                        self.parameter_estimator.export(k,j)
                        self.print_queue.put("Parameter extraction finished.")

    def key_generation(self):
        # Timeout for execution of key generation script
        wait_for_finish = 15000
        self.key_gen_button.setEnabled(False)

        # Start key generation script
        try:
            self.key_generation_process = QtCore.QProcess()
            self.key_generation_process.start("./runDemo_LOSparams.sh", [])
            finished = self.key_generation_process.waitForFinished(wait_for_finish)
            if not finished:
                # TODO: Improve execution speed of matlab script, e.g. by starting a matlab instance in the background when the gui is started.
                #~ self.print_queue.put("Key generation process didn't finish execution after " + str(wait_for_finish) + "ms.")
                self.key_generation_process.kill()
            else:
                self.print_queue.put("Key generation finished.")
        except Exception:
            self.print_queue.put("Cannot start key generation script.")

        # Load and display generated keys from files
        for rx in range(3):
            for tx in range(3):
                try:
                    file_name = "Key" + str(rx) + str(tx) + ".txt"
                    file_path = Path(file_name)
                    key = file_path.read_text()
                    self.key_table_widget.setItem(rx, tx, QtGui.QTableWidgetItem(key))
                    self.key_table_widget.item(rx, tx).setFont(self.key_font)
                except Exception as error:
                    pass
                    # ~ print(error)
                    self.print_queue.put("No key file for pair " + str(rx) + str(tx))

        self.key_gen_button.setEnabled(False)

    def setup_threads(self):
        # TODO: Check if QtProcess should be declared as static class member
        self.key_generation_process = None

        ##################################
        ### Thread for Online Plotting ###
        ##################################
        self.visualization_updater = VisualizationUpdater(self.gui_data, self.print_queue)
        self.visualization_updater.moveToThread(self.visualization_updater_thread)

        # TODO: refactor visualization updater and visualisation settings widget
        self.visualization_updater.data_available.connect(self.visualization_settings_widget.online_update_metadata)
        self.visualization_updater_thread.start()

        ##################################
        ### Start info text box thread ###
        ##################################
        self.info_text_worker = QueuePrintWorker(self.print_queue)
        self.info_text_worker.moveToThread(self.info_text_thread)
        self.info_text_worker.new_info_text.connect(self.update_info_text)
        self.info_text_thread.started.connect(self.info_text_worker.run)
        self.info_text_thread.start()
