#!/usr/bin/env python3

import os
import queue
import numpy as np
from pathlib import Path

# TODO: class could be made a singleton since only one instance of GUI data should exist at a moment.
#       --> hiding of globals? better design idea at this point?
class GuiData():
    """
    GUIs central class for configuration and csi data.
    """
    def __init__(self):
        self.bob_csi = CsiRecord()
        self.alice_csi = CsiRecord()

        self.csi_vis_queue_a = queue.Queue()
        self.csi_vis_queue_b = queue.Queue()
        self.csi_log_queue_a = queue.Queue()
        self.csi_log_queue_b = queue.Queue()

        self._meas_nr_a = 0
        self._meas_nr_b = 0
        self._num_tones = 56
        self._corr_threshold = 0.95
        self._show_centeroid = False
        self._uni_plot = False

    @property
    def measurement_nr_a(self):
        return self._meas_nr_a
    @measurement_nr_a.setter
    def measurement_nr_a(self, value: int):
        self._meas_nr_a = value
        
    @property
    def measurement_nr_b(self):
        return self._meas_nr_b
    @measurement_nr_b.setter
    def measurement_nr_b(self, value: int):
        self._meas_nr_b = value

    @property
    def nr_of_subcarriers(self):
        return self._num_tones
    @nr_of_subcarriers.setter
    def nr_of_subcarriers(self, value: int):
        self._num_tones = value

    @property
    def correlation_threshold(self):
        return self._corr_threshold
    @correlation_threshold.setter
    def correlation_threshold(self, value: float):
        if value >= -1.0 and value <= 1.0:
            self._corr_threshold = value

    @property
    def unidirectional_plot(self):
        return self._uni_plot
    @unidirectional_plot.setter
    def unidirectional_plot(self, value: bool):
        self._uni_plot = value
        
    @property
    def show_centeroid(self):
        return self._show_centeroid
    @show_centeroid.setter
    def show_centeroid(self, value: bool):
        self._show_centeroid = value
        
class CsiRecord():
    """
    Used to load and store csi data including csi header, phase and amplitude.
    """
    def __init__(self):
        self.path = None
        self.csi = []
        self.headers = []
        self.amp = []
        self.phase = []
        
    def load(self):
        if self.path:
            with self.path.open('rb') as fd:
                file_size = os.fstat(fd.fileno()).st_size
                while fd.tell() < file_size:
                    self.headers.append(np.load(fd))
                    self.csi.append(np.load(fd))

                self.calc_phase_and_amp()
                
        else:
            print("Path is not set.")
            
    def store(self):
        pass
    
    def add(self, new_csi, new_csi_hdr):
        self.headers.append(new_csi_hdr)
        self.csi.append(new_csi)
        self.amp.append(np.absolute(new_csi[:][:][:]['real'] + 1.j*new_csi[:][:][:]['imag']))
        self.phase.append(np.unwrap(np.angle(new_csi[:][:][:]['real'] + 1.j*new_csi[:][:][:]['imag'])))
    
    def calc_phase_and_amp(self):
        # Don't compute dbm value (10*np.log10) for amplitude in favour of better visualization performance
        for i in range(len(self.csi)):
            self.amp.append(np.absolute(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag']))
            self.phase.append(np.unwrap(np.angle(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag'])))
        
        # If you want to compute the dbm value, you have to check the value before calling log10:
        #~ for i in range(len(self.csi)):
            #~ amplitudes_mat = []
            #~ csi = self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag']
            #~ for rx in range(self.headers[i]['nr'][0]):
                #~ amplitudes_row = []
                #~ for tx in range(self.headers[i]['nc'][0]):
                    #~ amplitudes_row.append([10*np.log10(np.absolute(subcarrier)) if np.absolute(subcarrier) > 1 else 0 for subcarrier in csi[rx][tx][:]])
                #~ amplitudes_mat.append(amplitudes_row)
            #~ self.amp.append(amplitudes_mat)
            #~ self.phase.append(np.unwrap(np.angle(csi)))

    def clear(self):
        self.path = None
        self.csi.clear()
        self.headers.clear()
        self.amp.clear()
        self.phase.clear()
        
    def set_path(self, path):
        csi_file = Path(path)
        if csi_file.is_file():
            self.path = csi_file
        else:
            raise Exception("No file to load has been selected. Check file path.")
