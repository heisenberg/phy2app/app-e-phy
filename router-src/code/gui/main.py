#!/usr/bin/env python3

from main_window import MainWindow
import visualization_updater
import queue_print_worker
import csi_acquisition_widget

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QThread
import qdarkstyle
import sys

def main():
    app = QtGui.QApplication(['CSI Evaluation GUI'])
    
    # Set locale to C -> double decimal delimiter is a dot.
    QtCore.QLocale.setDefault(QtCore.QLocale.c());    
    
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    
    main_window = MainWindow()
    main_window.showMaximized()
    
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
        
if __name__ == '__main__':
    main()
