#!/usr/bin/env python3

import queue
import csi_types as ct
import numpy as np
from PyQt5 import QtCore
from pathlib import Path
from datetime import datetime

class LoggingWorker(QtCore.QObject):
    start = QtCore.pyqtSignal()
    
    def __init__(self, queue_a, queue_b, print_queue):
        super().__init__()
        self.queue_a = queue_a
        self.queue_b = queue_b
        self.print_queue = print_queue
        self.logging = False
        self.running = False
        self.a_path = None
        self.b_path = None
        self.max_rcv_time_diff = 4000
        self.max_rcv_time_diff_on = False
        
        # TODO: use GuiData object
        self.correlation_threshold = 0.96
        
        self.correlation_threshold_on = False        
        self.pkt_count = 0
        self.DELIMITER = ' '
        self.DUMMY = "NaN"
        self.NO_3X3_FILTER = True
        
    def log(self):
        self.running = True
        if self.logging:
            # Export csi as .npy binary file
            self.a_path = Path('./csi_offline_bin_data/Alice/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_alice_csi_log.npy')
            self.b_path = Path('./csi_offline_bin_data/Bob/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_bob_csi_log.npy')
            # Export csi as .csv file 
            self.a_csv_path = Path('./csi_offline_bin_data/Alice/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_alice_csi_log.csv')
            self.b_csv_path = Path('./csi_offline_bin_data/Bob/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_bob_csi_log.csv')
            with self.a_csv_path.open('w') as a_csv_fd:
                for rx in range(3):
                    for tx in range(3):
                        string = 'RX' + str(rx) + 'TX' + str(tx) + ' '
                        a_csv_fd.write('{:12}'.format(string))

            with self.b_csv_path.open('w') as b_csv_fd:
                for rx in range(3):                    
                    for tx in range(3):
                        string = 'RX' + str(rx) + 'TX' + str(tx) + ' '
                        b_csv_fd.write('{:12}'.format(string))
            self._log()
    
    def _log(self):
        if self.running:
            if self.logging:
                a_hdr = np.frombuffer(self.queue_a.get(), dtype = ct.DTYPE_CSI_HDR)
                b_hdr = np.frombuffer(self.queue_b.get(), dtype = ct.DTYPE_CSI_HDR)
        
                # Filter packets by timestamp
                if self.max_rcv_time_diff_on:
                    order = True
                    while order:
                        t_a = a_hdr[0][0]
                        t_b = b_hdr[0][0]
                        td = int(t_a) - int(t_b)
                        if td < -self.max_rcv_time_diff:
                            #drop alice's csi and get next header
                            self.queue_a.get()
                            a_hdr = np.frombuffer(self.queue_a.get(), dtype = ct.DTYPE_CSI_HDR)
                        elif td > self.max_rcv_time_diff:
                            #drop bob's csi and get next header
                            self.queue_b.get()
                            b_hdr = np.frombuffer(self.queue_b.get(), dtype = ct.DTYPE_CSI_HDR)
                        else:
                            order = False
                
                rx_a = a_hdr[0][8]
                tx_a = a_hdr[0][9]
                num_tones_a = a_hdr[0][7]
                rx_b = b_hdr[0][8]
                tx_b = b_hdr[0][9]        
                num_tones_b = b_hdr[0][7]

                # Only log 3x3 MIMO packets without physical error indication
                if self.NO_3X3_FILTER or (rx_a == 3 and tx_a == 3 and rx_b == 3 and tx_b == 3 and a_hdr[0][4] == 0 and b_hdr[0][4] == 0):
                    a_csi = np.frombuffer(self.queue_a.get(), dtype = ct.DTYPE_CSI_COMPLEX)
                    b_csi = np.frombuffer(self.queue_b.get(), dtype = ct.DTYPE_CSI_COMPLEX)
                    
                    a_csi = a_csi.reshape(rx_a, tx_a, num_tones_a)
                    b_csi = b_csi.reshape(rx_b, tx_b, num_tones_b)

                    if self.correlation_threshold_on:
                        a_phase = np.unwrap(np.angle(a_csi[:][:][:]['real'] + 1.j*a_csi[:][:][:]['imag']))
                        b_phase = np.unwrap(np.angle(b_csi[:][:][:]['real'] + 1.j*b_csi[:][:][:]['imag']))
                        # TODO: Check if index is valid
                        # TODO: Don't just check antenna 0 to antenna 0 correlation
                        correlation = np.corrcoef(b_phase[0][0][:], a_phase[0][0][:])[0][1]
                        if correlation >= self.correlation_threshold:
                            self.write_to_drive(a_hdr, b_hdr, a_csi, b_csi)
                            self.pkt_count += 1
                            self.print_queue.put(str(self.pkt_count) + " packets logged.")
                        # else: # Drop it                          
                    else:
                        # No correlation filter. Log csi data.
                        self.write_to_drive(a_hdr, b_hdr, a_csi, b_csi)
                        self.pkt_count += 1
                        self.print_queue.put(str(self.pkt_count) + " packets logged.")

                    # NEW FILE EVERY 512 PACKETS
                    if(self.pkt_count == 512):
                        # Export csi as .npy binary file
                        self.a_path = Path('./csi_offline_bin_data/Alice/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_alice_csi_log.npy')
                        self.b_path = Path('./csi_offline_bin_data/Bob/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_bob_csi_log.npy')
                        # Export csi as .csv file 
                        self.a_csv_path = Path('./csi_offline_bin_data/Alice/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_alice_csi_log.csv')
                        self.b_csv_path = Path('./csi_offline_bin_data/Bob/' + str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')) + '_bob_csi_log.csv')
                        with self.a_csv_path.open('w') as a_csv_fd:
                            for rx in range(3):
                                for tx in range(3):
                                    string = 'RX' + str(rx) + 'TX' + str(tx) + ' '
                                    a_csv_fd.write('{:12}'.format(string))

                        with self.b_csv_path.open('w') as b_csv_fd:
                            for rx in range(3):                    
                                for tx in range(3):
                                    string = 'RX' + str(rx) + 'TX' + str(tx) + ' '
                                    b_csv_fd.write('{:12}'.format(string))
                        self.pkt_count = 0

                else:
                    # drop it
                    self.queue_a.get()
                    self.queue_b.get()
                
                QtCore.QTimer.singleShot(0, self._log)
            
        else:
            # TODO: load logged data
            pass
            
    def write_to_drive(self, a_hdr, b_hdr, a_csi, b_csi):
        # Reaz needs csi in this format for the matlab script
        a_csi_for_reaz = a_csi['real'] + 1.j*a_csi['imag']
        b_csi_for_reaz = b_csi['real'] + 1.j*b_csi['imag']
        
        # Store binary data in npy file
        with self.a_path.open('ab') as a_fd:
            np.save(a_fd, a_hdr)
            np.save(a_fd, a_csi)
        with self.b_path.open('ab') as b_fd:
            np.save(b_fd, b_hdr)
            np.save(b_fd, b_csi)
            
        # Store data in csv file
        with self.a_csv_path.open('a') as a_csv_fd:
            for subcarrier in range(a_hdr[0][7]):
                a_csv_fd.write('\n')
                for rx in range(3):
                    for tx in range(3):
                        if rx < a_hdr[0][8] and tx < a_hdr[0][9]:
                            string = str(a_csi_for_reaz[rx][tx][subcarrier])
                            string = string.replace('(','')
                            string = string.replace(')','')
                            string = string + self.DELIMITER
                        else:
                            string = self.DUMMY + self.DELIMITER
                        a_csv_fd.write('{:12}'.format(string))
                        
        with self.b_csv_path.open('a') as b_csv_fd:
            for subcarrier in range(b_hdr[0][7]):
                b_csv_fd.write('\n')
                for rx in range(3):         
                    for tx in range(3):
                        if rx < b_hdr[0][8] and tx < b_hdr[0][9]:
                            string = str(b_csi_for_reaz[rx][tx][subcarrier])
                            string = string.replace('(','')
                            string = string.replace(')','')
                            string = string + self.DELIMITER
                        else:
                            string = self.DUMMY + self.DELIMITER
                        b_csv_fd.write('{:12}'.format(string))
    
    def toggle_logging(self):
        self.logging = not self.logging
        
    def set_dt_filter(self, microseconds):
        self.max_rcv_time_diff = microseconds
        
    def set_corr_filter(self, threshold):
        self.correlation_threshold = threshold
    
    def disable_logging(self):
        self.running = False
        self.pkt_count = 0
