#!/usr/bin/env python3

import os
import numpy as np
from pathlib import Path

DTYPE_C_TIMESTAMP = np.dtype([
    ("sec", np.int32),
    ("nsec", np.int32)
])
DTYPE_C_TIMESTAMP = DTYPE_C_TIMESTAMP.newbyteorder('>')

DTYPE_CSI_COMPLEX = np.dtype([
    ("real", np.int32),
    ("imag", np.int32)
])
DTYPE_CSI_COMPLEX = DTYPE_CSI_COMPLEX.newbyteorder('>')

DTYPE_CSI_HDR = np.dtype([
    ("tstamp", np.uint64),
    ("csi_len", np.uint16),
    ("channel", np.uint16),
    ("phyerr", np.uint8),
    ("noise", np.uint8),
    ("rate", np.uint8),
    ("chanbw", np.uint8),
    ("num_tones", np.uint8),
    ("nr", np.uint8),
    ("nc", np.uint8),
    ("rssi", np.uint8),
    ("rssi_0", np.uint8),
    ("rssi_1", np.uint8),
    ("rssi_2", np.uint8),
    ("pld_len", np.uint16)
])
DTYPE_CSI_HDR = DTYPE_CSI_HDR.newbyteorder('>')

CSI_HEADER_SIZE = 25
