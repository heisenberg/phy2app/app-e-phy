#!/usr/bin/env python3
from gui_data import GuiData
from visualization_plot_widget import VisualizationPlotWidget
from visualization_settings_widget import VisualizationSettingsWidget
from csi_acquisition_widget import CSIAcquisitionWidget
from visualization_updater import VisualizationUpdater
from queue_print_worker import QueuePrintWorker

from parameter_estimator import ParameterEstimator
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph.pyqtgraph as pg
import queue
import socket

import scipy.io
import numpy as np

# The main window class contains all the widgets that are displayed
class MainWindow(QtWidgets.QMainWindow):
    visualization_updater_thread = QtCore.QThread()
    info_text_thread = QtCore.QThread()

    def __init__(self):
        super().__init__()

        self.gui_data = GuiData()
        self.print_queue = queue.Queue()

        self.__view()
        self.__control()

    def Ccmd1(self):
        self.key_table_process.write('ls -l')

    def Ccmd2(self):
        self.key_table_process.write("ping google.de")

    def __view(self):
        ######################################
        ### Initialize GUIs central widget ###
        ######################################
        self.current_plots = pg.GraphicsLayoutWidget(show=False)
        row = 0
        col = 0
        for i in range(9):
            p = VisualizationPlotWidget(rx = row, tx = col, title = "RX" + str(col+1) + " TX" + str(row+1), gui_data = self.gui_data)
            self.current_plots.addItem(p, row = row, col = col)
            if(row < 2):
                row += 1
            else:
                row = 0
                col += 1
        self.setCentralWidget(self.current_plots)

        ########################
        ### Left side of GUI ###
        ########################
        # Visualization settings frame
        self.visualization_settings_widget = VisualizationSettingsWidget(self, self.current_plots, self.gui_data, self.print_queue)
        self.visualization_scroll_area = QtGui.QScrollArea()
        self.visualization_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.visualization_scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.visualization_scroll_area.setWidget(self.visualization_settings_widget)
        self.visualization_settings_dock_widget = QtGui.QDockWidget("Visualization settings")
        self.visualization_settings_dock_widget.setWidget(self.visualization_scroll_area)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.visualization_settings_dock_widget)
        # Probing settings frame
        self.csi_acquisition_widget = CSIAcquisitionWidget(self, self.gui_data, self.print_queue)
        self.csi_acquisition_dock_widget = QtGui.QDockWidget("CSI acquisition")
        self.csi_acquisition_scroll_area = QtGui.QScrollArea()
        self.csi_acquisition_scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.csi_acquisition_scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.csi_acquisition_scroll_area.setWidget(self.csi_acquisition_widget)
        self.csi_acquisition_dock_widget.setWidget(self.csi_acquisition_scroll_area)

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.csi_acquisition_dock_widget)

        #########################
        ### Right side of GUI ###
        #########################
        # Metadata frame
        self.metadata_layout = QtGui.QVBoxLayout(self.visualization_settings_widget.metadata_table_widget)
        self.metadata_dock_widget = QtGui.QDockWidget("Metadata")
        self.metadata_dock_widget.setWidget(self.visualization_settings_widget.metadata_table_widget)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.metadata_dock_widget)
        # Info box frame
        self.info_text = QtWidgets.QPlainTextEdit()
        self.info_text.setReadOnly(True)
        self.info_text_layout = QtGui.QVBoxLayout(self.info_text)
        self.info_text_dock_widget = QtGui.QDockWidget("Info box")
        self.info_text_dock_widget.setWidget(self.info_text)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.info_text_dock_widget)

        #####################
        ### Bottom of GUI ###
        #####################
        # Key generation frame
        self.parameter_estimator = ParameterEstimator()
        self.keygen_widget = QtGui.QWidget(self)
        self.keygen_layout = QtGui.QVBoxLayout(self.keygen_widget)
        self.key_gen_button = QtGui.QPushButton("Generate")

        self.param_extract_button = QtGui.QPushButton("Extract")
        self.keygen_form = QtGui.QFormLayout()
        self.keygen_form.addRow(QtGui.QLabel("Extract parameter"), self.param_extract_button)
        self.keygen_form.addRow(QtGui.QLabel("Generate key"), self.key_gen_button)
        self.keygen_layout.addLayout(self.keygen_form)

        self.keygen_dock_widget = QtGui.QDockWidget("Key generation")
        self.keygen_dock_widget.setWidget(self.keygen_widget)
        self.keygen_dock_widget.setMaximumWidth(self.visualization_settings_dock_widget.width() - 255)
        self.keygen_dock_widget.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.keygen_dock_widget)

        # Key information frame
        self.key_table_process = QtCore.QProcess(self)
        self.key_table_terminal = QtGui.QWidget(self)

        self.key_table_cmd1 = QtGui.QPushButton("Cmd1", self)
        self.key_table_cmd2 = QtGui.QPushButton("Cmd2", self)

        self.key_table_layout = QtGui.QVBoxLayout(self.key_table_terminal)

        self.key_table_tab = QtWidgets.QTabWidget()
        #self.key_table_layout.addWidget(EmbTerminal())
        self.key_table_layout.addWidget(self.key_table_tab)
        #self.key_table_tab.addTab(EmbTerminal(), "Local")
        self.key_table_tab.addTab(EmbTerminal(), "AP")
        self.key_table_tab.addTab(EmbTerminal(), "Client")


        # self.key_table_layoutH = QtGui.QHBoxLayout(self)
        # self.key_table_layoutH.addWidget(self.key_table_cmd1)
        # self.key_table_layoutH.addWidget(self.key_table_cmd2)

        # self.key_table_layout.addLayout(self.key_table_layoutH)

        #self.key_table_layout.addWidget(self.key_table_terminal)

        #self.key_table_process.start('urxvt',['-embed', str(self.key_table_terminal.winId())])

        # self.key_table_cmd1.clicked.connect(self.Ccmd1)
        # self.key_table_cmd2.clicked.connect(self.Ccmd2)

        # self.terminal = QtGui.QWidget()
        # self.key_table_layout = QtWidgets.QVBoxLayout(self.key_table_terminal)
        # self.key_table_process = QtCore.QProcess()

        # self.key_table_layout.addWidget(self.terminal)
        # self.key_table_process.start('xterm', ['-into', str(self.terminal.winId())])

        # self.key_font = QtGui.QFont("Monospace", 10, QtGui.QFont.Bold)
        # self.key_font.setStyleHint(QtGui.QFont.TypeWriter)






        self.key_status_dock_widget = QtGui.QDockWidget("Key information")
        self.key_status_dock_widget.setWidget(self.key_table_terminal)

        self.key_status_dock_widget.setMinimumHeight(250)
        # self.key_status_dock_widget.setMaximumHeight(150)

        self.key_status_dock_widget.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.key_status_dock_widget)

        # self.key_table_process.write('ls -l')

    def __control(self):
        self.setup_threads()

        self.key_gen_button.clicked.connect(self.key_generation)
        self.param_extract_button.clicked.connect(self.parameter_estimation)

        ##########################
        ### Menu configuration ###
        ##########################

        fullscreen_action = QtGui.QAction("Fullsreen", self)
        fullscreen_action.setShortcut("F11")
        fullscreen_action.triggered.connect(self.toggleView)
        quit_action = QtGui.QAction("Quit", self)
        quit_action.setShortcut("Ctrl+Q")
        quit_action.triggered.connect(QtCore.QCoreApplication.quit)
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('File')
        file_menu.addAction(quit_action)
        view_menu = main_menu.addMenu('View')
        view_menu.addAction(fullscreen_action)

        ########################################
        ### State machine for probing button ###
        ########################################

        self.pb_state_machine = QtCore.QStateMachine()
        self.pb_state_offline = QtCore.QState()
        self.pb_state_online = QtCore.QState()

        # State transitions
        self.pb_state_offline.addTransition(self.csi_acquisition_widget.probing_button.clicked, self.pb_state_online)
        self.pb_state_online.addTransition(self.csi_acquisition_widget.probing_button.clicked, self.pb_state_offline)

        # Offline state properties
        self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_a, "enabled", True)
        self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_b, "enabled", True)
        #~ self.pb_state_offline.assignProperty(self.visualization_settings_widget.load_button_e, "enabled", True)
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.probing_button, "text", "Start probing")
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.probing_group, "enabled", True)
        self.pb_state_offline.assignProperty(self.csi_acquisition_widget.logging_group, "enabled", True)

        # Online state properties
        self.pb_state_online.assignProperty(self.visualization_settings_widget.all_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.alice_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.bob_sb, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_a, "enabled", False)
        self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_b, "enabled", False)
        #~ self.pb_state_online.assignProperty(self.visualization_settings_widget.load_button_e, "enabled", False)
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_button, "text", "Stop probing")
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_button, "enabled", False)
        self.pb_state_online.assignProperty(self.csi_acquisition_widget.probing_group, "enabled", False)

        # Offline --> Online state transition signals
        self.pb_state_offline.exited.connect(self.visualization_updater.reconnect)
        self.pb_state_offline.exited.connect(self.csi_acquisition_widget.receiver_controller.reconnect)
        self.pb_state_offline.exited.connect(lambda: QtCore.QTimer.singleShot(2000, lambda: self.csi_acquisition_widget.probing_button.setEnabled(True)))
        self.pb_state_offline.exited.connect(self.apply_logging_filters)

        # Online --> Offline state transition signals
        self.pb_state_online.exited.connect(lambda: self.visualization_settings_widget.enable_spin_boxes(True))
        self.pb_state_online.exited.connect(self.visualization_updater.disconnect)
        self.pb_state_online.exited.connect(self.csi_acquisition_widget.receiver_controller.disconnect)
        self.pb_state_online.exited.connect(lambda: self.csi_acquisition_widget.probing_button.setEnabled(False))
        self.pb_state_online.exited.connect(lambda: QtCore.QTimer.singleShot(2000, lambda: self.csi_acquisition_widget.probing_button.setEnabled(True)))

        # Add states to machine and start it
        self.pb_state_machine.addState(self.pb_state_offline)
        self.pb_state_machine.addState(self.pb_state_online)
        self.pb_state_machine.setInitialState(self.pb_state_offline)
        self.pb_state_machine.start()

        # Start thread
        self.csi_acquisition_widget.start_threads_signal.emit()

    def apply_logging_filters(self):
        if self.csi_acquisition_widget.pkt_time_diff_cb.isChecked:
            self.csi_acquisition_widget.receiver_controller.set_logging_dt_filter(int(self.csi_acquisition_widget.pkt_time_diff_dsb.text()))
        if self.csi_acquisition_widget.pkt_correlation_cb.isChecked:
            self.csi_acquisition_widget.receiver_controller.set_logging_corr_filter(float(self.csi_acquisition_widget.pkt_correlation_dsb.text()))

    def update_info_text(self, text):
        self.info_text.appendPlainText(text)

    def toggleView(self, index):
        if self.windowState() != QtCore.Qt.WindowFullScreen:
            self.showFullScreen()
        else:
            self.showMaximized()

    def parameter_estimation(self):
        #~ self.key_gen_button.setEnabled(True)
        self.parameter_estimator.load_csi(self.gui_data.alice_csi, self.gui_data.bob_csi)
        for k in range(self.gui_data.alice_csi.phase[0].shape[0]):
            for j in range(self.gui_data.alice_csi.phase[0].shape[1]):
                self.print_queue.put("Extracting parameters for RX " + str(k) + ", TX " + str(j))
                params_a, params_b = self.parameter_estimator.extract(k,j)

                for i in range(min(len(params_a),len(params_b))):
                    self.print_queue.put("Alice's parameters:" + str(params_a[i]))
                    self.print_queue.put("Bob's parameters : " + str(params_b[i]))
                    self.print_queue.put("")

                self.parameter_estimator.export(k,j)
                self.print_queue.put("Parameter extraction finished.")

    def key_generation(self):
        #with open("bits_alice.txt", "w") as ff:
        #    ff.write("")
        #with open("bits_bob.txt", "w") as ff:
        #    ff.write("")
        #with open("passphrases_alice.txt", "w") as ff:
        #    ff.write("")
        #with open("passphrases_bob.txt", "w") as ff:
        #    ff.write("")
        for k in range(self.gui_data.alice_csi.phase[0].shape[0]):
            for j in range(self.gui_data.alice_csi.phase[0].shape[1]):
                # IMPORT MEASUREMENT DATA
                a00 = np.array(scipy.io.loadmat('csi_parameters/params_a'+str(k)+str(j)+'.mat')["params_a"])
                b00 = np.array(scipy.io.loadmat('csi_parameters/params_b'+str(k)+str(j)+'.mat')["params_b"])
                N_PKT = a00.shape[0]
                WINDOW_SIZE = 3
                key_a, key_b = [], []

                # all packets, all tx-rx available
                # blocks of 128 bits
                # passphrases in txt file
                # binary strings for alice and bob separated but all packets and all txrx

                # SLIDING WINDOW QUANTIZATION
                for i in range(N_PKT//WINDOW_SIZE):
                    key_a += list(map(int, a00[i*WINDOW_SIZE:i*WINDOW_SIZE+WINDOW_SIZE, 0] > np.mean(a00[i*WINDOW_SIZE:i*WINDOW_SIZE+WINDOW_SIZE, 0])))
                    key_b += list(map(int, b00[i*WINDOW_SIZE:i*WINDOW_SIZE+WINDOW_SIZE, 0] > np.mean(b00[i*WINDOW_SIZE:i*WINDOW_SIZE+WINDOW_SIZE, 0])))
                # remaining bits that didn't fit in one window
                key_a += list(map(int, a00[WINDOW_SIZE*(N_PKT//WINDOW_SIZE):N_PKT, 0] > np.mean(a00[WINDOW_SIZE*(N_PKT//WINDOW_SIZE):N_PKT, 0])))
                key_b += list(map(int, b00[WINDOW_SIZE*(N_PKT//WINDOW_SIZE):N_PKT, 0] > np.mean(b00[WINDOW_SIZE*(N_PKT//WINDOW_SIZE):N_PKT, 0])))

                key_a = "".join(str(x) for x in key_a)
                key_b = "".join(str(x) for x in key_b)
                self.print_queue.put("Alice's key bitstring TX" + str(k) + " RX" + str(j)+": " + key_a)
                self.print_queue.put("Bob's key bitstring TX" + str(k) + " RX" + str(j)+": " + key_b)
                with open("bits_alice.txt", "a") as abf:
                    abf.write(key_a)
                with open("bits_bob.txt", "a") as bbf:
                    bbf.write(key_b)
                self.print_queue.put("")

                passphrase_a, passphrase_b = "", ""
                # HEX MAPPING
                # UTF-8 / lowercase HEX / uppercase HEX
                for i in range(N_PKT//8):
                    if(i % 16 == 0 and i != 0):
                        with open("passphrases.txt", "a") as pf:
                            pf.write(passphrase_a + "\n")
                            pf.write(passphrase_b + "\n")
                        self.print_queue.put("Alice's passphrase TX" + str(k) + " RX" + str(j)+" block " + str(i/16) + ":\n" + passphrase_a)
                        self.print_queue.put("Bob's's passphrase TX" + str(k) + " RX" + str(j)+" block " + str(i/16) + ":\n" + passphrase_b)
                        passphrase_a, passphrase_b = "", ""
                    if(int(key_a[i*8:i*8+8], 2) <= 0x20):
                        passphrase_a += hex(int(key_a[i*8:i*8+8], 2))[2:].upper()
                    elif(int(key_a[i*8:i*8+8], 2) >= 0x80):
                        passphrase_a += hex(int(key_a[i*8:i*8+8], 2))[2:].lower()
                    else:
                        passphrase_a += chr(int(key_a[i*8:i*8+8], 2))

                    if(int(key_b[i*8:i*8+8], 2) <= 0x20):
                        passphrase_b += hex(int(key_b[i*8:i*8+8], 2))[2:].upper()
                    elif(int(key_b[i*8:i*8+8], 2) >= 0x80):
                        passphrase_b += hex(int(key_b[i*8:i*8+8], 2))[2:].lower()
                    else:
                        passphrase_b += chr(int(key_b[i*8:i*8+8], 2))

        '''
        # Timeout for execution of key generation script
        wait_for_finish = 15000
        self.key_gen_button.setEnabled(False)

        # Start key generation script
        try:
            self.key_generation_process = QtCore.QProcess()
            self.key_generation_process.start("./runDemo_LOSparams.sh", [])
            finished = self.key_generation_process.waitForFinished(wait_for_finish)
            if not finished:
                # TODO: Improve execution speed of matlab script, e.g. by starting a matlab instance in the background when the gui is started.
                #~ self.print_queue.put("Key generation process didn't finish execution after " + str(wait_for_finish) + "ms.")
                self.key_generation_process.kill()
            else:
                self.print_queue.put("Key generation finished.")
        except Exception:
            self.print_queue.put("Cannot start key generation script.")

        # Load and display generated keys from files
        # for rx in range(3):
        #     for tx in range(3):
        #         try:
        #             file_name = "Key" + str(rx) + str(tx) + ".txt"
        #             file_path = Path(file_name)
        #             key = file_path.read_text()
        #             self.key_table_terminal.setItem(rx, tx, QtGui.QTableWidgetItem(key))
        #             self.key_table_terminal.item(rx, tx).setFont(self.key_font)
        #         except Exception as error:
        #             pass
        #             # ~ print(error)
        #             self.print_queue.put("No key file for pair " + str(rx) + str(tx))

        self.key_gen_button.setEnabled(False)
        '''
    def setup_threads(self):
        # TODO: Check if QtProcess should be declared as static class member
        self.key_generation_process = None

        ##################################
        ### Thread for Online Plotting ###
        ##################################
        self.visualization_updater = VisualizationUpdater(self.gui_data, self.print_queue)
        self.visualization_updater.moveToThread(self.visualization_updater_thread)

        # TODO: refactor visualization updater and visualisation settings widget
        self.visualization_updater.data_available.connect(self.visualization_settings_widget.online_update_metadata)
        self.visualization_updater_thread.start()

        ##################################
        ### Start info text box thread ###
        ##################################
        self.info_text_worker = QueuePrintWorker(self.print_queue)
        self.info_text_worker.moveToThread(self.info_text_thread)
        self.info_text_worker.new_info_text.connect(self.update_info_text)
        self.info_text_thread.started.connect(self.info_text_worker.run)
        self.info_text_thread.start()

class EmbTerminal(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EmbTerminal, self).__init__(parent)
        self.process = QtCore.QProcess(self)
        self.terminal = QtWidgets.QWidget(self)
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.terminal)
        # Works also with urxvt:
        self.process.start('urxvt',['-embed', str(int(self.winId()))])
        self.setFixedSize(1000, 200)
