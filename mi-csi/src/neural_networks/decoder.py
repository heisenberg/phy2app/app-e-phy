import torch
import torch.nn as nn
import torch.nn.functional as F
from src.utils.custom_typing import EncoderOutput


class BaseDecoder(nn.Module):
	def __init__(
		self,
		img_size: int,
		kernel_size: int,
		repr_dim: int,
	):
		"""Encoder to extract the representations

		Args:
			img_size (int): [Image size (must be squared size)]
			in_channels (int): Number of input channels
			num_filters (int): Intermediate number of filters
			kernel_size (int): Convolution kernel size
			repr_dim (int): Dimension of the desired representation
		"""
		super().__init__()

		self.linear1 = nn.Linear(repr_dim, 14)

		self.deconv1 = nn.ConvTranspose1d(1, 64, kernel_size=kernel_size, padding=2)
		self.upsample1 = nn.Upsample(size=28)

		self.deconv2 = nn.ConvTranspose1d(64, 32, kernel_size=kernel_size, padding=2)
		self.upsample2 = nn.Upsample(size=img_size)

		self.deconv3 = nn.ConvTranspose1d(32, 1, kernel_size=kernel_size, padding=2)

	def forward(self, x: torch.Tensor) -> EncoderOutput:
		"""Forward encoder

		Args:
			x (torch.Tensor): Image from a given domain

		Returns:
			EncoderOutput: Representation and feature map
		"""
		x = F.relu(self.linear1(x))
		x = x[:, None, :]
		x = F.relu(self.deconv1(x))
		x = self.upsample1(x)
		x = F.relu(self.deconv2(x))
		x = self.upsample2(x)
		x = torch.sigmoid(self.deconv3(x))
		return x
