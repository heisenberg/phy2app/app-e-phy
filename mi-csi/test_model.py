from cProfile import label
import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import ruamel.yaml as yaml
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.decomposition import PCA
from src.neural_networks.encoder import BaseEncoder

## Function for data import

def import_data(path):
    input_data = np.load(path)
    input_data = np.swapaxes(input_data, 1, 2)
    input_data = input_data[:512000,:,:]
    print("Data:", np.shape(input_data))

    train_size, val_size, test_size = int(0.6*input_data.shape[0]), int(0.2*input_data.shape[0]), int(0.2*input_data.shape[0])

    train_dataset, val_dataset, test_dataset = [input_data[i] for i in range(train_size)], [input_data[i] for i in range(train_size, train_size+val_size)], [input_data[i] for i in range(train_size+val_size, train_size+val_size+test_size)]

    print("Train:", train_size)
    print("Val:", val_size)
    print("Test:", test_size)
    print("Total:", train_size+val_size+test_size)
    print()

    return train_dataset, val_dataset, test_dataset

## Model paths and parameter configurations

sh_folder = "054606e527274270a08e55de8258a22d"
ex_folder = "3946c8d667ed49529d5c157426878610"

with open("conf/share_conf.yaml", "r") as f:
    conf_sh = yaml.safe_load(f)
with open("conf/exclusive_conf.yaml", "r") as f:
    conf_ex = yaml.safe_load(f)

MODEL_PARAM = conf_sh["model_param"]
MODEL_PARAM_EX = conf_ex["model_param"]

img_size=MODEL_PARAM["img_size"]
channels=MODEL_PARAM["channels"]
shared_dim=MODEL_PARAM["shared_dim"]
exclusive_dim=MODEL_PARAM_EX["exclusive_dim"]
switched=MODEL_PARAM["switched"]

## Model definition

model_sh_x = BaseEncoder(
    img_size=img_size,
    in_channels=channels,
    num_filters=64,
    kernel_size=5,
    repr_dim=shared_dim,
)

model_sh_y = BaseEncoder(
    img_size=img_size,
    in_channels=channels,
    num_filters=64,
    kernel_size=5,
    repr_dim=shared_dim,
)

model_ex_x = BaseEncoder(
    img_size=img_size,
    in_channels=channels,
    num_filters=64,
    kernel_size=5,
    repr_dim=exclusive_dim,
)

model_ex_y = BaseEncoder(
    img_size=img_size,
    in_channels=channels,
    num_filters=64,
    kernel_size=5,
    repr_dim=exclusive_dim,
)

## Import weights from training

model_sh_x.load_state_dict(torch.load("mlruns/1/"+sh_folder+"/artifacts/sh_encoder_x/state_dict.pth"))
model_sh_x.eval()

model_sh_y.load_state_dict(torch.load("mlruns/1/"+sh_folder+"/artifacts/sh_encoder_y/state_dict.pth"))
model_sh_y.eval()

model_ex_x.load_state_dict(torch.load("mlruns/2/"+ex_folder+"/artifacts/ex_encoder_x/state_dict.pth"))
model_ex_x.eval()

model_ex_y.load_state_dict(torch.load("mlruns/2/"+ex_folder+"/artifacts/ex_encoder_y/state_dict.pth"))
model_ex_y.eval()

## Load data

print("Loading Alice data:")
alice_train, alice_val, alice_test = import_data('data/CSI/phase_dataset_alice.npy')
print("Loading Bob data:")
bob_train, bob_val, bob_test = import_data('data/CSI/phase_dataset_bob.npy')

pca = PCA(n_components=2)

alice_shared = []
bob_shared = []
phase_correlation = []
shared_correlation = []
exclusive_correlation = []

## Do something with data

for i in tqdm(range(1000)):
    #s = random.randint(0, len(alice_test))

    img_x = alice_test[i]
    img_y = bob_test[i]

    ## Normalize measurements

    norm_x = (img_x[0] - np.mean(img_x[0])) / np.std(img_x[0])
    norm_y = (img_y[0] - np.mean(img_y[0])) / np.std(img_y[0])

    ## Correlation of normalized measurements

    phase_correlation.append(np.correlate(norm_x, norm_y))

    ## Extract shared data for x and y

    sh_x = model_sh_x(torch.Tensor(img_x[None, :]))[0].detach().numpy()[0]
    sh_y = model_sh_y(torch.Tensor(img_y[None, :]))[0].detach().numpy()[0]

    ## Extract exclusive data for x and y
    
    ex_x = model_ex_x(torch.Tensor(img_x[None, :]))[0].detach().numpy()[0]
    ex_y = model_ex_y(torch.Tensor(img_y[None, :]))[0].detach().numpy()[0]

    ## Save values

    alice_shared.append(sh_x)
    bob_shared.append(sh_y)

    ## Normalize values and compute correlation

    norm_sh_x = (sh_x - np.mean(sh_x)) / np.std(sh_x)
    norm_sh_y = (sh_y - np.mean(sh_y)) / np.std(sh_y)

    shared_correlation.append(np.correlate(norm_sh_x, norm_sh_y))

    ## Normalize values and compute correlation

    norm_ex_x = (ex_x - np.mean(ex_x)) / np.std(ex_x)
    norm_ex_y = (ex_y - np.mean(ex_y)) / np.std(ex_y)

    exclusive_correlation.append(np.correlate(norm_ex_x, norm_ex_y))
    
    '''
    ## Plot measurement, shared and exclusive in one plot
    fig, axs = plt.subplots(1, 3)
    axs[0].plot(img_x[0, :], label="Alice")
    axs[0].plot(img_y[0, :], label="Bob")
    axs[0].set_title("Raw phase measurements")
    axs[0].legend()
    axs[1].plot(sh_x)
    axs[1].plot(sh_y)
    axs[1].set_title("Shared information")
    axs[2].plot(ex_x)
    axs[2].plot(ex_y)
    axs[2].set_title("Exclusive information")
    plt.tight_layout()
    plt.savefig("figs/" + str(i) + ".png")
    #plt.show()
    plt.close()
    '''

## Plot correlation
plt.plot(phase_correlation, label="Phase")
plt.plot(shared_correlation, label="Shared information")
plt.plot(exclusive_correlation, label="Exclusive information")
plt.xlabel("Samples")
plt.ylabel("Cross-correlation")
plt.legend()
plt.show()
    
'''
## PCA
principalComponents_x = pca.fit_transform(alice_shared)
principalComponents_y = pca.fit_transform(bob_shared)
print(principalComponents_x)
print(principalComponents_y)
plt.scatter(principalComponents_x[:, 0], principalComponents_x[:, 1])
plt.scatter(principalComponents_y[:, 0], principalComponents_y[:, 1])
plt.show()
'''