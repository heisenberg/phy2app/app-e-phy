This repository contains relevant works related to collaborative project PHY2APP and its sub-project APP-E-PHY

**Directroy Structure**
- data: Default location for data, either commited or created by scripts. It shall locally contains required .npy files as inout to all scripts/notebooks. .npy files are excluded from this repository. They are available at https://box.fu-berlin.de/s/77pWqEFiQJf98bS
- ml-dl-fl: Machine learning related code that is used to develop a model from CSI measurement to extract features
- router-src 
    - code : Python code for CSI visualization in GUI and C/C++ code for compiling module for Wi-Fi routers. Compiling instructions is available in ~/submodules/openwrt README file.
    - submodules : dependent git repositories added as git submodules. Only OpenWrt repository will be needed to cross-compile. Other submodules repositories are directly copied into the relevant directories.
- mi-csi: Mutual Information Approach based on https://arxiv.org/pdf/1912.03915.pdf
    - Configuration: conf/{share, exclusive}_conf.yaml to edit dimensions/hyperparameters/etc.
    - run sdim_runner.sh in console
    - shared models are saved in mlruns/1/
    - edit edim_runner.sh and paste path of saved shared model
    - run edim_runner.sh in console
    - "mlflow ui" in console, then localhost:5000 in browser to access mlflow interface and inspect losses
    - test_model.py to test model (remember to paste the correct model paths in the beginning)
